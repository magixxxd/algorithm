package com.xxd.algo.newcode.mid01.class01;

import java.util.Arrays;

public class Code04_ColorLeftRight {

    // RGRGR -> RRRGG
    public static int minPaint(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }

        char[] chars = s.toCharArray();
        int[] left = new int[chars.length]; // 辅助数组，左边有多少个G
        int[] right = new int[chars.length];// 辅助数组，右边有多少个R
        for (int i = 0; i < chars.length; i++) {
            if (i == 0) {
                left[i] = chars[i] == 'G' ? 1 : 0;
            } else {
                left[i] = chars[i] == 'G' ? (left[i - 1] + 1) : left[i - 1];
            }
        }

        for (int i = chars.length - 1; i >= 0 ; i--) {
            if (i == chars.length - 1) {
                right[i] = chars[i] == 'R' ? 1 : 0;
            } else {
                right[i] = chars[i] == 'R' ? right[i + 1] + 1 : right[i + 1];
            }
        }
        System.out.println(Arrays.toString(left));
        System.out.println(Arrays.toString(right));

        // 全部涂G
        int res = left[left.length - 1];

        // 全部涂R
        res = Math.min(res, right[0]);

        for (int i = 0; i < chars.length - 1; i++) {
            // 左边涂R ，右边涂G
            res = Math.min(res, left[i] + right[i + 1]);
        }

        return res;
    }

    public static void main(String[] args) {
        String test = "GGGGGR";
        System.out.println(minPaint(test));

    }
}
