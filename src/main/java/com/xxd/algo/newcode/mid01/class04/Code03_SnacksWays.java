package com.xxd.algo.newcode.mid01.class04;

public class Code03_SnacksWays {

	public static int ways1(int[] arr, int w) {
		return process(arr, 0, w);
	}

	/**
	 * 暴力递归，尝试模型，从左到右
	 * @param arr 数组
	 * @param index 当前尝试的索引
	 * @param rest 还剩下多少容量
	 * @return
	 */
	public static int process(int[] arr, int index, int rest) {
		if (rest < 0) {
			return -1; // 没有方案，就是这种方案不可行
		}

		if (index == arr.length) { // 没有零食可以选择了，加上之前的所有决策构成一种方案
			return 1;
		}

		// 可以做两种选择，选当前的零食
		int next1 = process(arr, index + 1, rest - arr[index]);
		// 不选当前的零食
		int next2 = process(arr, index + 1, rest);
		return next2 + (next1 == -1 ? 0 : next1);
	}

	public static int ways2(int[] arr, int w) {
		int N = arr.length;
		int[][] dp = new int[N + 1][w + 1];
		for (int j = 0; j <= w; j++) {
			dp[N][j] = 1;
		}
		for (int i = N - 1; i >= 0; i--) {
			for (int j = 0; j <= w; j++) {
				dp[i][j] = dp[i + 1][j] + ((j - arr[i] >= 0) ? dp[i + 1][j - arr[i]] : 0);
			}
		}
		return dp[0][w];
	}

	public static int ways3(int[] arr, int w) {
		int N = arr.length;
		int[][] dp = new int[N][w + 1];
		for (int i = 0; i < N; i++) {
			dp[i][0] = 1;
		}
		if (arr[0] <= w) {
			dp[0][arr[0]] = 1;
		}
		for (int i = 1; i < N; i++) {
			for (int j = 1; j <= w; j++) {
				dp[i][j] = dp[i - 1][j] + ((j - arr[i]) >= 0 ? dp[i - 1][j - arr[i]] : 0);
			}
		}
		int ans = 0;
		for (int j = 0; j <= w; j++) {
			ans += dp[N - 1][j];
		}
		return ans;
	}

	public static void main(String[] args) {
		int[] arr = { 4, 3, 2, 9 };
		int w = 10;
		System.out.println(ways1(arr, w));
		System.out.println(ways2(arr, w));
		System.out.println(ways3(arr, w));

	}

}
