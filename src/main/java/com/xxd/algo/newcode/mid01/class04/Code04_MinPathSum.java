package com.xxd.algo.newcode.mid01.class04;

public class Code04_MinPathSum {

    public static int minPathSum1(int[][] m) {
        if (m == null || m.length == 0 || m[0] == null || m[0].length == 0) {
            return 0;
        }
        int row = m.length;
        int col = m[0].length;
        int[][] dp = new int[row][col];
        dp[0][0] = m[0][0];
        for (int i = 1; i < row; i++) {
            dp[i][0] = dp[i - 1][0] + m[i][0];
        }

        for (int j = 1; j < col; j++) {
            dp[0][j] = dp[0][j - 1] + m[0][j];
        }

        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                dp[i][j] = Math.min(dp[i][j - 1], dp[i - 1][j]) + m[i][j];
            }
        }

        return dp[row - 1][col - 1];
    }

    /**
     * 行更多 就申请行大小的数组
     * 列更多 就申请列大小的数组
     *
     * @param m
     * @return
     */
    public static int minPathSum2(int[][] m) {
        if (m == null || m.length == 0 || m[0] == null || m[0].length == 0) {
            return 0;
        }
        int more = Math.max(m.length, m[0].length);
        int less = Math.min(m.length, m[0].length);
        boolean rowmore = m.length == more;
        int[] dp = new int[less];
        dp[0] = m[0][0];
        for (int i = 1; i < less; i++) {
            dp[i] = dp[i - 1] + (rowmore ? m[0][i] : m[i][0]);
        }

        for (int i = 1; i < more; i++) {
            dp[0] = dp[0] + (rowmore ? m[i][0] : m[0][i]);
            for (int j = 1; j < less; j++) {
                dp[j] = (rowmore ? m[i][j] : m[j][i]) + Math.min(dp[j - 1], dp[j]);
            }
        }
        return dp[less - 1];
    }

    public static int minPathSum3(int[][] m) {
        if (m == null || m.length == 0 || m[0] == null || m[0].length == 0) {
            return 0;
        }
        int[] dp = new int[m[0].length];
        int row = m.length;
        int col = m[0].length;
        dp[0] = m[0][0];

        for (int i = 1; i < col; i++) {
            dp[i] = m[0][i] + dp[i - 1];
        }

        for (int i = 1; i < row; i++) {
            dp[0] = dp[0] + m[i][0];
            for (int j = 1; j < col; j++) {
                dp[j] = Math.min(dp[j - 1], dp[j]) + m[i][j];
            }
        }

        return dp[col - 1];
    }

    // 暴力递归
    public static int minPath1(int[][] matrix) {
        if (matrix.length <= 0 || matrix[0].length <= 0 || matrix == null) return 0;
        return path(matrix, 0, 0);
    }

    public static int path(int[][] matrix, int i, int j) {
        if (i == matrix.length - 1 && j == matrix[0].length - 1) return matrix[i][j];
        if (i == matrix.length - 1) return matrix[i][j] + path(matrix, i, j + 1);
        if (j == matrix[0].length - 1) return matrix[i][j] + path(matrix, i + 1, j);

        return matrix[i][j] + Math.min(path(matrix, i + 1, j),
                path(matrix, i, j + 1));
    }

    /**
     * 暴力递归方法
     */
    public static int minPathSum4(int[][] m, int row, int col) {
        if (m == null) {
            return 0;
        }
        // 到达最后一个格子了
        if (row == m.length - 1 && col == m[0].length - 1) {
            return m[row][col];
        }

        if (row == m.length - 1) { // 只能往右走
            return m[row][col] + minPathSum4(m, row, col + 1);
        }

        if (col == m[0].length - 1) { // 只能往下走
            return m[row][col] + minPathSum4(m, row + 1, col);
        }

        // 可以往下或者往右

        return m[row][col] +
                Math.min(minPathSum4(m, row + 1, col), minPathSum4(m, row, col + 1));
    }

    // for test
    public static int[][] generateRandomMatrix(int rowSize, int colSize) {
        if (rowSize < 0 || colSize < 0) {
            return null;
        }
        int[][] result = new int[rowSize][colSize];
        for (int i = 0; i != result.length; i++) {
            for (int j = 0; j != result[0].length; j++) {
                result[i][j] = (int) (Math.random() * 10);
            }
        }
        return result;
    }

    // for test
    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i != matrix.length; i++) {
            for (int j = 0; j != matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        // int[][] m = generateRandomMatrix(3, 4);
        int[][] m = {
                {1, 3, 5, 9, 7},
                {8, 1, 3, 4, 7},
                {5, 5, 6, 6, 6},
                {8, 8, 4, 5, 2},
        };
        printMatrix(m);
        System.out.println(minPathSum1(m));
        System.out.println(minPathSum2(m));
        System.out.println(minPathSum3(m));
        System.out.println(minPathSum4(m, 0, 0));
        System.out.println(minPath1(m));

    }
}
