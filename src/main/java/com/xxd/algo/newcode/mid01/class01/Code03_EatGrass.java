package com.xxd.algo.newcode.mid01.class01;

import java.util.PriorityQueue;

public class  Code03_EatGrass {

	// n份青草放在一堆
	// 先手后手都决定聪明
	// string "先手" "后手"
	public static String winner1(int n) {
		// 0 1 2 3 4
		// 后 先 后 先 先
		if (n < 5) {
			return (n == 0 || n == 2) ? "后手" : "先手";
		}

		int base = 1; // 从吃1份开始尝试
		while (base <= n) {
			if (winner1(n - base).equals("后手")) {
				return "先手";
			}

			base = base * 4;
		}

		return "后手";
	}

	public static String winner2(int n) {
		if (n % 5 == 0 || n % 5 == 2) {
			return "后手";
		} else {
			return "先手";
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i <= 50; i++) {
			System.out.println(i + " : " + winner1(i));
		}
	}

}
