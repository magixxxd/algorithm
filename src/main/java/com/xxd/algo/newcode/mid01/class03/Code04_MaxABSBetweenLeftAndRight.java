package com.xxd.algo.newcode.mid01.class03;

public class Code04_MaxABSBetweenLeftAndRight {

	/**
	 * 暴力解法 复杂度O(N2)
	 * @param arr
	 * @return
	 */
	public static int maxABS1(int[] arr) {
		int res = Integer.MIN_VALUE;
		int maxLeft = 0;
		int maxRight = 0;
		for (int i = 0; i != arr.length - 1; i++) {
			maxLeft = Integer.MIN_VALUE;
			for (int j = 0; j != i + 1; j++) {
				maxLeft = Math.max(arr[j], maxLeft);
			}
			maxRight = Integer.MIN_VALUE;
			for (int j = i + 1; j != arr.length; j++) {
				maxRight = Math.max(arr[j], maxRight);
			}
			res = Math.max(Math.abs(maxLeft - maxRight), res);
		}
		return res;
	}

	/**
	 * 使用辅助数组的方式来玩
	 * O(N)
	 * @param arr
	 * @return
	 */
	public static int maxABS2(int[] arr) {
		// 左边最大的值
		int[] leftMax = new int[arr.length];
		// 右边最大的值
		int[] rightMax = new int[arr.length];

		leftMax[0] = arr[0];
		rightMax[arr.length - 1] = arr[arr.length - 1];

		for (int i = 1; i < arr.length; i++) {
			leftMax[i] = Math.max(leftMax[i - 1], arr[i]);
		}

		for (int i = arr.length - 2; i > -1; i--) {
			rightMax[i] = Math.max(rightMax[i + 1], arr[i]);
		}

		int max = 0;
		for (int i = 0; i < arr.length - 1; i++) {
			max = Math.max(max, Math.abs(leftMax[i] - rightMax[i + 1]));
		}
		return max;
	}

	/**
	 * 分析问题的普遍性
	 * @param arr
	 * @return
	 */
	public static int maxABS3(int[] arr) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < arr.length; i++) {
			max = Math.max(arr[i], max);
		}
		return max - Math.min(arr[0], arr[arr.length - 1]);
	}

	public static int[] generateRandomArray(int length) {
		int[] arr = new int[length];
		for (int i = 0; i != arr.length; i++) {
			arr[i] = (int) (Math.random() * 1000) - 499;
		}
		return arr;
	}

	public static void main(String[] args) {
		int[] arr = generateRandomArray(200);
		System.out.println(maxABS1(arr));
		System.out.println(maxABS2(arr));
		System.out.println(maxABS3(arr));
	}
}
