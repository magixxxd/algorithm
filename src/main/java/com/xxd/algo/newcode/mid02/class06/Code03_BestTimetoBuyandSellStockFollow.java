package com.xxd.algo.newcode.mid02.class06;

public class Code03_BestTimetoBuyandSellStockFollow {

	public static int maxProfit(int K, int[] prices) {
		if (prices == null || prices.length == 0) {
			return 0;
		}
		int N = prices.length;
		if (K >= N / 2) {
			return allTrans(prices);
		}
		int[][] dp = new int[K + 1][N];
		int ans = 0;
		for (int j = 1; j <= K; j++) {
			int pre = dp[j][0];
			int best = pre - prices[0];
			for (int i = 1; i < N; i++) {
				pre = dp[j - 1][i];
				dp[j][i] = Math.max(dp[j][i - 1], prices[i] + best);
				best = Math.max(best, pre - prices[i]);
				ans = Math.max(dp[j][i], ans);
			}
		}

		return ans;
	}

	private static int allTrans(int[] prices) {
		int ans = 0;
		for (int i = 1; i < prices.length; i++) {
			ans += Math.max(prices[i] - prices[i - 1], 0);
		}
		return ans;
	}
}
