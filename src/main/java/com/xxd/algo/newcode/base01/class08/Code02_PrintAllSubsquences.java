package com.xxd.algo.newcode.base01.class08;

import java.util.ArrayList;
import java.util.List;

public class Code02_PrintAllSubsquences {

    public static List<String> getAllSubs(String str) {
        char[] chars = str.toCharArray();
        List<String> ans = new ArrayList<>();

        process(chars, 0, ans, "");

        return ans;
    }

    /**
     * str[0..index-1] 的沿途决定，是path记录的
     * str[index] 这个字符，都可以选择要还是不要
     * @param str 传进来的str
     * @param index 当前的索引
     * @param ans 递归传递的list
     * @param path 路劲
     */
    public static void process(char[] str, int index,
                               List<String> ans, String path) {
        if (index == str.length) {
            ans.add(path);
            return;
        }

        // 不要这个字符
        process(str, index + 1, ans, path);

        // 要这个字符
        process(str, index + 1, ans, path + str[index]);
    }


    public static void main(String[] args) {
        String test = "abc";
        List<String> ans = getAllSubs(test);


        for (String str : ans) {
            System.out.println(str);
        }
    }
}
