package com.xxd.algo.newcode.base01.class05;

import java.util.LinkedList;

public class Code05_IsCBT {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    /**
     * 1) 任何节点不能有右孩子 无左孩子
     * 2) 一旦遇到第一个左右不双全的孩子，后面遇到的节点必须是叶子节点
     *
     * @param head
     * @return
     */
    public static boolean isCBT(Node head) {
        if (head == null) {
            return true;
        }
        LinkedList<Node> queue = new LinkedList<>();

        // 是否遇到过左右两个孩子不双全的节点
        boolean leaf = false;
        Node l = null;
        Node r = null;
        queue.add(head);
        while (!queue.isEmpty()) {
            head = queue.poll();
            l = head.left;
            r = head.right;

            // 遇到过孩子不双全 且有孩子（说明不是叶子结点）
            boolean leafNoComplete = leaf && !(l == null && r == null);

            boolean hasRight = l == null && r != null; // 无左节点，有右节点
            if (leafNoComplete || hasRight) { // 如果遇到了不双全的节点之后，又发现当前节点不是叶节点
                return false;
            }


            if (l != null) {
                queue.add(l);
            }
            if (r != null) {
                queue.add(r);
            }

            if (l == null || r == null) {
                leaf = true;
            }
        }
        return true;
    }

}
