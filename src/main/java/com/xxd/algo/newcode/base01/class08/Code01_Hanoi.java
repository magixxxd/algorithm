package com.xxd.algo.newcode.base01.class08;

import java.util.ArrayList;
import java.util.List;

public class Code01_Hanoi {


    public static void hanota(List<Integer> A, List<Integer> B, List<Integer> C) {
        if (A == null || A.size() == 0) {
            return;
        }
        int N = A.size();
        process(N, A, B, C);
    }

    private static void process(int n, List<Integer> from,
                                List<Integer> other,
                                List<Integer> to) {
        if (n <= 0) {
            return;
        }
        process(n - 1, from, to, other);
        to.add(from.remove(from.size() - 1));
        process(n - 1, other, from, to);
    }

    public static void hanoi(int n) {
        if (n > 0) {
            func(n, "左", "中", "右");
        }
    }

    // 1~i 圆盘 目标是from -> to， other是另外一个
    public static void func(int N, String from, String other, String to) {
        if (N <= 0) {
            return;
        }

        func(N - 1, from, to, other);
        System.out.println("Move " + N + " from " + from + " to " + to);
        func(N - 1, other, from, to);
    }

    public static void printAllWays(int n) {
        leftToRight(n);
    }

    public static void leftToRight(int n) {
        if (n == 1) {
            System.out.println("Move 1 from left to right");
            return;
        }
        leftToMid(n - 1);
        System.out.println("Move " + n + " from left to right");
        midToRight(n - 1);
    }

    public static void leftToMid(int n) {
        if (n == 1) {
            System.out.println("Move 1 from left to mid");
            return;
        }
        leftToRight(n - 1);
        System.out.println("Move " + n + " from left to mid");
        rightToMid(n - 1);
    }

    public static void midToRight(int n) {

    }

    public static void rightToMid(int n) {

    }

    public static void main(String[] args) {
        hanoi(3);
        ArrayList<Integer> a = new ArrayList<>();
        a.add(2);
        a.add(1);
        a.add(0);
        ArrayList<Integer> b = new ArrayList<>();
        ArrayList<Integer> c = new ArrayList<>();
        hanota(a, b, c);

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(c.get(0));
    }

}
