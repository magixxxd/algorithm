package com.xxd.algo.msb.interview.trainingcamp003.class23;

import java.util.HashMap;

// 本题为leetcode原题
// 测试链接：https://leetcode.com/problems/largest-component-size-by-common-factor/
// 方法1会超时，但是方法2直接通过
public class Code04_LargestComponentSizebyCommonFactor {

	public static int largestComponentSize1(int[] arr) {
		int N = arr.length;
		UnionFind set = new UnionFind(N);
		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if (gcd(arr[i], arr[j]) != 1) {
					set.union(i, j);
				}
			}
		}
		return set.maxSize();
	}

	public static int largestComponentSize2(int[] arr) {
		int N = arr.length;
		UnionFind unionFind = new UnionFind(N);
		HashMap<Integer, Integer> fatorsMap = new HashMap<>();
		for (int i = 0; i < N; i++) {
			int num = arr[i];
			int limit = (int) Math.sqrt(num);
			for (int j = 1; j <= limit; j++) { // 从 1 去遍历 到我的 sqrt(i)
				if (num % j == 0) { // 如果发现能被我整除，是我的约数
					if (j != 1) { // 因子是大于1的
						if (!fatorsMap.containsKey(j)) { // 没有加到因子map里面去过
							fatorsMap.put(j, i);
						} else {
							// 说明我的因子和其他人的因子有一样的 且这个因子必然是大于1的
							unionFind.union(fatorsMap.get(j), i);
						}
					}
					int other = num / j; // 另外的一个因子，
					if (other != 1) { // j 不是我自己，因为上面的 比如说 15 /3 = 5 上面只加了3，相应的5 也是我的因子也要去加入哇
						if (!fatorsMap.containsKey(other)) {
							fatorsMap.put(other, i);
						} else {
							unionFind.union(fatorsMap.get(other), i);
						}
					}
				}
			}
		}
		return unionFind.maxSize();
	}

	public static int gcd(int m, int n) {
		return n == 0 ? m : gcd(n, m % n);
	}

	public static class UnionFind {
		private int[] parents;
		private int[] sizes;
		private int[] help;

		public UnionFind(int N) {
			parents = new int[N];
			sizes = new int[N];
			help = new int[N];
			for (int i = 0; i < N; i++) {
				parents[i] = i;
				sizes[i] = 1;
			}
		}

		public int maxSize() {
			int ans = 0;
			for (int size : sizes) {
				ans = Math.max(ans, size);
			}
			return ans;
		}

		private int find(int i) {
			int hi = 0;
			while (i != parents[i]) {
				help[hi++] = i;
				i = parents[i];
			}
			for (hi--; hi >= 0; hi--) {
				parents[help[hi]] = i;
			}
			return i;
		}

		public void union(int i, int j) {
			int f1 = find(i);
			int f2 = find(j);
			if (f1 != f2) {
				int big = sizes[f1] >= sizes[f2] ? f1 : f1;
				int small = big == f1 ? f2 : f1;
				parents[small] = big;
				sizes[big] = sizes[f1] + sizes[f2];
			}
		}
	}

	public static void main(String[] args) {
		int[] arr = {3, 4, 7, 8, 12, 17};
		System.out.println(largestComponentSize2(arr));
	}
}
