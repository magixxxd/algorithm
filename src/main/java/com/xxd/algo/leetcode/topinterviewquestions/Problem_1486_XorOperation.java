package com.xxd.algo.leetcode.topinterviewquestions;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-07 10:47
 * @description:
 */
public class Problem_1486_XorOperation {

    public static int xorOperation(int n, int start) {
        int ans = 0;

        for (int i = 0; i < n; i++) {
            ans ^= start + 2 * i;
        }

        return ans;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 200; i++) {
            if (i % 2 == 0) {
                System.out.println("n = 2, start = " + i + " res = " + xorOperation(2, i));
            }
        }

//        System.out.println(xorOperation(4, 3));
//        System.out.println(xorOperation(1, 7));
//        System.out.println(xorOperation(10, 5));
    }
}
