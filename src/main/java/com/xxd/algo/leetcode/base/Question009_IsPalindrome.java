package com.xxd.algo.leetcode.base;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-06 23:14
 * @description:
 */
public class Question009_IsPalindrome {

    /**
     * 判断是否是回文数
     * @param x
     * @return
     */
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }

        String str = "" + x;
        char[] array = str.toCharArray();

        int length = array.length - 1; // 4
        int mid = array.length / 2; // 2
        int i = 0;

        while (i < mid) {
            if (array[i] != array[length - i]) {
                return false;
            } else {
                i++;
            }
        }

        return true;
    }

    public boolean isPalindrome2(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }

        int revertedNumber = 0;
        while (x > revertedNumber) {
            revertedNumber = revertedNumber * 10 + x % 10;
            x /= 10;
        }

        return x == revertedNumber || x == revertedNumber / 10;
    }
    
    public static void main(String[] args) {

        // 121

        Question009_IsPalindrome isPalindrome = new Question009_IsPalindrome();
        System.out.println(isPalindrome.isPalindrome(1111));
        System.out.println(isPalindrome.isPalindrome2(1111));
    }
}
