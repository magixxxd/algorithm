package com.xxd.algo.leetcode.base;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-07 17:05
 * @description:
 */
public class Question690_GetImportance {

    static class Employee {
        public int id;
        public int importance;
        public List<Integer> subordinates;

        public Employee(int id, int importance, List<Integer> subordinates) {
            this.id = id;
            this.importance = importance;
            this.subordinates = subordinates;
        }
    }

    public int getImportance(List<Employee> employees, int id) {
        if (employees == null || employees.size() == 0) {
            return 0;
        }

        int result = 0;

        for (Employee employee : employees) {
            if (employee.id == id) {
                result += employee.importance;

                List<Integer> subordinates = employee.subordinates;
                if (subordinates == null || subordinates.isEmpty()) {
                    return employee.importance;
                }

                for (Integer subordinateId : employee.subordinates) {
                    result +=  getImportance(employees, subordinateId);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();

        int id = 1;
        int importance = 5;
        List<Integer> subordinates = new ArrayList<>();
        subordinates.add(2);
        subordinates.add(3);
        Employee employee = new Employee(id, importance, subordinates);
        employees.add(employee);

        int id2 = 2;
        int importance2 = 3;
        List<Integer> subordinates2 = new ArrayList<>();
        subordinates2.add(4);
        Employee employee2 = new Employee(id2, importance2, subordinates2);
        employees.add(employee2);

        int id3 = 3;
        int importance3 = 4;
        List<Integer> subordinates3 = new ArrayList<>();
        Employee employee3 = new Employee(id3, importance3, subordinates3);
        employees.add(employee3);


        int id4 = 4;
        int importance4 = 1;
        List<Integer> subordinates4 = new ArrayList<>();
        Employee employee4 = new Employee(id4, importance4, subordinates4);
        employees.add(employee4);


        Question690_GetImportance getImportance = new Question690_GetImportance();
        System.out.println(getImportance.getImportance(employees, 1));

    }
}
