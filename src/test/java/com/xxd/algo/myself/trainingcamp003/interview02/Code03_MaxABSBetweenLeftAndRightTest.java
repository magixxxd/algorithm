package com.xxd.algo.myself.trainingcamp003.interview02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-31 22:18
 * @description:
 */
public class Code03_MaxABSBetweenLeftAndRightTest {

    /**
     * 暴力递归方法
     *
     * @param arr
     * @return
     */
    public static int maxABS1(int[] arr) {
        int maxLeft = 0;
        int maxRight = 0;
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length - 1; i++) {
            maxLeft = Integer.MIN_VALUE;
            for (int j = 0; j <= i; j++) {
                maxLeft = Math.max(maxLeft, arr[j]);
            }

            maxRight = Integer.MIN_VALUE;
            for (int j = i + 1; j < arr.length; j++) {
                maxRight = Math.max(maxRight, arr[j]);
            }
            System.out.println("maxLeft = " + maxLeft + "maxRight = " + maxRight);
            ans = Math.max(Math.abs(maxLeft - maxRight), ans);
        }
        return ans;
    }

    /**
     * 辅助数组
     *
     * @param arr
     * @return
     */
    public static int maxABS2(int[] arr) {
        int[] leftMax = new int[arr.length];
        int[] rightMax = new int[arr.length];
        leftMax[0] = arr[0];

        for (int i = 1; i < arr.length; i++) {
            leftMax[i] = Math.max(leftMax[i - 1], arr[i]);
        }

        rightMax[arr.length - 1] = arr[arr.length - 1];
        for (int i = arr.length - 2; i >= 0; i--) {
            rightMax[i] = Math.max(rightMax[i + 1], arr[i]);
        }

        int ans = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            ans = Math.max(ans, Math.abs(leftMax[i] - rightMax[i + 1]));
        }

        return ans;
    }

    /**
     * 思考后的方法
     *
     * @param arr
     * @return
     */
    public static int maxABS3(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(max, arr[i]);
        }

        return max - Math.min(arr[0], arr[arr.length - 1]);
    }
}
