package com.xxd.algo.myself.trainingcamp003.interview02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-31 18:07
 * @description:
 */
public class Code02_PackingMachineTest {


    /**
     * 求最少丢多少轮
     * @param arr
     * @return
     */
    public static int MinOps(int[] arr) {
        if (arr == null) {
            return 0;
        }

        int sum = 0;
        for (int e : arr) {
            sum += e;
        }

        if (sum % arr.length != 0) {
            return 0;
        }

        int avg = sum / arr.length;

        // 左边需要多少件 a - b
        int leftNeed = 0;
        // 右边需要多少件 c - d
        int rightNeed = 0;
        int leftSum = 0;
        int ans = 0;

        for (int i = 0; i < arr.length; i++) {
            // 当前数 为中间数
            leftNeed = leftSum - i * avg;
            rightNeed = (sum - leftSum - arr[i]) - (arr.length - i - 1) * avg;

            // 开始结算
            if (leftNeed < 0 && rightNeed < 0) {
                ans = Math.max(ans, Math.abs(leftNeed) + Math.abs(rightNeed));
            } else {
                ans = Math.max(ans, Math.max(Math.abs(leftNeed), Math.abs(rightNeed)));
            }

            leftSum += arr[i];
        }

        return ans;
    }
}
