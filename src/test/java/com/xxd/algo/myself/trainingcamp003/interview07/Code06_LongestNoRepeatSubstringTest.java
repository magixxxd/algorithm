package com.xxd.algo.myself.trainingcamp003.interview07;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 22:32
 * @description:
 */
public class Code06_LongestNoRepeatSubstringTest {


    public static int maxUnique2(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        HashMap<Character, Integer> map = new HashMap<>();

        char[] chas = str.toCharArray();
        int[] dp = new int[chas.length];
        dp[0] = 1;
        map.put(chas[0], 0);
        int max = dp[0];
        for (int i = 1; i < chas.length; i++) {
            // 上一次出现的位置
            if (!map.containsKey(chas[i])) { // 没有出现过，那么必然我的值就是前面的加1
                dp[i] = dp[i - 1] + 1;
            } else { // 出现了
                Integer pre = map.get(chas[i]);
                if (pre < i - dp[i - 1]) {
                    dp[i] = dp[i - 1] + 1;
                } else {
                    dp[i] = i - pre;
                }
            }
            map.put(chas[i], i);
            max = Math.max(max, dp[i]);
        }

        return max;
    }

    public static void main(String[] args) {
        String str = "pwwkew";
        //String str = getRandomString(20);
        System.out.println(str);
        System.out.println(maxUnique2(str));
    }
}
