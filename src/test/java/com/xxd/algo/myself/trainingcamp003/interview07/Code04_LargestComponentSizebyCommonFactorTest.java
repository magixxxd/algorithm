package com.xxd.algo.myself.trainingcamp003.interview07;


import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-21 07:44
 * @description:
 */
public class Code04_LargestComponentSizebyCommonFactorTest {

    public static int largestComponentSize1(int[] arr) {
        int N = arr.length;
        UnionFind set = new UnionFind(N);
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                if (gcd(arr[i], arr[j]) != 1) {
                    set.union(i, j);
                }
            }
        }
        return set.maxSize();
    }

    public static int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    /**
     *
     * @param arr
     * @return
     */
    public static int largestComponentSize2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int N = arr.length;
        UnionFind unionFind = new UnionFind(N);
        Map<Integer, Integer> fatorsMap = new HashMap<>();
        for (int i = 0; i < N; i++) {
            int num = arr[i];
            int limit = (int) Math.sqrt(num);
            for (int j = 1; j <= limit; j++) {
                if (num % j == 0) {
                    if (j != 1) {
                        if (!fatorsMap.containsKey(j)) {
                            fatorsMap.put(j, i);
                        } else {
                            unionFind.union(fatorsMap.get(j), i);
                        }
                    }

                    int other = num / j;
                    if (other != 1) {
                        if (!fatorsMap.containsKey(other)) {
                            fatorsMap.put(other, i);
                        } else {
                            unionFind.union(fatorsMap.get(other), i);
                        }
                    }
                }
            }
        }

        return unionFind.maxSize();
    }

    /**
     * 并查集结构
     */
    public static class UnionFind {
        private int[] parents;
        private int[] sizes;
        private int[] help;

        public UnionFind(int N) {
            parents = new int[N];
            sizes = new int[N];
            help = new int[N];
            for (int i = 0; i < N; i++) {
                parents[i] = i;
                sizes[i] = 1;
            }
        }


        public int maxSize() {
            int ans = 0;
            for (int size : sizes) {
                ans = Math.max(ans, size);
            }

            return ans;
        }

        public void union(int i, int j) {
            // 找到i 的代表节点，也就是往上到不能往上
            int f1 = find(i);

            // 找到j 的代表节点，也就是往上到不能往上
            int f2 = find(j);

            if (f1 != f2) { // 把小的那一坨 去连接大的
                int big = sizes[f1] >= sizes[f2] ? f1 : f2;
                int small = big == f1 ? f2 : f1;

                parents[small] = big;
                sizes[big] = sizes[f1] + sizes[f2];
            }
        }

        /**
         * 给你一个节点，找到那个代表节点
         *
         * @param i
         * @return
         */
        private int find(int i) {
            int hi = 0;

            while (i != parents[i]) { //  往上窜 到不能往上为止
                // 收集沿路的值，为我下面扁平化铺路
                help[hi++] = i;
                i = parents[i];
            }
            for (hi--; hi >= 0; hi--) {
                parents[help[hi]] = i;
                hi--;
            }

            return i;
        }
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 7, 8, 12, 17};
        System.out.println(largestComponentSize1(arr));
        System.out.println(largestComponentSize2(arr));
    }
}