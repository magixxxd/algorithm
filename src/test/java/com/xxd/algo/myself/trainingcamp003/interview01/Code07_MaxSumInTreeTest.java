package com.xxd.algo.myself.trainingcamp003.interview01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-26 09:50
 * @description:
 */
public class Code07_MaxSumInTreeTest {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int val) {
            value = val;
        }
    }

    public static int max = Integer.MIN_VALUE;

    /**
     * 路劲必须是跟结点触发，到叶节点为止，返回最大路劲和
     *
     * @return
     */
    public static int question1(Node head) {
        if (head == null) {
            return 0;
        }

        return process1(head);
    }

    /**
     * 获取以 head 为头的最大值
     * @param head
     * @return
     */
    private static int process1(Node head) {
        if (head.left == null && head.right == null) {
            return head.value;
        }

        int next = Integer.MIN_VALUE;

        if (head.left != null) {
            next = process1(head.left);
        }

        if (head.right != null) {
            next = Math.max(next, process1(head.right));
        }

        return next + head.value;
    }

    static class Info {
        // 整棵树的最大值
        int allTreeMaxSum;

        // 以当前为头的最大值
        int fromHeadMaxSum;

        public Info(int allTreeMaxSum, int fromHeadMaxSum) {
            this.allTreeMaxSum = allTreeMaxSum;
            this.fromHeadMaxSum = fromHeadMaxSum;
        }
    }


    /**
     * 路劲可以从任何节点出发，但必须往下走到任何节点，返回最大路径和
     *  1）X无关的时候， 1，左树上的整体最大路径和  2，右树上的整体最大路径和
     *  2) X有关的时候 3. x自己 4. x往左走 5，x往右走
     *
     * @param head
     */
    public static int question2(Node head) {
        if (head == null) {
            return 0;
        }

        return process2(head).allTreeMaxSum;
    }

    private static Info process2(Node x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process2(x.left);
        Info rightInfo = process2(x.right);

        int p1 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p1 = leftInfo.allTreeMaxSum;
        }

        int p2 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p2 = rightInfo.allTreeMaxSum;
        }

        int p3 = x.value;

        int p4 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p4 = leftInfo.fromHeadMaxSum;
        }

        int p5 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p5 = rightInfo.fromHeadMaxSum;
        }

        // 所有可能性最大的 就是整棵树的最大值
        int allTreeMaxSize =  Math.max(Math.max(Math.max(p1, p2), p3), Math.max(p4, p5));
        // 以当前头为最大值，就是x自己，或者左树 右树出发的最大值
        int fromHeadMaxSum = Math.max(Math.max(p3, p4), p5);

        return new Info(allTreeMaxSize, fromHeadMaxSum);
    }

    /**
     * 路劲可以从任何节点出发，到任何节点，返回最大路劲和
     *  1）X无关的时候， 1， 左树上的整体最大路径和 2， 右树上的整体最大路径和
     *  2) X有关的时候 3， x自己  4， x往左走 5，x往右走 6, 既往左，又往右
     * @param head
     * @return
     */
    public static int question3(Node head) {
        if (head == null) {
            return 0;
        }

        return process3(head).allTreeMaxSum;
    }

    private static Info process3(Node x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process3(x.left);
        Info rightInfo = process3(x.right);

        int p1 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p1 = leftInfo.allTreeMaxSum;
        }

        int p2 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p2 = rightInfo.allTreeMaxSum;
        }

        int p3 = x.value;

        int p4 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p4 = leftInfo.fromHeadMaxSum;
        }

        int p5 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p5 = rightInfo.fromHeadMaxSum;
        }

        int p6 = Integer.MIN_VALUE;
        if (leftInfo != null && rightInfo != null) {
            p6 = x.value + leftInfo.fromHeadMaxSum + rightInfo.fromHeadMaxSum;
        }

        // 所有可能性最大的 就是整棵树的最大值
        int allTreeMaxSum = Math.max(Math.max(Math.max(p1, p2), p3), Math.max(Math.max(p4, p5), p6));

        // 以当前头为最大值，就是x自己，或者左树 右树出发的最大值
        int fromHeadMaxSum = Math.max(Math.max(p3, p4), p5);

        return new Info(allTreeMaxSum, fromHeadMaxSum);
    }


    /**
     * 任何一个节点 到叶子节点的 最大路劲和
     *
     * @param head
     */
    public static int question4(Node head) {
        if (head == null) {
            return 0;
        }
        return process4(head);
    }

    /**
     * process4 代表求 以当前节点到任意叶子节点的最大值
     * @param head
     * @return
     */
    private static int process4(Node head) {
        // 到了叶子节点就去更新最大值，否则返回当前的值
        if (head.left == null && head.right == null) {
            max = Math.max(max, head.value);
            return head.value;
        }

        int nextMax = 0;

        if (head.left != null) {
            nextMax = question4(head.left);
        }

        if (head.right != null) {
            nextMax = Math.max(nextMax, question4(head.right));
        }

        int ans = head.value + nextMax;
        max = Math.max(max, ans);
        return ans;
    }

    public static void main(String[] args) {
        Node head = new Node(-2);
        head.left = new Node(12);
        head.left.left = new Node(-11);
        head.left.right = new Node(12);

        head.right = new Node(1);
        head.right.left = new Node(8);
        head.right.right = new Node(3);

        System.out.println(question4(head));
        System.out.println(max);
    }
}