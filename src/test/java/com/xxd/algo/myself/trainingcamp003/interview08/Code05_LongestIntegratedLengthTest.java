package com.xxd.algo.myself.trainingcamp003.interview08;

import java.util.Arrays;
import java.util.HashSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-01 07:59
 * @description:
 */
public class Code05_LongestIntegratedLengthTest {

    public static int getLIL1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int len = 0;
        for (int start = 0; start < arr.length; start++) {
            for (int end = start; end < arr.length; end++) {
                if (isIntegrated(arr, start, end)) {
                    len = Math.max(len, end - start + 1);
                }
            }
        }

        return len;
    }

    private static boolean isIntegrated(int[] arr, int left, int right) {
        int[] newArr = Arrays.copyOfRange(arr, left, right + 1);
        Arrays.sort(newArr);

        for (int i = 1; i < newArr.length; i++) {
            if (newArr[i - 1] != newArr[i] - 1) {
                return false;
            }
        }

        return true;
    }

    public static int getLTL(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int len = 0;
        int max = 0;
        int min = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int start = 0; start < arr.length; start++) {
            set.clear();
            max = Integer.MIN_VALUE;
            min = Integer.MAX_VALUE;
            for (int end = start; end < arr.length; end++) {
                if (set.contains(arr[end])) {
                    break;
                }

                min = Math.min(min, arr[end]);
                max = Math.max(max, arr[end]);
                set.add(arr[end]);

                if (max - min == end - start) {
                    len = Math.max(max, end - start + 1);
                }
            }
        }

        return len;
    }
}