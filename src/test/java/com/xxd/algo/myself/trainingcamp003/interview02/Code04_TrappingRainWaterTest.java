package com.xxd.algo.myself.trainingcamp003.interview02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-31 23:03
 * @description:
 */
public class Code04_TrappingRainWaterTest {

    /**
     * 暴力递归方法
     *
     * @param arr
     * @return
     */
    public static int water1(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }

        int maxLeft = 0;
        int maxRight = 0;
        int ans = 0;
        for (int i = 1; i < arr.length - 1; i++) {
            maxLeft = Integer.MIN_VALUE;
            for (int j = 0; j < i; j++) {
                maxLeft = Math.max(maxLeft, arr[j]);
            }

            maxRight = Integer.MIN_VALUE;
            for (int j = i + 1; j < arr.length; j++) {
                maxRight = Math.max(maxRight, arr[j]);
            }
            ans += Math.max(Math.min(maxLeft, maxRight) - arr[i], 0);
        }
        return ans;
    }

    /**
     * 辅助数组的搞法
     *
     * @param arr
     * @return
     */
    public static int water2(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }

        int[] leftMax = new int[arr.length];
        leftMax[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            leftMax[i] = Math.max(arr[i], leftMax[i - 1]);
        }


        int[] rightMax = new int[arr.length];
        rightMax[arr.length - 1] = arr[arr.length - 1];
        for (int i = arr.length - 2; i >= 0; i--) {
            rightMax[i] = Math.max(arr[i], rightMax[i + 1]);
        }


        int ans = 0;
        int maxLeft;
        int maxRight;
        for (int i = 1; i < arr.length - 1; i++) {
            maxLeft = leftMax[i - 1];
            maxRight = rightMax[i + 1];

            ans += Math.max(Math.min(maxLeft, maxRight) - arr[i], 0);
        }
        return ans;
    }

    /**
     * 双向指针
     *
     * @param arr
     * @return
     */
    public static int water3(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }

        int L = 1;
        int R = arr.length - 2;
        int leftMax = arr[0];
        int rightMax = arr[arr.length - 1];
        int ans = 0;
        while (L <= R) {
            if (leftMax > rightMax) {
                ans += Math.max(rightMax - arr[R], 0);
                rightMax = Math.max(arr[R--], rightMax);
            } else {
                ans += Math.max(leftMax - arr[L], 0);
                leftMax = Math.max(arr[L++], leftMax);
            }

        }

        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {4, 5, 1, 3, 2, 3, 5, 7, 78, 12};
        System.out.println(water1(arr));
        System.out.println(water2(arr));
        System.out.println(water3(arr));
    }
}
