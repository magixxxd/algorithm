package com.xxd.algo.myself.trainingcamp003.interview04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-08 21:08
 * @description:
 */
public class Code05_EnvelopesProblemTest {

    /**
     * 信封类
     */
    public static class Envelope {
        int l;
        int h;

        public Envelope(int l, int h) {
            this.l = l;
            this.h = h;
        }

        @Override
        public String toString() {
            return "Envelope{" +
                    "l=" + l +
                    ", h=" + h +
                    '}';
        }
    }

    public static class EnvelopeComparator implements Comparator<Envelope> {

        @Override
        public int compare(Envelope o1, Envelope o2) {
            if (o1.l == o2.l) { // 长度一样，高度从大到小
                return o2.h - o1.h;
            } else { // 长度不一样
                return o1.l - o2.l;
            }
        }
    }

    /**
     * 获取信封最大能装多少个
     * @param matrix
     * @return
     */
    public static int maxEnvelopes(int[][] matrix) {
        Envelope[] envelopes = getSortedEnvelopes(matrix);

        // 把这批信封的高度取出来, 求最长递增子序列问题
        int[] dp = new int[envelopes.length];
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < envelopes.length; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (envelopes[i].h > envelopes[j].h) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                    max = Math.max(dp[i], max);
                }
            }
        }
        return max;
    }

    /**
     * 获取排好序的信封
     * @param matrix
     * @return
     */
    public static Envelope[] getSortedEnvelopes(int[][] matrix) {
        Envelope[] res = new Envelope[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            res[i] = new Envelope(matrix[i][0], matrix[i][1]);
        }
        Arrays.sort(res, new EnvelopeComparator());
        return res;
    }
    public static void main(String[] args) {
        List<Envelope> list = new ArrayList<>();
        list.add(new Envelope(1, 5));
        list.add(new Envelope(1, 4));
        list.add(new Envelope(1, 6));
        list.add(new Envelope(3, 2));

        list.add(new Envelope(2, 2));
        list.add(new Envelope(2, 3));
        list.add(new Envelope(3, 4));
        list.add(new Envelope(2, 8));
        list.add(new Envelope(2, 1));

        list.add(new Envelope(3, 0));
        list.add(new Envelope(1, 2));
        list.add(new Envelope(1, 3));
        list.add(new Envelope(1, 1));
        list.add(new Envelope(3, 3));

        list.sort(new EnvelopeComparator());

        int[][] test = { { 3, 4 }, { 2, 3 }, { 4, 5 }, { 1, 3 }, { 2, 2 }, { 3, 6 }, { 1, 2 }, { 3, 2 }, { 2, 4 } };
        System.out.println(maxEnvelopes(test));
    }

}
