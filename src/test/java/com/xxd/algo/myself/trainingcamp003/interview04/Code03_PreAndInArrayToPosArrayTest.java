package com.xxd.algo.myself.trainingcamp003.interview04;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-08 20:34
 * @description:
 */
public class Code03_PreAndInArrayToPosArrayTest {
    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int v) {
            value = v;
        }
    }

    /**
     * 前 中 转 后
     *
     * @return
     */
    public static int[] preInToPos1(int[] pre, int[] in) {
        if (pre == null || in == null || pre.length != in.length) {
            return null;
        }

        int N = pre.length;
        int[] pos = new int[N];

        process(pre, 0, N - 1, in, 0, N - 1, pos, 0, N - 1);
        return pos;
    }

    /**
     * 递归处理
     *
     * @param pre
     * @param l1
     * @param r1
     * @param in
     * @param l2
     * @param r2
     * @param pos
     * @param l3
     * @param r3
     */
    private static void process(
            int[] pre, int l1, int r1,
            int[] in, int l2, int r2,
            int[] pos, int l3, int r3) {
        if (l1 > r1) {
            return;
        }
        if (l1 == r1) {
            pos[l3] = pre[l1];
            return;
        }
        pos[r3] = pre[l1];
        int mid = l2;
        while (mid <= r2) { // 在中序数组中找到位置
            if (in[mid] == pre[l1]) {
                break;
            }
            mid++;
        }

        int leftSize = mid - l2;
        // 处理左子树
        process(pre, l1 + 1, l1 + leftSize,
                in, l2, mid - 1,
                pos, l3, l3 + leftSize - 1);

        // 处理右子树
        process(pre, l1 + leftSize + 1, r1,
                in, mid + 1, r2,
                pos, l3 + leftSize, r3 - 1);
    }

    public static void main(String[] args) {
        int[] pre = {1, 2, 4, 5, 3, 6, 7};
        int[] in = {4, 2, 5, 1, 6, 3, 7};
        int[] post = preInToPos1(pre, in);
        System.out.println(Arrays.toString(post));
    }

}
