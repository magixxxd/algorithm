package com.xxd.algo.myself.trainingcamp003.interview07;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-21 19:15
 * @description:
 */
public class Code05_RemoveDuplicateLettersLessLexiTest {

    // 在str中，每种字符都要保留一个，让最后的结果，字典序最小 ，并返回
    public static String removeDuplicateLetters1(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }

        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            Integer counter = map.get(str.charAt(i));
            if (counter == null) {
                map.put(str.charAt(i), 1);
            } else {
                map.put(str.charAt(i), counter + 1);
            }
        }

        int minIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            minIndex = str.charAt(i) < str.charAt(minIndex) ? i : minIndex;

            Integer counter = map.get(str.charAt(i));
            counter = counter - 1;
            if (counter == 0) {
                break;
            }
        }


        return str.charAt(minIndex) +
                removeDuplicateLetters1(str.substring(minIndex + 1).replaceAll("" + str.charAt(minIndex), ""));
    }

    public static void main(String[] args) {
        String str = "dcacadbbacdbcacb";
        // String str = "dcdbbacdcb";
        System.out.println(removeDuplicateLetters1(str));
    }
}
