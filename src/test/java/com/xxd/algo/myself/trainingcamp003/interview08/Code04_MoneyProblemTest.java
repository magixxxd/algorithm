package com.xxd.algo.myself.trainingcamp003.interview08;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-21 20:17
 * @description:
 */
public class Code04_MoneyProblemTest {

    /**
     * @param d       怪兽的能力
     * @param p       怪兽要求的钱
     * @param ability 当前能力
     * @param index   当前的关卡
     * @return 目前，你的能力是ability，你来到了index号怪兽的面前，如果要通过后续所有的怪兽，
     * 请返回需要花的最少钱数
     */
    public static long process(int[] d, int[] p, int ability, int index) {
        if (index == d.length) {
            return 0;
        }

        // 必须贿赂当前怪兽
        if (ability < d[index]) {
            return p[index] + process(d, p, ability + d[index], index + 1);
        } else {
            // 可以选择贿赂，可以选择直接通过
            return Math.min(
                    p[index] + process(d, p, ability + d[index], index + 1),  // 贿赂
                    process(d, p, ability, index + 1));
        }
    }

    public static long func1(int[] d, int[] p) {
        return process(d, p, 0, 0);
    }
    // dp[i][j] 含义，代表你来到了当前i关，且拥有的能力是 j 需要通过 花费最少的钱
    public static long func2(int[] d, int[] p) {
        int sum = 0;
        for (int num : d) {
            sum += num;
        }
        long[][] dp = new long[d.length + 1][sum + 1];
        for (int i = 0; i <= sum; i++) {
            dp[0][i] = 0;
        }
        for (int cur = d.length - 1; cur >= 0; cur--) {
            for (int hp = 0; hp <= sum; hp++) {
                if (hp + d[cur] > sum) { // 我获取的能力已经超过最大的能力了，就没有在计算了
                    continue;
                }
                if (hp < d[cur]) { // 打不过，必定要贿赂
                    dp[cur][hp] = p[cur] + dp[cur + 1][hp + d[cur]];
                } else { // 打的过，不一定贿赂
                    dp[cur][hp] = Math.min(p[cur] + dp[cur + 1][hp + d[cur]], dp[cur + 1][hp]);
                }
            }
        }
        return dp[0][0];
    }
    public static void main(String[] args) {
        int[] d = {4, 5, 1, 11};
        int[] p = {10, 11, 7, 9};
        System.out.println(func1(d, p));
        System.out.println(func2(d, p));
    }
}
