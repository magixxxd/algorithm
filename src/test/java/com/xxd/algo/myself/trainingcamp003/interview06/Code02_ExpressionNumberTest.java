package com.xxd.algo.myself.trainingcamp003.interview06;

public class Code02_ExpressionNumberTest {

    /**
     * 校验这个表达式 是否有效
     * 1） 长度必须是奇数
     * 2） 所有的运算符 都一定是奇数的位置
     *
     * @param exp
     * @return
     */
    public static boolean isValid(char[] exp) {
        if ((exp.length & 1) == 0) { // 偶数
            return false;
        }

        for (int i = 0; i < exp.length; i = i + 2) {
            if ((exp[i] != '1') && (exp[i] != '0')) {
                return false;
            }
        }

        for (int i = 1; i < exp.length; i = i + 2) {
            if ((exp[i] != '&') && (exp[i] != '|') && (exp[i] != '^')) {
                return false;
            }
        }

        return true;
    }

    /**
     * 暴力递归
     * 尝试模型，范围上尝试
     *
     * @param express
     * @param desired
     * @return
     */
    public static int num1(String express, boolean desired) {
        if (express == null || express.equals("")) {
            return 0;
        }

        char[] exp = express.toCharArray();
        if (!isValid(exp)) {
            return 0;
        }

        return f(exp, desired, 0, exp.length - 1);
    }

    /**
     * str[L..R] 上必须返回desired 的方法数
     *
     * @param str
     * @param desired
     * @param L
     * @param R
     * @return
     */
    public static int f(char[] str, boolean desired, int L, int R) {
        if (L == R) {
            if (str[L] == '1') { //返回true的1种 返回false 的0 种
                return desired ? 1 : 0;
            } else { // 返回true的0种 返回false 的一种
                return desired ? 0 : 1;
            }
        }

        int res = 0;
        if (desired) {// 期待返回true 的方法数
            for (int i = L + 1; i < R; i += 2) {
                switch (str[i]) {
                    case '&':
                        res += f(str, true, L, i - 1) * f(str, true, i + 1, R);
                        break;

                    case '|':
                        res += f(str, true, L, i - 1) * f(str, false, i + 1, R);
                        res += f(str, false, L, i - 1) * f(str, true, i + 1, R);
                        res += f(str, true, L, i - 1) * f(str, true, i + 1, R);
                        break;

                    case '^':
                        res += f(str, true, L, i - 1) * f(str, false, i + 1, R);
                        res += f(str, false, L, i - 1) * f(str, true, i + 1, R);
                        break;
                }
            }
        } else { // 期待为false
            for (int i = L + 1; i < R; i += 2) {
                switch (str[i]) {
                    case '&':
                        res += f(str, false, L, i - 1) * f(str, true, i + 1, R);
                        res += f(str, true, L, i - 1) * f(str, false, i + 1, R);
                        res += f(str, false, L, i - 1) * f(str, false, i + 1, R);
                        break;

                    case '|':
                        res += f(str, false, L, i - 1) * f(str, false, i + 1, R);
                        break;

                    case '^':
                        res += f(str, true, L, i - 1) * f(str, true, i + 1, R);
                        res += f(str, false, L, i - 1) * f(str, false, i + 1, R);
                        break;
                }
            }
        }
        return res;
    }

    /**
     * 动态规划
     *
     * @param express
     * @param desired
     * @return
     */
    public static int num2(String express, boolean desired) {
        if (express == null || express.equals("")) {
            return 0;
        }

        char[] exp = express.toCharArray();
        if (!isValid(exp)) {
            return 0;
        }

        int[][] t = new int[exp.length][exp.length];
        int[][] f = new int[exp.length][exp.length];
        t[0][0] = exp[0] == '0' ? 0 : 1;
        f[0][0] = exp[0] == '1' ? 0 : 1;

        for (int i = 2; i < exp.length; i += 2) {
            t[i][i] = exp[i] == '0' ? 0 : 1;
            f[i][i] = exp[i] == '1' ? 0 : 1;
            for (int j = i - 2; j >= 0; j -= 2) {
                for (int k = j; k < i; k += 2) {
                    if (exp[k + 1] == '&') {
                        t[j][i] += t[j][k] * t[k + 2][i];
                        f[j][i] += (f[j][k] + t[j][k]) * f[k + 2][i] + f[j][k] * t[k + 2][i];
                    } else if (exp[k + 1] == '|') {
                        t[j][i] += (f[j][k] + t[j][k]) * t[k + 2][i] + t[j][k] * f[k + 2][i];
                        f[j][i] += f[j][k] * f[k + 2][i];
                    } else {
                        t[j][i] += f[j][k] * t[k + 2][i] + t[j][k] * f[k + 2][i];
                        f[j][i] += f[j][k] * f[k + 2][i] + t[j][k] * t[k + 2][i];
                    }
                }
            }
        }
        return desired ? t[0][t.length - 1] : f[0][f.length - 1];
    }


    public static void main(String[] args) {
        String express = "1^0&0|1&1^0&0^1|0|1&1";
        boolean desired = true;
        System.out.println(num1(express, desired));
        System.out.println(num2(express, desired));
    }
}