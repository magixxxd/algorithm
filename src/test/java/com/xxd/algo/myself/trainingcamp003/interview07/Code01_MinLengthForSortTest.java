package com.xxd.algo.myself.trainingcamp003.interview07;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 22:01
 * @description:
 */
public class Code01_MinLengthForSortTest {

    public static int getMinLength(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }

        int max = arr[0];
        int noMaxIndex = -1;
        for (int i = 1; i < arr.length; i++) {
            if (max > arr[i]) {
                noMaxIndex = i;
            } else {
                max = Math.max(max, arr[i]);
            }
        }

        int min = arr[arr.length - 1];
        int noMinIndex = -1;
        for (int i = arr.length - 2; i >= 0; i++) {
            if (arr[i] <= min) {
                min = Math.min(min, arr[i]);
            } else {
                noMaxIndex = i;
            }
        }

        return noMaxIndex - noMinIndex - 1;
    }
}
