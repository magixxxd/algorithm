package com.xxd.algo.myself.trainingcamp003.interview02;

public class Code06_PrintUniquePairAndTriadTest {

    /**
     * 打印所有的二元组
     *
     * @param arr
     * @param k
     */
    public static void printUniquePair(int[] arr, int k) {
        if (arr == null || arr.length < 2) {
            return;
        }

        int L = 0;
        int R = arr.length - 1;

        while (L < R) {
            if (arr[L] + arr[R] < k) { // 那就只能加小点了
                L++;
            } else if (arr[L] + arr[R] == k) {
                // 如果当前的数跟上一个的数不一样的话，那就打印下
                if (L - 1 > 0 && arr[L] != arr[L - 1]) {
                    System.out.println(arr[L] + " - " + arr[R]);
                }
                L++;
            } else if (arr[L] + arr[R] > k) {
                R--;
            }
        }
    }

    /**
     * 打印所有的三元组
     *
     * @param arr
     * @param k
     */
    public static void printUniqueTriad(int[] arr, int k) {
        if (arr == null || arr.length < 3) {
            return;
        }

        for (int i = 0; i < arr.length; i++) {
            int aim = k - arr[i];
            printRest(arr, i, i + 1, arr.length - 1, aim);
        }
    }

    public static void printRest(int[] arr, int f, int l, int r, int k) {
        while (l < r) {
            if (arr[l] + arr[r] < k) {
                l++;
            } else if (arr[l] + arr[r] > k) {
                r--;
            } else {
                if (l == f + 1 || arr[l - 1] != arr[l]) {
                    System.out.println(arr[f] + "," + arr[l] + "," + arr[r]);
                }
                l++;
                r--;
            }
        }
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i != arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int sum = 10;
        int[] arr1 = {-8, -4, -3, 0, 1, 2, 4, 5, 8, 9};
        printArray(arr1);
        System.out.println("====");
        printUniquePair(arr1, sum);
        System.out.println("====");
        printUniqueTriad(arr1, sum);

    }

}
