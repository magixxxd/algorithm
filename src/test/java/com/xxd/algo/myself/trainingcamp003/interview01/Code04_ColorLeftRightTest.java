package com.xxd.algo.myself.trainingcamp003.interview01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-25 16:29
 * @description:
 */
public class Code04_ColorLeftRightTest {

    // RGRGR -> RRRGG
    public static int minPaint(String s) {
        char[] chars = s.toCharArray();

        int[] left = new int[chars.length]; // 辅助数组，左边有多少个G
        int[] right = new int[chars.length];// 辅助数组，右边有多少个R
        for (int i = 0; i < chars.length; i++) {
            if (i == 0) {
                left[i] = chars[i] == 'G' ? 1 : 0;
            } else {
                left[i] = chars[i] == 'G' ? (left[i - 1] + 1) : left[i - 1];
            }
        }

        for (int i = chars.length - 1; i >= 0 ; i--) {
            if (i == chars.length - 1) {
                right[i] = chars[i] == 'R' ? 1 : 0;
            } else {
                right[i] = chars[i] == 'R' ? right[i + 1] + 1 : right[i + 1];
            }
        }

        // 全部涂G
        int res = left[chars.length - 1];

        // 全部涂R
        res = Math.min(res, right[0]);

        for (int i = 0; i < chars.length - 1; i++) {
            // 左边涂R ，右边涂G
            res = Math.min(res, left[i] + right[i + 1]);
        }

        return res;
    }

    public static void main(String[] args) {
        String test = "GGGGGR";
        System.out.println(minPaint(test));
    }
}
