package com.xxd.algo.myself.trainingcamp003.interview04;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-10 09:03
 * @description:
 */
public class Code07_SubMatrixMaxSumTest {


    public static int maxSum(int[][] m) {
        if (m == null || m.length == 0 || m[0].length == 0) {
            return 0;
        }

        int cur = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < m.length; i++) {
            for (int j = i; j < m.length; j++) { // i ~ j 行
                cur = 0;
                for (int k = 0; k < m[0].length; k++) { // 0 到最后一列
                    for (int l = j; l >= 0; l--) {
                        cur += m[l][k];
                    }
                    max = Math.max(max, cur);
                    cur = cur < 0 ? 0 : cur;
                }
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {-90, 48, 78},
                {64, -40, 64},
                {-81, -7, 66},
                {1999, 10, -10}
        };
        System.out.println(maxSum(matrix));
    }
}
