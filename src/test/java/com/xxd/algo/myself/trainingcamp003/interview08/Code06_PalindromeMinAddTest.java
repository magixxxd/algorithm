package com.xxd.algo.myself.trainingcamp003.interview08;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-23 21:09
 * @description:
 */
public class Code06_PalindromeMinAddTest {

    public static int f(String str, int L, int R) {
        if (L < 0 || R >= str.length() || L >= R) {
            return 0;
        }

        int ans = Integer.MAX_VALUE;

        if (str.charAt(L) == str.charAt(R)) {
            ans = Math.min(f(str, L + 1, R - 1), ans);
        }

        ans = Math.min(Math.min(ans, f(str, L, R - 1) + 1), f(str, L + 1, R) + 1);
        return ans;
    }

    public static int process(String str) {
        return f(str, 0, str.length() - 1);
    }

    public static int getDp(String str) {
        if (str == null || str.length() == 1) {
            return 0;
        }

        int[][] dp = new int[str.length()][str.length()];

        for (int col = 1; col < str.length(); col++) {
            dp[col - 1][col] = str.charAt(col) == str.charAt(col - 1) ? 0 : 1;
        }

        for (int i = 2; i < str.length(); i++) {
            for (int row = 0; row < str.length() - i; row++) {
                int col = row + i;
                if (str.charAt(col) == str.charAt(row)) {
                    dp[row][col] = dp[row + 1][col - 1];
                } else {
                    dp[row][col] = Math.min(dp[row][col - 1], dp[row + 1][col]) + 1;
                }
            }
        }
        return dp[0][str.length() - 1];
    }

    public static void main(String[] args) {
        String str = "AB1CD2EFG3H43IJK2L1MN";

        System.out.println(process("AB1CD2EFG3H43IJK2L1MN"));
        System.out.println(getDp("AB1CD2EFG3H43IJK2L1MN"));
    }
}
