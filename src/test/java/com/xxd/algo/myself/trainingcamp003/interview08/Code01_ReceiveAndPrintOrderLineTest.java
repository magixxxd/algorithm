package com.xxd.algo.myself.trainingcamp003.interview08;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-24 07:39
 * @description:
 */
public class Code01_ReceiveAndPrintOrderLineTest {

    public static class Node {
        public String info;
        public Node next;

        public Node(String str) {
            info = str;
        }
    }


    public static class MessageBox {
        private Map<Integer, Node> head;
        private Map<Integer, Node> tail;
        int waitPoint;
        public MessageBox() {
            head = new HashMap<>();
            tail = new HashMap<>();
            waitPoint = 1;
        }

        public void receive(int num, String info) {
            if (num < 1) {
                return;
            }

            Node cur = new Node(info);
            head.put(num, cur);
            tail.put(num, cur);

            Node last = tail.get(num - 1);
            if (last != null) {
                last.next = cur;
                tail.remove(num - 1);
                head.remove(num);
            }

            Node next = head.get(num + 1);
            if (next != null) {
                cur.next = next;
                tail.remove(num);
                head.remove(num + 1);
            }

            if (num == waitPoint) {
                print();
            }
        }

        private void print() {
            Node node = head.get(waitPoint);
            head.remove(waitPoint);

            while (node != null) {
                System.out.println(node.info + "  ");
                waitPoint++;
                node = node.next;
            }

            tail.remove(waitPoint - 1);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        // MessageBox only receive 1~N
        MessageBox box = new MessageBox();

        box.receive(2,"B"); // - 2"
        box.receive(1,"A"); // 1 2 -> print, trigger is 1

        box.receive(4,"D"); // - 4
        box.receive(5,"E"); // - 4 5
        box.receive(7,"G"); // - 4 5 - 7
        box.receive(8,"H"); // - 4 5 - 7 8
        box.receive(6,"F"); // - 4 5 6 7 8
        box.receive(3,"C"); // 3 4 5 6 7 8 -> print, trigger is 3

        box.receive(9,"I"); // 9 -> print, trigger is 9

        box.receive(10,"J"); // 10 -> print, trigger is 10

        box.receive(12,"L"); // - 12
        box.receive(13,"M"); // - 12 13
        box.receive(11,"K"); // 11 12 13 -> print, trigger is 11

    }
}
