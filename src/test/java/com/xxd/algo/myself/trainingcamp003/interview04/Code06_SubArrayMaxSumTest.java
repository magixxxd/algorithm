package com.xxd.algo.myself.trainingcamp003.interview04;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-08 21:28
 * @description:
 */
public class Code06_SubArrayMaxSumTest {
    public static int maxSum(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int cur = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            cur += arr[i];
            max = Math.max(cur, max);
            cur = cur < 0 ? 0 : cur;
        }

        return max;
    }
}
