package com.xxd.algo.myself.trainingcamp003.interview03;

public class Code02_SnacksWaysTest {

    public static int ways1(int[] arr, int w) {
        // arr[0...]
        return process(arr, 0, w);
    }

    // 从左往右的经典模型
    // 还剩的容量是rest，arr[index...]自由选择，
    // 返回选择方案
    // index ： 0～N
    // rest : 0~w
    public static int process(int[] arr, int index, int rest) {
        if (rest < 0) {
            return -1;
        }

        if (index == arr.length) {
            return 1;
        }

        // 选择当前的零食
        int p1 = process(arr, index + 1, rest - arr[index]);

        // 不选择当前的零食得到的结果
        int p2 = process(arr, index + 1, rest);

        return (p1 != -1 ? p1 : 0) + p2;
    }

    /**
     * 4,3,2,9 10
     * 动态规划，int[][] dp dp[index][rest] index 的变化范围应该是 0 ~ 4
     * rest的有效变化范围应该是 0 ~ 10，那么答案其实就是dp[0][10]
     *
     * @param arr
     * @param w
     * @return
     */
    public static int ways2(int[] arr, int w) {
        int N = arr.length;
        int[][] dp = new int[N + 1][w + 1];

        for (int i = 0; i <= w; i++) {
            dp[N][i] = 1;
        }

        for (int i = N - 1; i >= 0; i--) { // 倒数第二行开始
            for (int j = 0; j <= w; j++) {
                dp[i][j] = dp[i + 1][j] + ((j - arr[i] >= 0) ? dp[i + 1][j - arr[i]] : 0);
            }
        }

        return dp[0][w];
    }

    public static int ways3(int[] arr, int w) {
        int N = arr.length;
        int[][] dp = new int[N][w + 1];
        for (int i = 0; i < N; i++) {
            dp[i][0] = 1;
        }
        if (arr[0] <= w) {
            dp[0][arr[0]] = 1;
        }
        for (int i = 1; i < N; i++) {
            for (int j = 1; j <= w; j++) {
                dp[i][j] = dp[i - 1][j] + ((j - arr[i]) >= 0 ? dp[i - 1][j - arr[i]] : 0);
            }
        }
        int ans = 0;
        for (int j = 0; j <= w; j++) {
            ans += dp[N - 1][j];
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {4, 3, 2, 9, 3, 2, 1, 2};
        int w = 8;
        System.out.println(ways1(arr, w));
        System.out.println(ways2(arr, w));
        System.out.println(ways3(arr, w));

    }

}
