package com.xxd.algo.myself.trainingcamp003.interview01;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-25 18:42
 * @description:
 */
public class Code06_MakeNoTest {

    public static int[] makeNo(int size) {
        if (size == 1) {
            return new int[]{1};
        }

        int halfSize = (size + 1) / 2;
        int[] base = makeNo(halfSize);
        int index = 0;

        int[] ans = new int[size];
        for (; index < base.length; index++) {
            ans[index] = 2 * base[index] - 1;
        }

        for (int i = 0; index < size; index++, i++) {
            ans[index] = 2 * base[i];
        }

        return ans;
    }


    public static void main(String[] args) {
        System.out.println(Arrays.toString(makeNo(5)));
    }
}
