package com.xxd.algo.myself.trainingcamp003.interview08;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-24 07:56
 * @description:
 */
public class Code02_MoneyWaysTest {

    /**
     * @param arbitrary 普通币
     * @param onlyone   纪念币
     * @param money     要拼的钱数
     * @return
     */
    public static int moneyWays(int[] arbitrary, int[] onlyone, int money) {
        if (money < 0) {
            return 0;
        }
        if ((arbitrary == null || arbitrary.length == 0) && (onlyone == null || onlyone.length == 0)) {
            return money == 0 ? 1 : 0;
        }
        // 任意张 的数组， 一张的数组，不可能都没有
        int[][] dparb = getDpArb(arbitrary, money);
        int[][] dpone = getDpOne(onlyone, money);
        if (dparb == null) { // 任意张的数组没有，一张的数组有
            return dpone[dpone.length - 1][money];
        }
        if (dpone == null) { // 任意张的数组有，一张的数组没有
            return dparb[dparb.length - 1][money];
        }
        int res = 0;
        for (int i = 0; i <= money; i++) {
            res += dparb[dparb.length - 1][i] * dpone[dpone.length - 1][money - i];
        }
        return res;
    }

    /**
     * 普通币
     *
     * @param arr
     * @param money
     * @return
     */
    public static int[][] getDpArb(int[] arr, int money) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        int[][] dp = new int[arr.length][money + 1];
        for (int row = 0; row < arr.length; row++) {
            dp[row][0] = 1; // 第一列全部都是0
        }

        for (int col = 1; col <= money; col++) {
            if (col % arr[0] == 0) {
                dp[0][col] = 1;
            }
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j < money; j++) {
                dp[i][j] = dp[i - 1][j]; // 当前的i 使用0张
                if (j - arr[i] > 0) {
                    dp[i][j] += dp[i][j - arr[i]];
                }
            }
        }

        return dp;
    }

    /**
     * 纪念币
     *
     * @param arr
     * @param money
     * @return
     */
    public static int[][] getDpOne(int[] arr, int money) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        int[][] dp = new int[arr.length][money + 1];

        for (int i = 0; i < arr.length; i++) { // 第一列
            dp[i][0] = 1;
        }
        if (arr[0] <= money) { // 第一行
            dp[0][arr[0]] = 1;
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j <= money; j++) {
                dp[i][j] = dp[i - 1][j];  // 不要
                dp[i][j] += j - arr[i] >= 0 ? dp[i - 1][j - arr[i]] : 0; //要
            }
        }

        return dp;
    }

}
