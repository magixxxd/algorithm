package com.xxd.algo.myself.trainingcamp003.interview06;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-13 20:22
 * @description:
 */
public class Code01_MaxEORTest {

    /**
     * 最大子数组的异或和
     * 以当前位置为结尾求最大异或和
     *
     * @param arr 11 1 15 10 13 4
     * @return
     */
    public static int maxXorSubarray1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int[] eor = new int[arr.length];
        eor[0] = arr[0];

        for (int i = 1; i < arr.length; i++) {
            eor[i] = eor[i - 1] ^ arr[i];
        }

        int max = eor[0];

        // 以当前位置结尾去遍历
        for (int j = 1; j < arr.length; j++) {
            for (int i = 0; i <= j; i++) {
                max = Math.max(max, i == 0 ? eor[j] : eor[j] ^ eor[i - 1]);
            }
        }

        return max;
    }

    /**
     * 前缀树的节点
     */
    public static class Node {
        Node[] nexts = new Node[2];

    }

    /**
     * 前缀树结构
     */
    public static class NumTrie {

        private Node head = new Node();

        /**
         * @param newNum
         */
        public void add(int newNum) {
            Node cur = head;

            // 从高位到第位，取出每一位的状态
            for (int move = 31; move >= 0; move--) {
                int path = ((newNum >> move) & 1);

                cur.nexts[path] = cur.nexts[path] == null ? new Node() : cur.nexts[path];
                cur = cur.nexts[path];
            }
        }

        /**
         * 该结构之前收集了一票数字，并且建好了前缀树
         * sum,和 谁 ^ 最大的结果（把结果返回）
         *
         * @param sum
         * @return
         */
        public int maxXor(int sum) {
            Node cur = head;
            int res = 0;
            for (int move = 31; move >= 0; move--) {
                int path = (sum >> move) & 1;
                // 期待的路 path 如果是0 那我肯定期待1 啊 1 期待 0
                int best = move == 31 ? path : (path ^ 1);

                // 实际走的路
                best = cur.nexts[best] != null ? best : (best ^ 1);
                // (path ^ best) 当前位位异或完的结果
                res |= (path ^ best) << move;
                cur = cur.nexts[best];
            }

            return res;
        }
    }

    /**
     * 最大异或和
     *
     * @param arr
     * @return
     */
    public static int maxXorSubarray2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int max = Integer.MIN_VALUE;
        int eor = 0;

        NumTrie trie = new NumTrie();

        trie.add(0);
        for (int i = 0; i < arr.length; i++) {
            eor ^= arr[i];

            max = Math.max(max, trie.maxXor(eor));
            trie.add(eor);
        }

        return max;
    }

    public static void main(String[] args) {
        int[] arr = {11, 1, 15, 10, 13, 4};
        System.out.println(maxXorSubarray1(arr));
        System.out.println(maxXorSubarray2(arr));
    }
}
