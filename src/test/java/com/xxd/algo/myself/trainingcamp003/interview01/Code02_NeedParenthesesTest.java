package com.xxd.algo.myself.trainingcamp003.interview01;

public class Code02_NeedParenthesesTest {

    public static void main(String[] args) {
        String s = "(())()";
        System.out.println(isValid(s));
        System.out.println(needParentheses(s));
        System.out.println(maxLength(s));
    }

    /**
     * 怎么判断只由'('和')'两种字符组成的字符串是完整的。
     *
     * @param s
     * @return
     */
    public static boolean isValid(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }

        int count = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            count += chars[i] == '(' ? 1 : -1;
            if (count < 0) {
                return false;
            }
        }

        return count == 0;
    }

    /**
     * 如果一个可能不完整的字符串， 怎么求至少需要添加多少个括号能让其完整。(()))(
     *
     * @param s
     * @return
     */
    public static int needParentheses(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

        int count = 0;
        int need = 0;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            count += chars[i] == '(' ? 1 : -1;

            if (count < 0) {
                need++;
                count++;
            }
        }
        return count + need;
    }

	/**
	 * 问题3: 求只由'(' 和 ')'两种字符组成的字符串中， 最大完整子串长度。
	 * @param s
	 * @return
	 */
	public static int maxLength(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		char[] str = s.toCharArray();
		int[] dp = new int[str.length];

		int pre = 0;
		int res = 0;
		for (int i = 0; i < str.length; i++) {
			if (str[i] == ')') {
				pre = i - dp[i - 1] - 1; // 与str[i]配对的左括号的位置 pre
				if (pre >= 0 && str[pre] == '(') {
					dp[i] = dp[i - 1] + 2 + (pre > 0 ? dp[pre - 1] : 0);
				}
			}
			res = Math.max(res, dp[i]);
		}

		return res;
	}
}