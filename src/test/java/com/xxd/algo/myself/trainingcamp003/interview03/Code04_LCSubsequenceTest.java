package com.xxd.algo.myself.trainingcamp003.interview03;

public class  Code04_LCSubsequenceTest {

	/**
	 * 暴力递归
	 *
	 * @param str1
	 * @param str2
	 * @return
	 */
	private static int recursion(String str1, String str2) {
		if (str1 == null || str2 == null || str1.equals("") || str2.equals("")) {
			return 0;
		}

		char[] chars1 = str1.toCharArray();
		char[] chars2 = str2.toCharArray();
		return getMaxLength(chars1, chars2, chars1.length - 1, chars2.length - 1);
	}

	/**
	 * 获取最大的公共子序列的长度
	 * @param str1
	 * @param str2
	 * @return
	 */
	private static int getMaxLength(char[] str1, char[] str2, int index1, int index2) {
		if (index1 == 0 && index2 == 0) {
			return str1[0] == str2[0] ? 1 : 0;
		}

		if (index1 == 0) {
			return str1[index1] == str2[index2] ? 1 : getMaxLength(str1, str2, index1, index2 - 1);
		}

		if (index2 == 0) {
			return str1[index1] == str2[index2] ? 1 : getMaxLength(str1, str2, index1 - 1, index2);
		}

		int ans = 0;
		if (str1[index1] == str2[index2]) {
			ans = getMaxLength(str1, str2, --index1, --index2) + 1;
		} else {
			ans = Math.max(
					getMaxLength(str1, str2, index1 - 1, index2),
					getMaxLength(str1, str2, index1, index2 - 1));
		}

		return ans;
	}

	public static int dp(String s1, String s2) {
		char[] str1 = s1.toCharArray();
		char[] str2 = s2.toCharArray();
		int N = str1.length;
		int M = str2.length;

		int[][] dp = new int[N][M];

		dp[0][0] = str1[0] == str2[0] ? 1 : 0;
		for (int j = 1; j < M; j++) {
			dp[0][j] = str1[0] == str2[j] ? 1 : dp[0][j - 1];
		}
		for (int i = 1; i < N; i++) {
			dp[i][0] = str1[i] == str2[0] ? 1 : dp[i - 1][0];
		}
		for (int i = 1; i < N; i++) {
			for (int j = 1; j < M; j++) {
				dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
				if (str1[i] == str2[j]) {
					dp[i][j] = Math.max(dp[i][j], dp[i - 1][j - 1] + 1);
				}
			}
		}
		return dp[N - 1][M - 1];
	}

	public static String lcse(String str1, String str2) {
		if (str1 == null || str2 == null || str1.equals("") || str2.equals("")) {
			return "";
		}
		char[] chs1 = str1.toCharArray();
		char[] chs2 = str2.toCharArray();
		int[][] dp = getdp(chs1, chs2);
		int m = chs1.length - 1;
		int n = chs2.length - 1;
		char[] res = new char[dp[m][n]];
		int index = res.length - 1;
		while (index >= 0) {
			if (n > 0 && dp[m][n] == dp[m][n - 1]) {
				n--;
			} else if (m > 0 && dp[m][n] == dp[m - 1][n]) {
				m--;
			} else {
				res[index--] = chs1[m];
				m--;
				n--;
			}
		}
		return String.valueOf(res);
	}

	public static int[][] getdp(char[] str1, char[] str2) {
		int[][] dp = new int[str1.length][str2.length];
		dp[0][0] = str1[0] == str2[0] ? 1 : 0;
		for (int i = 1; i < str1.length; i++) {
			dp[i][0] = Math.max(dp[i - 1][0], str1[i] == str2[0] ? 1 : 0);
		}
		for (int j = 1; j < str2.length; j++) {
			dp[0][j] = Math.max(dp[0][j - 1], str1[0] == str2[j] ? 1 : 0);
		}
		for (int i = 1; i < str1.length; i++) {
			for (int j = 1; j < str2.length; j++) {
				dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
				if (str1[i] == str2[j]) {
					dp[i][j] = Math.max(dp[i][j], dp[i - 1][j - 1] + 1);
				}
			}
		}
		return dp;
	}

	public static void main(String[] args) {
		String str1 = "A1BC23Z4";
		String str2 = "12O3YU4P";

//		String str1 = "A1BC2D3EFGH45I6JK7LMN";
//		String str2 = "12OPQ3RST4U5V6W7XYZ";
		System.out.println(lcse(str1, str2));
		System.out.println(recursion(str1, str2));
		System.out.println(dp(str1, str2));

	}
}