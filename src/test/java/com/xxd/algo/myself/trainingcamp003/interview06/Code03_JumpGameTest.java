package com.xxd.algo.myself.trainingcamp003.interview06;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-16 15:31
 * @description:
 */
public class Code03_JumpGameTest {

    public static int jump(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int step = 0;
        int cur = 0;
        int next = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (cur < i) {
                step++;
                cur = next;
            }

            next = Math.max(next, i + arr[i]);
        }
        return step;
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 9, 1, 2, 6, 2, 4, 7, 2};
        System.out.println(jump(arr));
    }
}
