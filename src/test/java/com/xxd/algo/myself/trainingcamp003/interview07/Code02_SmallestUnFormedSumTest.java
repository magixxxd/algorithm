package com.xxd.algo.myself.trainingcamp003.interview07;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 22:12
 * @description:
 */
public class Code02_SmallestUnFormedSumTest {

    /**
     * 背包，数组每个值，我可以选择要还是不要，就可以得到所有可能的sum和
     *
     * @param arr
     * @return
     */
    public static int unformedSum1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 1;
        }

        Set<Integer> set = new HashSet<>();
        process(arr, 0, 0, set);

        int min = Integer.MAX_VALUE;
        for (int i = 0; i != arr.length; i++) {
            min = Math.min(min, arr[i]); // 得到数组的最小值
        }
        // set 里面有我的所有答案，我就去枚举我的答案，从最小的开始去枚举，如果发现这个值不在我的答案里面，就返回
        for (int i = min + 1; i != Integer.MIN_VALUE; i++) {
            if (!set.contains(i)) {
                return i;
            }
        }
        return 0;
    }

    /**
     * 背包问题
     *
     * @param arr
     * @param index
     * @param sum
     * @param set
     */
    private static void process(int[] arr, int index, int sum, Set<Integer> set) {
        if (index == arr.length) {
            set.add(sum);
            return;
        }
        process(arr, index + 1, sum, set);
        process(arr, index + 1, sum + arr[index], set);
    }

    public static void main(String[] args) {
        int[] arr = {3, 2, 5};
        System.out.println(unformedSum1(arr));
    }
}
