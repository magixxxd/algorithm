package com.xxd.algo.myself.trainingcamp003.interview04;

import java.util.TreeMap;

public class Code01_GetFolderTreeTest {
	public static class Node {
		String name;
		TreeMap<String, Node> nextMap;

		public Node(String name) {
			this.name = name;
			nextMap = new TreeMap<>();
		}
	}

	public static void print(String[] folderPaths) {
		if (folderPaths == null || folderPaths.length == 0) {
			return;
		}

		Node head = generatorFolderTree(folderPaths);

		printProcess(head, 0);
	}

	/**
	 * 打印
	 * @param node
	 * @param level 层数
	 */
	private static void printProcess(Node node, int level) {
		if (level != 0) {
			// 2 * (level - 1)
			System.out.println(get4nSpace(level) + node.name);
		}
		for (Node next : node.nextMap.values()) {
			printProcess(next, level + 1);
		}
	}

	public static String get4nSpace(int n) {
		String res = "";
		for (int i = 1; i < n; i++) {
			res += "    ";
		}
		return res;
	}

	/**
	 * 生成前缀树
	 * @param folderPaths
	 * @return
	 */
	private static Node generatorFolderTree(String[] folderPaths) {
		Node head = new Node("");

		for (String folderPath : folderPaths) {
			String regex = "\\\\";
			String[] paths = folderPath.split(regex); // a b c
			Node cur = head;
			for (int i = 0; i < paths.length; i++) {
				if (!cur.nextMap.containsKey(paths[i])) {
					cur.nextMap.put(paths[i], new Node(paths[i]));
				}
				cur = cur.nextMap.get(paths[i]);
			}
		}

		return head;
	}

	public static void main(String[] args) {
		String[] arr = {"a\\b\\c","a\\b\\s","b\\c\\s"};
		print(arr);
	}

}
