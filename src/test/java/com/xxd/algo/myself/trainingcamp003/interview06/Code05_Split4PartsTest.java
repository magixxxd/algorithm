package com.xxd.algo.myself.trainingcamp003.interview06;


import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 20:51
 * @description:
 */
public class Code05_Split4PartsTest {

    public static boolean canSplit(int[] arr) {
        if (arr == null || arr.length < 7) {
            return false;
        }

        Map<Integer, Integer> sumIndexMap = new HashMap<>();
        int sum = arr[0];

        for (int i = 1; i < arr.length; i++) {
            sumIndexMap.put(sum, i);
            sum += arr[i];
        }

        int leftSum = arr[0];
        for (int i = 1; i < arr.length - 5; i++) {
            int checkSum = 2 * leftSum + arr[i];
            if (sumIndexMap.containsKey(checkSum)) {
                int s2 = sumIndexMap.get(checkSum);
                checkSum += leftSum + arr[s2];
                if (sumIndexMap.containsKey(checkSum)) { // 100 * 3 + x + y
                    int s3 = sumIndexMap.get(checkSum); // k -> z
                    if (checkSum + arr[s3] + leftSum == sum) {
                        return true;
                    }
                }
            }

            leftSum += arr[i];
        }

        return false;
    }
}
