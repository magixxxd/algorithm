package com.xxd.algo.myself.trainingcamp003.interview06;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 17:41
 * @description:
 */
public class Code04_TopKSumCrossTwoArraysTest {

    static class Node implements Comparable<Node> {
        int x;
        int y;
        int sum;

        public Node(int x, int y, int sum) {
            this.x = x;
            this.y = y;
            this.sum = sum;
        }

        @Override
        public int compareTo(Node o) {
            return o.sum - this.sum;
        }
    }


    public static int[] topKSum(int[] arr1, int[] arr2, int topK) {
        if (arr1 == null || arr2 == null || topK < 1) {
            return null;
        }

        topK = Math.min(topK, arr1.length * arr2.length);

        int[] res = new int[topK];
        int resIndex = 0;

        int row = arr1.length;
        int col = arr2.length;

        boolean[][] isEnter = new boolean[row][col];

        PriorityQueue<Node> maxHeap = new PriorityQueue<Node>();
        maxHeap.add(new Node(row - 1, col - 1, arr1[row - 1] + arr2[col - 1]));
        isEnter[row - 1][col - 1] = true;

        while (resIndex < topK) {
            Node curNode = maxHeap.poll();
            int sum = curNode.sum;
            int x = curNode.x;
            int y = curNode.y;
            res[resIndex++] = sum;

            if (x - 1 >= 0 && !isEnter[x - 1][y]) {
                maxHeap.add(new Node(x - 1, y, arr1[x - 1] + arr2[y]));
                isEnter[x - 1][y] = true;
            }

            if (y - 1 >= 0 && !isEnter[x][y - 1]) {
                maxHeap.add(new Node(x, y - 1, arr1[x] + arr2[y - 1]));
                isEnter[x][y - 1] = true;
            }
        }

        return res;
    }


    // For test, this method is inefficient but absolutely right
    public static int[] topKSumTest(int[] arr1, int[] arr2, int topK) {
        int[] all = new int[arr1.length * arr2.length];
        int index = 0;
        for (int i = 0; i != arr1.length; i++) {
            for (int j = 0; j != arr2.length; j++) {
                all[index++] = arr1[i] + arr2[j];
            }
        }
        Arrays.sort(all);
        int[] res = new int[Math.min(topK, all.length)];
        index = all.length - 1;
        for (int i = 0; i != res.length; i++) {
            res[i] = all[index--];
        }
        return res;
    }

    public static int[] generateRandomSortArray(int len) {
        int[] res = new int[len];
        for (int i = 0; i != res.length; i++) {
            res[i] = (int) (Math.random() * 50000) + 1;
        }
        Arrays.sort(res);
        return res;
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i != arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static boolean isEqual(int[] arr1, int[] arr2) {
        if (arr1 == null || arr2 == null || arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i != arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int a1Len = 5000;
        int a2Len = 4000;
        int k = 20000000;
        int[] arr1 = generateRandomSortArray(a1Len);
        int[] arr2 = generateRandomSortArray(a2Len);
        long start = System.currentTimeMillis();
        int[] res = topKSum(arr1, arr2, k);
        long end = System.currentTimeMillis();
        System.out.println(end - start + " ms");

        start = System.currentTimeMillis();
        int[] absolutelyRight = topKSumTest(arr1, arr2, k);
        end = System.currentTimeMillis();
        System.out.println(end - start + " ms");

        System.out.println(isEqual(res, absolutelyRight));

    }

}
