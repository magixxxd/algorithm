package com.xxd.algo.myself.trainingcamp003.interview01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-25 16:52
 * @description:
 */
public class Code05_MaxOneBorderSizeTest {

    public static int getMaxSize(int[][] m) {
        int row = m.length;
        int col = m[0].length;

        int[][] down = new int[row][col];
        int[][] right = new int[row][col];

        setBorderMap(m, right, down);

        for (int size = Math.min(row, col); size != 0; size--) {
            if (hasSizeOfBorder(size, right, down)) {
                return size;
            }
        }

        return 0;
    }

    /**
     * 是否有这个长度的正方形
     *
     * @param size
     * @param right
     * @param down
     * @return
     */
    public static boolean hasSizeOfBorder(int size,
                                          int[][] right,
                                          int[][] down) {
        int col = right[0].length;
        int row = right.length;

        for (int i = 0; i != row - size + 1; i++) {
            for (int j = 0; j != col - size + 1; j++) {
                if (right[i][j] >= size &&// 右边合规
                        down[i][j] >= size &&// 下边合规
                        right[i + size - 1][j] >= size &&// 下右合规
                        down[i][j + size - 1] >= size) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 构造辅助 二维数组
     *
     * @param m
     * @param right
     * @param down
     */
    private static void setBorderMap(int[][] m,
                                     int[][] right,
                                     int[][] down) {
        int row = m.length;
        int col = m[0].length;

        for (int i = 0; i < row; i++) {
            for (int j = col - 1; j >= 0; j--) {
                if (j == col - 1) {
                    right[i][j] = m[i][j] == 1 ? 1 : 0;
                } else {
                    right[i][j] = m[i][j] == 1 ? (right[i][j + 1] + 1) : 0;
                }
            }
        }

        for (int i = 0; i < col; i++) { // i 代表列
            for (int j = row - 1; j >= 0; j--) { // j 代表行
                if (j == row - 1) {
                    down[j][i] = m[j][i] == 1 ? 1 : 0;
                } else {
                    down[j][i] = m[j][i] == 1 ? down[j + 1][i] + 1 : 0;
                }
            }
        }
    }

    public static int[][] generateRandom01Matrix(int rowSize, int colSize) {
        int[][] res = new int[rowSize][colSize];
        for (int i = 0; i != rowSize; i++) {
            for (int j = 0; j != colSize; j++) {
                res[i][j] = (int) (Math.random() * 2);
            }
        }
        return res;
    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i != matrix.length; i++) {
            for (int j = 0; j != matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
       /* int[][] arr = {
                {0, 1, 1, 0, 1},
                {1, 0, 1, 1, 1},
                {0, 1, 1, 1, 1},
                {0, 1, 1, 0, 1},
                {0, 0, 1, 1, 1}
        };
        System.out.println(getMaxSize(arr));*/


        int[][] matrix = generateRandom01Matrix(7, 8);
        printMatrix(matrix);
        System.out.println(getMaxSize(matrix));
    }
}
