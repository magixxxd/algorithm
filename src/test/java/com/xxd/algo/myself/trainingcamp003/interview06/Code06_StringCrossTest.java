package com.xxd.algo.myself.trainingcamp003.interview06;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-19 21:14
 * @description:
 */
public class Code06_StringCrossTest {

    /**
     * @param s1
     * @param s2
     * @param ai
     * @return
     */
    public static boolean isCross1(String s1, String s2, String ai) {
        if (s1 == null || s2 == null || ai == null) {
            return false;
        }

        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        char[] aim = ai.toCharArray();
        if (aim.length != str1.length + str2.length) {
            return false;
        }
        boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
        dp[0][0] = true;

        // 第0列
        for (int i = 1; i <= str1.length; i++) {
            if (str1[i - 1] != aim[i - 1]) {
                break;
            }
            dp[i][0] = true;
        }

        // 第0行
        for (int i = 1; i <= str2.length; i++) {
            if (str2[i - 1] != aim[i - 1]) {
                break;
            }
            dp[0][i] = true;
        }

        for (int row = 1; row <= str1.length; row++) {
            for (int col = 1; col <= str2.length; col++) {
                // 可能性1
                if (str1[row - 1] == aim[row + col - 1] && dp[row - 1][col]
                        ||
                        // 可能性2
                        str2[col - 1] == aim[row + col - 1] && dp[row][col - 1]) {
                    dp[row][col] = true;
                }
            }
        }
        return dp[str1.length][str2.length];
    }

    public static void main(String[] args) {
        String str1 = "1234";
        String str2 = "abcd";
        String aim = "1a23bcd4";
        System.out.println(isCross1(str1, str2, aim));
    }
}
