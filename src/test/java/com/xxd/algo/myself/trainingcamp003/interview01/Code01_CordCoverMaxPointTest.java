package com.xxd.algo.myself.trainingcamp003.interview01;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-03-25 12:11
 * @description:
 */
public class Code01_CordCoverMaxPointTest {

    /**
     * 二分查找
     *
     * @param arr
     * @param L
     * @return
     */
    public static int maxPoint1(int[] arr, int L) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            int index = nearestIndex(arr, i, arr[i] - L);
            max = Math.max(max, i - index + 1);
        }

        return max;
    }

    /**
     * 查找 arr 书中 0 .. R 范围上 大于等于 value 的 索引
     * 比如  1 3 6 7 9 11 13大于等于4  那就是 5 返回索引 2
     *
     * @param arr
     * @param R
     * @param value
     * @return
     */
    private static int nearestIndex(int[] arr, int R, int value) {
        int L = 0;
        int mid = L;
        int index = R;
        while (L <= R) {
            mid = L + ((R - L) >> 1);
            if (arr[mid] >= value) {
                R = mid - 1;
                index = mid;
            } else if (arr[mid] < value) {
                L = mid + 1;
            }
        }

        return index;
    }


    /**
     * 窗口
     *
     * @param arr
     * @param L
     * @return
     */
    public static int maxPoint2(int[] arr, int L) {
        int left = 0;
        int right = 0;

        int max = Integer.MIN_VALUE;
        int N = arr.length;
        while (left < N) {
            while (right < N && arr[right] - arr[left] <= L) {
                right++;
            }
            max = Math.max(max, right - (left++));
        }
        return max;
    }

    /**
     * 暴力方法
     *
     * @param arr
     * @param L
     * @return
     */
    public static int test(int[] arr, int L) {
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            int point = 1;
            for (int j = i + 1; j < arr.length && arr[j] - arr[i] <= L; j++) {
                point++;
            }
            max = Math.max(max, point);
        }

        return max;

    }

    // for test
    public static int[] generateArray(int len, int max) {
        int[] ans = new int[(int) (Math.random() * len) + 1];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = (int) (Math.random() * max);
        }
        Arrays.sort(ans);
        return ans;
    }

    public static void main(String[] args) {
      /*  int[] arr = {2, 7, 13, 19, 23, 24};
        int ans3 = test(arr, 8);*/
        int len = 100;
        int max = 1000;
        int testTime = 100000;
        System.out.println("测试开始");
        for (int i = 0; i < testTime; i++) {
            int L = (int) (Math.random() * max);
            int[] arr = generateArray(len, max);
            int ans1 = maxPoint1(arr, L);
            int ans2 = maxPoint2(arr, L);
            int ans3 = test(arr, L);

            if (ans1 != ans2 || ans2 != ans3) {
                System.out.println("oops!");
                break;
            }
        }
    }
}