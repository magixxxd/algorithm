package com.xxd.algo.myself.leetcode;

import java.util.Stack;

public class Problem_0143_ReorderListTest {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }
    }

    public static void reorderList(ListNode head) {
        if (head == null) {
            return;
        }

        Stack<ListNode> stack = new Stack<>();
        ListNode cur = head;
        while (cur != null) {
            stack.push(cur);
            cur = cur.next;
        }

        int size = stack.size();
        int cnt = size / 2;
        ListNode l1 = head;
        ListNode l2 = null;
        ListNode next = null;
        while (cnt > 0) {
            next = l1.next;
            l2 = stack.pop();

            l1.next = l2;
            l2.next = next;

            l1 = next;
            cnt--;
        }

        if ((size & 1) != 0) { // 奇数
            l1.next = null;
        } else { // 偶数
            l2.next = null;
        }
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
//        head.next = new ListNode(2);
//        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);

        reorderList(head);
    }
}
