package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 11:42
 * @description:
 */
public class Problem_0091_DecodeWaysTest {

    public static int numDecodings(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        char[] str = s.toCharArray();
        return process(str, 0);
    }

    private static int process(char[] str, int index) {
        if (index == str.length) {
            return 1;
        }

        if (str[index] == '0') {
            return 0;
        }

        // 单个字符转换
        int ways = process(str, index + 1);

        // 和下面的那个字符一起转换
        if (index + 1 == str.length) {
            return ways;
        }

        int num = (str[index] - '0') * 10 + (str[index + 1] - '0');
        if (num <= 26) {
            ways += process(str, index + 2);
        }

        return ways;
    }


    public static int numDecodings1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int N = s.length();
        int[] dp = new int[N + 1];
        dp[N] = 1;
        char[] str = s.toCharArray();
        for (int i = N - 1; i >= 0; i--) {
            if (str[i] != '0') {
                dp[i] = dp[i + 1];
                if (i + 1 == N) {
                    continue;
                }
                int num = (str[i] - '0') * 10 + str[i + 1] - '0';
                // num > 26
                if (num <= 26) {
                    dp[i] += dp[i + 2];
                }
            }
        }

        return dp[0];
    }

}
