package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-06 10:55
 * @description:
 */
public class Problem_0085_LargestRectangleInHistogramTest {

    public static int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }

        int N = matrix.length;
        int M = matrix[0].length;

        // 辅助数组
        int[][] left = new int[N][M];
        for (int i = 0; i < N; i++) { // 第一列
            if (matrix[i][0] == '1') {
                left[i][0] = 1;
            }
        }

        for (int i = 1; i < M; i++) { // 第二列开始
            for (int j = 0; j < N; j++) {
                if (matrix[j][i] == '1') {
                    left[j][i] = left[j][i - 1] + 1;
                }
            }
        }

        // printMatrix(N, M, left);

        // 从left 的 最后一列开始
        int ans = 0;
        for (int i = M - 1; i >= 0; i--) {
            int[] height = new int[N];
            for (int j = 0; j < N; j++) {
                height[j] = left[j][i];
            }
            ans = Math.max(ans, largestRectangleArea(height));
        }

        return ans;
    }

    public static int largestRectangleArea(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }

        int maxArea = 0;

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < height.length; i++) {
            while (!stack.isEmpty() && height[stack.peek()] > height[i]) {
                int index = stack.pop();
                int k = stack.isEmpty() ? -1 : stack.peek(); // 下面压的那个哥们
                int curArea = (i - k - 1) * height[index];
                maxArea = Math.max(curArea, maxArea);
            }
            stack.push(i);
        }
        while (!stack.isEmpty()) {
            int j = stack.pop();
            int k = stack.isEmpty() ? -1 : stack.peek();
            int curArea = (height.length - k - 1) * height[j];
            maxArea = Math.max(maxArea, curArea);
        }
        return maxArea;
    }

    private static void printMatrix(int n, int m, int[][] left) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(left[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        char[][] matrix = {
                {'1', '0', '1', '0', '0'},
                {'1', '0', '1', '1', '1'},
                {'1', '1', '1', '1', '1'},
                {'1', '0', '0', '1', '0'},
        };
        System.out.println(maximalRectangle(matrix));
    }
}
