package com.xxd.algo.myself.leetcode;

import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 18:33
 * @description:
 */
public class Problem_0239_SlidingWindowMaximumTest {

    public int[] maxSlidingWindow(int[] arr, int w) {
        if (arr == null || w < 1 || arr.length < w) {
            return null;
        }

        int[] res = new int[arr.length - w + 1];
        LinkedList<Integer> qmax = new LinkedList<>();
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[i]) {
                qmax.pollLast();
            }

            qmax.addLast(i);

            if (qmax.peekFirst() == i - w) { // 过期了
                qmax.pollFirst();
            }

            if (i >= w - 1) {
                res[index++] = arr[qmax.peekFirst()];
            }
        }

        return res;
    }
}
