package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-07 07:10
 * @description:
 */
public class Problem_0014_LongestCommonPrefixTest {

    public static String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        if (strs.length == 1) {
            return strs[0];
        }

        char[] firstStr = strs[0].toCharArray();
        int min = Integer.MAX_VALUE;
        for (int i = 1; i < strs.length; i++) {
            char[] curStr = strs[i].toCharArray();
            int index = 0;
            while (index < firstStr.length && index < curStr.length) {
                if (firstStr[index] != curStr[index]) {
                    break;
                }
                index++;
            }

            min = Math.min(index, min);
            if (min == 0) {
                return "";
            }
        }

        return strs[0].substring(0, min);
    }

    public static void main(String[] args) {
        //String[] strs = {"flower","flow","flight"};
         String[] strs = {"a"};
        //String[] strs = {"", "b"};
        System.out.println(longestCommonPrefix(strs));
    }
}
