package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 18:49
 * @description:
 */
public class Problem_0242_ValidAnagramTest {

    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        char[] str = s.toCharArray();
        char[] tar = t.toCharArray();
        int[] count = new int[256];
        for (char c : str) {
            count[c]++;
        }

        for (char c : tar) {
            if (--count[c] < 0) {
                return false;
            }
        }

        return true;
    }
}
