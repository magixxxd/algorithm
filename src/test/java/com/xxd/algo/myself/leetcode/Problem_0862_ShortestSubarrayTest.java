package com.xxd.algo.myself.leetcode;

import sun.reflect.annotation.AnnotationSupport;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-01 10:29
 * @description:
 */
public class Problem_0862_ShortestSubarrayTest {

    public static int shortestSubarray(int[] A, int K) {
        if (A == null || A.length == 0) {
            return 0;
        }

        int n = A.length;
        int[] sum = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            sum[i] = sum[i - 1] + A[i - 1]; // 求前缀和
        }

        Deque<Integer> q = new LinkedList<>();
        int minLength = Integer.MAX_VALUE;
        for (int i = 0; i <= n; i++) {
            while (!q.isEmpty() && sum[q.peekLast()] >= sum[i]) { // 双端队列从小到大
                q.pollLast();
            }

            while (!q.isEmpty() && sum[i] - sum[q.peekFirst()] >= K) {
                minLength = Math.min(minLength, i - q.pollFirst());
            }
            q.addLast(i);
        }
        return minLength == Integer.MAX_VALUE ? -1 : minLength;
    }

    public static int shortestSubarray2(int[] nums, int K) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int N = nums.length;
        int[] sum = new int[N + 1];
        for (int i = 1; i <= nums.length; i++) { // 前缀和
            sum[i] = sum[i - 1] + nums[i - 1];
        }

        int R = 0;
        int ans = Integer.MAX_VALUE;
        LinkedList<Integer> queue = new LinkedList<>();
        while (R <= N) {
            while (!queue.isEmpty() && sum[queue.peekLast()] >= sum[R]) {
                // 队列 从左到右 就是 从小到大 存的就是窗口的最小值
                queue.pollLast();
            }

            // 开始统计结果
            while (!queue.isEmpty() && sum[R] - sum[queue.peekFirst()] >= K) {
                ans = Math.min(ans, R - queue.pollFirst());
            }

            queue.addLast(R);
            R++;
        }

        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    public static void main(String[] args) {
        int[] arr = {3, 2, 1, -4, 8, -5, 6, 7, 3};
        int target = 10;
        System.out.println(shortestSubarray(arr, target));
        System.out.println(shortestSubarray2(arr, target));
    }
}
