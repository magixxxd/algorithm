package com.xxd.algo.myself.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-06 18:15
 * @description:
 */
public class Problem_0316_RemoveDuplicateLettersTest {
    public static String removeDuplicateLetters(String s) {
        char[] str = s.toCharArray();
        int N = str.length;

        Map<Character, Integer> map = new HashMap<>();
        int k = 0;
        for (char c : str) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
                k++;
            } else {
                map.put(c, 1);
            }
        }
        LinkedList<Character> queue = new LinkedList<>();
        HashSet<Character> set = new HashSet<>();
        for (int i = 0; i < N; i++) {
            while (k > 0
                    && !queue.isEmpty()
                    && map.get(queue.peekLast()) > 1
                    && queue.peekLast() >= str[i]
                    && !set.contains(str[i])) {
                Character c = queue.pollLast();
                map.put(c, map.get(c) - 1);
                k--;
                set.remove(c);
            }

            if (!set.contains(str[i])) {
                queue.addLast(str[i]);
                set.add(str[i]);
            } else {
                map.put(str[i], map.get(str[i]) - 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()) {
            Character cha = queue.pollFirst();
            sb.append(cha);
        }

        return sb.toString();
    }

    public static String removeDuplicateLetters2(String s) {
        boolean[] vis = new boolean[26];
        int[] num = new int[26];
        for (int i = 0; i < s.length(); i++) {
            num[s.charAt(i) - 'a']++;
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (!vis[ch - 'a']) {
                while (sb.length() > 0 && sb.charAt(sb.length() - 1) > ch) {
                    if (num[sb.charAt(sb.length() - 1) - 'a'] > 0) {
                        vis[sb.charAt(sb.length() - 1) - 'a'] = false;
                        sb.deleteCharAt(sb.length() - 1);
                    } else {
                        break;
                    }
                }
                vis[ch - 'a'] = true;
                sb.append(ch);
            }
            num[ch - 'a'] -= 1;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String str = "cbacdcbc";
        System.out.println(removeDuplicateLetters(str));
        System.out.println(removeDuplicateLetters2(str));
    }
}
