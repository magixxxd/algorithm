package com.xxd.algo.myself.leetcode;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-27 10:09
 * @description:
 */
public class Problem_0128_LongestConsecutiveSequenceTest {

    public static int longestConsecutive(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        HashMap<Integer, Integer> map = new HashMap<>();
        
        int len = 0;
        for (int num : nums) {
            if (!map.containsKey(num)) {
                int pre = map.containsKey(num - 1) ? map.get(num - 1) : 0;
                int pos = map.containsKey(num + 1) ? map.get(num + 1) : 0;
                int all = pre + pos + 1;


                map.put(num - pre, all);
                map.put(num + pos, all);
                len = Math.max(all, len);
            }
        }

        return len;
    }

    public static void main(String[] args) {
        int[] arr = {0, 3, 7, 2, 5, 8, 4, 6, 0, 1};
        System.out.println(longestConsecutive(arr));
    }
}