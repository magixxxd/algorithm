package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-30 10:59
 * @description:
 */
public class Problem_0187_FindRepeatedDnaSequencesTest {

    public static List<String> findRepeatedDnaSequences(String s) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() <= 10) {
            return ans;
        }

        HashMap<String, Integer> map = new HashMap<>();

        for (int L = 0, R = 9; R < s.length(); L++, R++) {
            String str = s.substring(L, R + 1);
            if (map.containsKey(str)) {
                map.put(str, map.get(str) + 1);
            } else {
                map.put(str, 1);
            }
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String str = entry.getKey();
            Integer count = entry.getValue();
            if (count > 1) {
                ans.add(str);
            }
        }

        return ans;
    }


    public static void main(String[] args) {
        String a = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
        findRepeatedDnaSequences(a);
    }

}
