package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-02 09:54
 * @description:
 */
public class Problem_0150_EvaluateReversePolishNotationTest {

    public static int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String str : tokens) {
            if (str.equals("+") || str.equals("-")||str.equals("*") || str.equals("/")) {
                compute(stack, str);
            } else {
                stack.push(Integer.parseInt(str));
            }
        }

        return stack.pop();
    }


    public static void compute(Stack<Integer> stack, String op) {
        Integer num2 = stack.pop();
        Integer num1 = stack.pop();

        int ans = 0;
        switch (op) {
            case "+":
                ans = num1 + num2;
                break;
            case "-":
                ans = num1 - num2;
                break;

            case "*":
                ans = num1 * num2;
                break;

            case "/":
                ans = num1 / num2;
                break;
        }

        stack.push(ans);
    }
}