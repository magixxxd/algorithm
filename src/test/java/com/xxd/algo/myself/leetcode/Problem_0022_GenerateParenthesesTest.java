package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;
/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-09 08:55
 * @description:
 */
public class Problem_0022_GenerateParenthesesTest {


    public static List<String> generateParenthesis(int n) {
        char[] path = new char[n << 1];
        List<String> ans = new ArrayList<>();
        process(path, 0, 0, n, ans);
        return ans;
    }

    /**
     * @param path
     * @param index          当前索引
     * @param leftMinusRight 左括号的数量减去右括号的数量
     * @param leftRest       左边剩余多少个左括号没有用
     * @param ans
     */
    private static void process(char[] path,
                                int index,
                                int leftMinusRight,
                                int leftRest,
                                List<String> ans) {
        if (index == path.length) {
            ans.add(String.valueOf(path));
        } else {
            if (leftMinusRight > 0) { // 左括号数量减右括号数量大于 0
                path[index] = ')';
                process(path, index + 1, leftMinusRight - 1, leftRest, ans);
            }

            if (leftRest > 0) { // 可以使用的左括号还有剩余
                path[index] = '(';
                process(path, index + 1, leftMinusRight + 1, leftRest - 1, ans);
            }
        }
    }
}