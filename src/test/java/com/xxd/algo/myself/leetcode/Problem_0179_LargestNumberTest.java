package com.xxd.algo.myself.leetcode;

import com.sun.deploy.panel.AbstractRadioPropertyGroup;

import java.awt.font.NumericShaper;
import java.security.acl.LastOwnerException;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-03 17:37
 * @description:
 */
public class Problem_0179_LargestNumberTest {

    public static class MyComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return (o2 + o1).compareTo(o1 + o2);
        }
    }

    public static String largestNumber(int[] nums) {
        String[] strs = new String[nums.length];
        for (int i = 0; i < nums.length; i++) {
            strs[i] = String.valueOf(nums[i]);
        }

        Arrays.sort(strs, new MyComparator());
        StringBuilder builder = new StringBuilder();
        for (String str : strs) {
            builder.append(str);
        }

        String ans = builder.toString();
        char[] str = ans.toCharArray();
        int index = -1;
        for (int i = 0; i < str.length; i++) {
            if (str[i] != '0') {
                index = i;
                break;
            }
        }


        return index == -1 ? "0" :ans.toString();
    }

    public static void main(String[] args) {
        int[] nums = {3, 30, 34, 5, 9};
        System.out.println(largestNumber(nums));

    }
}
