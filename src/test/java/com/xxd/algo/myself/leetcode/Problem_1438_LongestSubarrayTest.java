package com.xxd.algo.myself.leetcode;

import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-28 10:26
 * @description:
 */
public class Problem_1438_LongestSubarrayTest {

    public int longestSubarray(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int res = 0;
        LinkedList<Integer> qmax = new LinkedList<>();
        LinkedList<Integer> qmin = new LinkedList<>();
        int L = 0;
        int R = 0;
        while (L < arr.length) {
            while (R < arr.length) {
                // 更新最大值队列
                while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[R]) {
                    qmax.pollLast();
                }
                qmax.addLast(R);

                // 更新最小值
                while (!qmin.isEmpty() && arr[qmin.peekLast()] >= arr[R]) {
                    qmin.pollLast();
                }
                qmin.addLast(R);

                // 看是否达标了
                if (arr[qmax.peekFirst()] - arr[qmin.peekFirst()] > num) {
                    break;
                }

                R++;
            }

            res = Math.max(res, R - L); // 统计答案
            // 过期队列

            if (qmax.peekFirst() == L) {
                qmax.pollFirst();
            }

            if (qmin.peekFirst() == L) {
                qmin.pollFirst();
            }

            L++;
        }


        return res;
    }

}
