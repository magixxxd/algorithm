package com.xxd.algo.myself.leetcode;

import java.util.List;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-09 11:43
 * @description:
 */
public class Problem_0907_SumSubarrayMinsTest {

    public static int sumSubarrayMins(int[] arr) {
        int N = arr.length;
        long ans = 0;
        long BASE = 1_000_000_007;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < N; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] >= arr[i]) {
                Integer index = stack.pop();
                int pre = stack.isEmpty() ? -1 : stack.peek();

                int m = index - pre - 1;
                int n = i - index - 1;

                ans += ((m + 1) * (n + 1) % BASE) * arr[index];
                ans %= BASE;
            }

            stack.push(i);
        }

        int right = N;
        while (!stack.isEmpty()) {
            Integer index = stack.pop();

            int pre = stack.isEmpty() ? -1 : stack.peek();

            int n = right - index - 1;
            int m = index - pre - 1;

            ans += arr[index] * (m + 1) * (n + 1) % BASE;
            ans %= BASE;
        }


        return (int) ans;
    }

    public static void main(String[] args) {
        int[] arr = {3, 1, 2, 4};
        System.out.println(sumSubarrayMins(arr));
    }
}
