package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-24 09:34
 * @description:
 */
public class Problem_0394_DecodeStringTest {

    public static class Info {
        String ans;
        int end;

        public Info(String ans, int end) {
            this.ans = ans;
            this.end = end;
        }
    }

    public static String decodeString(String s) {
        return process(s.toCharArray(), 0).ans;
    }

    private static Info process(char[] s, int index) {
        StringBuilder ans = new StringBuilder();
        int times = 0;
        while (index < s.length && s[index] != ']') {
            if ((s[index] >= 'a' && s[index] <= 'z') || (s[index] >= 'A' && s[index] <= 'Z')) {
                ans.append(s[index++]);
            } else if (s[index] >= '0' && s[index] <= '9') {
                times = 10 * times + s[index++] - '0';
            } else { // [
                Info info = process(s, index + 1);
                ans.append(timesString(times, info.ans));
                times = 0;
                index = info.end + 1;
            }
        }

        return new Info(ans.toString(), index);
    }

    private static String timesString(int times, String str) {
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < times; i++) {
            ans.append(str);
        }
        return ans.toString();
    }
}
