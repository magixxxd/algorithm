package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-21 10:46
 * @description:
 */
public class Problem_0387_FirstUniqueCharacterInStringTest {

    public int firstUniqChar(String s) {
        char[] str = s.toCharArray();
        int N = str.length;
        int count[] = new int[256];
        for (int i = 0; i < N; i++) {
            count[str[i] - 'a']++;
        }
        for (int i = 0; i < N; i++) {
            if (count[str[i] - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }


}
