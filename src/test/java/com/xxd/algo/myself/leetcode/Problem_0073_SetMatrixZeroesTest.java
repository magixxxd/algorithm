package com.xxd.algo.myself.leetcode;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-22 15:40
 * @description:
 */
public class Problem_0073_SetMatrixZeroesTest {

    public static void setZeroes(int[][] matrix) {
        // 第0 行是否全是0
        boolean row0Zero = false;

        // 第1列 是否全是0
        boolean col0Zero = false;

        for (int i = 0; i < matrix[0].length; i++) { // 第一行
            if (matrix[0][i] == 0) {
                row0Zero = true;
                break;
            }
        }


        for (int i = 0; i < matrix.length; i++) { // 第一列
            if (matrix[i][0] == 0) {
                col0Zero = true;
                break;
            }
        }


        // 普遍位置开始了
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                if (matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }


        // 第一行开始搞
        for (int i = 1; i < matrix[0].length; i++) { // 第一行
            if (matrix[0][i] == 0) {
                for (int j = 1; j < matrix.length; j++) {
                    matrix[j][i] = 0;
                }
            }
        }

        for (int i = 1; i < matrix.length; i++) { // 第一行
            if (matrix[i][0] == 0) {
                for (int j = 1; j < matrix[0].length; j++) {
                    matrix[i][j] = 0;
                }
            }
        }

        if (row0Zero) {
            for (int i = 0; i < matrix[0].length; i++) {
                matrix[0][i] = 0;
            }
        }

        if (col0Zero) {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][0] = 0;
            }
        }

    }

    public static void main(String[] args) {
        int[][] matrix = {{0, 1, 2, 0},
                {3, 4, 5, 2},
                {1, 3, 1, 5}};
        setZeroes(matrix);
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }
}
