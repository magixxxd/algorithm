package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-28 15:36
 * @description:
 */
public class Problem_0011_ContainerWithMostWaterTest {

    public static int maxArea(int[] h) {
        if (h == null) {
            return 0;
        }

        int l = 0;
        int r = h.length - 1;
        int max = 0;
        while (l < r) {
            max = Math.max(max, Math.min(h[l], h[r]) * (r - l));
            if (h[l] > h[r]) {
                r--;
            } else {
                l++;
            }
        }

        return max;
    }




    public static int maxArea2(int[] h) {
        if (h == null) {
            return 0;
        }

        int l = 0;
        int r = h.length - 1;
        int max = 0;
        while (l < r) {
            max = Math.max(max, Math.min(h[l], h[r]) * (r - l));
            if (h[l] > h[r]) {
                r--;
            } else {
                l++;
            }
        }

        return max;
    }




    public static void main(String[] args) {
        int[] h = {1, 1};
        System.out.println(maxArea(h));
    }
}
