package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-20 10:09
 * @description:
 */
public class Problem_0062_UniquePathsTest {

    /**
     *
     * @param m 7
     * @param n 3
     * @return
     */
    public static int uniquePaths(int m, int n) {
        int part = n - 1; // 2
        int all = m + n - 2; // 8
        long o1 = 1;
        long o2 = 1;
        for (int i = part + 1, j = 1;
             i <= all || j <= all - part;
             i++, j++) {
            o1 *= i;
            o2 *= j;
            long gcd = gcd(o1, o2);
            o1 /= gcd;
            o2 /= gcd;
        }
        return (int) o1;
    }
    public static long gcd(long m, long n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    public static void main(String[] args) {
        System.out.println(uniquePaths(7, 3));
    }
}
