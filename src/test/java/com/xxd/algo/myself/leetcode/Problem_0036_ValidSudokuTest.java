package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-13 08:31
 * @description:
 */
public class Problem_0036_ValidSudokuTest {
    public static boolean isValidSudoku(char[][] board) {
        boolean[][] row = new boolean[9][10]; // row[2] [4] 第3行 是否出现了4
        boolean[][] col = new boolean[9][10]; // row[2] [4] 第2列 是否出现了4
        boolean[][] bucket = new boolean[9][10]; // bucket 是否出现过

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int bid = 3 * (i / 3) + (j / 3); //
                if (board[i][j] != '.') {
                    int num = board[i][j] - '0';
                    if (row[i][num] || col[j][num] || bucket[bid][num]) {
                        return false;
                    }
                    row[i][num] = true;
                    col[j][num] = true;
                    bucket[bid][num] = true;
                }
            }
        }

        return true;
    }
}
