package com.xxd.algo.myself.leetcode;

import com.xxd.algo.newcode.base01.class05.PaperFoldingTest;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-17 18:37
 * @description:
 */
public class Problem_0044_WildcardMatchingTest {

    public static boolean isMatch(String s, String p) {
        char[] str = s.toCharArray();
        char[] pattern = p.toCharArray();

        return process(str, pattern, 0, 0);
    }


    private static boolean process(char[] str, char[] pattern, int si, int pi) {
        if (si == str.length) {
            if (pi == pattern.length) {
                return true;
            } else {
                return pattern[pi] == '*' && process(str, pattern, si, pi + 1);
            }
        }

        if (pi == pattern.length) {
            return false;
        }

        if (pattern[pi] != '?' && pattern[pi] != '*') {
            return str[si] == pattern[pi] && process(str, pattern, si + 1, pi + 1);
        }

        if (pattern[pi] == '?') {
            return process(str, pattern, si + 1, pi + 1);
        }

        // pattern[pi] == '*' 就是讨论 一个* 当多少个字符的情况
        for (int len = 0; len <= str.length - si; len++) {
            if (process(str, pattern, si + len, pi + 1)) {
                return true;
            }
        }

        return false;
    }


    public static boolean dp2(String str, String pattern) {
        char[] s = str.toCharArray();
        char[] p = pattern.toCharArray();
        int N = s.length;
        int M = p.length;
        boolean[][] dp = new boolean[N + 1][M + 1];
        dp[N][M] = true;
        for (int pi = M - 1; pi >= 0; pi--) {
            dp[N][pi] = p[pi] == '*' && dp[N][pi + 1];
        }
        // 普遍位置
        for (int si = N - 1; si >= 0; si--) {
            for (int pi = M - 1; pi >= 0; pi--) {
                if (p[pi] != '?' && p[pi] != '*') {
                    dp[si][pi] = s[si] == p[pi] && dp[si + 1][pi + 1];
                    continue;
                }

                if (p[pi] == '?') {
                    dp[si][pi] = dp[si + 1][pi + 1];
                    continue;
                }

                // p[pi] == '*'
                dp[si][pi] = dp[si][pi + 1] || dp[si + 1][pi];
            }
        }

        return dp[0][0];
    }

    public static boolean dp(String str, String pattern) {
        char[] s = str.toCharArray();
        char[] p = pattern.toCharArray();
        int N = s.length;
        int M = p.length;
        boolean[][] dp = new boolean[N + 1][M + 1];
        dp[N][M] = true;
        for (int pi = M - 1; pi >= 0; pi--) {
            dp[N][pi] = p[pi] == '*' && dp[N][pi + 1];
        }

        for (int si = N - 1; si >= 0; si--) {
            for (int pi = M - 1; pi >= 0; pi--) {
                if (p[pi] != '?' && p[pi] != '*') {
                    dp[si][pi] = s[si] == p[pi] && dp[si + 1][pi + 1];
                    continue;
                }

                if (p[pi] == '?') {
                    dp[si][pi] = dp[si + 1][pi + 1];
                    continue;
                }

                // p[pi] == '*'
                dp[si][pi] = dp[si + 1][pi] || dp[si][pi + 1];
            }
        }

        return dp[0][0];
    }

    public static boolean isMatch2(String s, String p) {
        char[] str = s.toCharArray();
        char[] pattern = p.toCharArray();

        return f(str, pattern, 0, 0);
    }


    private static boolean f(char[] str, char[] pattern, int si, int pi) {
        if (si == str.length) { // si 到头了
            if (pi == pattern.length) {
                return true;
            } else {
                // pi 没有到头，必然是 * 才有救
                return pattern[pi] == '*' && f(str, pattern, si, pi + 1);
            }
        }

        if (pi == pattern.length) { // pi 到头了，没有救了
            return false;
        }

        if (pattern[pi] != '?' && pattern[pi] != '*') {
            return str[si] == pattern[pi] && f(str, pattern, si + 1, pi + 1);
        }

        // 可以匹配任意一个，且是必须匹配
        if (pattern[pi] == '?') {
            return f(str, pattern, si + 1, pi + 1);
        }

        // 当前位置是 * ， 可以匹配0 个，一直到str 的最后一个 还有多少个
        for (int len = 0; len <= str.length - si; len++) {
            if (f(str, pattern, si + len, pi + 1)) { // * 当 len 个 str的字符
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        String str = "bbbbbbbabbaabbabbbbaaabbabbabaaabbababbbabbbabaaabaab";
        String pattern = "b*b*ab**ba*b**b***bba";
        System.out.println(isMatch2(str, pattern));
        System.out.println(isMatch(str, pattern));
    }
}
