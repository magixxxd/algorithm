package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0108_ConvertSortedArrayToBinarySearchTree;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-25 09:23
 * @description:
 */
public class Problem_0108_ConvertSortedArrayToBinarySearchTreeTest {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode sortedArrayToBST(int[] nums) {
        return process(nums, 0, nums.length - 1);
    }

    private TreeNode process(int[] nums, int L, int R) {
        if (L > R) {
            return null;
        }
        if (L == R) {
            return new TreeNode(nums[L]);
        }

        int M = (L + R) / 2;
        TreeNode head = new TreeNode(nums[M]);
        head.left = process(nums, L, M - 1);
        head.right = process(nums, M + 1, R);
        return head;
    }

}
