package com.xxd.algo.myself.leetcode;

import com.sun.prism.shader.Mask_TextureRGB_AlphaTest_Loader;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 11:00
 * @description:
 */
public class Problem_0084_LargestRectangleInHistogramTest {

    public static int largestRectangleArea(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }

        int maxArea = 0;

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < height.length; i++) {
            while (!stack.isEmpty() && height[stack.peek()] > height[i]) {
                int index = stack.pop();
                int k = stack.isEmpty() ? -1 : stack.peek(); // 下面压的那个哥们
                int curArea = (i - k - 1) * height[index];
                maxArea = Math.max(curArea, maxArea);
            }
            stack.push(i);
        }
        while (!stack.isEmpty()) {
            int j = stack.pop();
            int k = stack.isEmpty() ? -1 : stack.peek();
            int curArea = (height.length - k - 1) * height[j];
            maxArea = Math.max(maxArea, curArea);
        }
        return maxArea;
    }


    public static int largestRectangleArea2(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }

        int maxArea = 0;
        Stack<Integer> stack = new Stack<>(); // 单调栈，从小到大
        for (int i = 0; i < height.length; i++) {
            while (!stack.isEmpty() && height[stack.peek()] > height[i]) {
                Integer index = stack.pop();
                int left = stack.isEmpty() ? -1 : stack.peek();
                int curArea = (i - 1 - left) * height[index];
                maxArea = Math.max(maxArea, curArea);
            }
            stack.push(i);
        }

        while (!stack.isEmpty()) {
            Integer index = stack.pop();
            int left = stack.isEmpty() ? -1 : stack.peek();
            int right = height.length;
            int curArea = (right - 1 - left) * height[index];
            maxArea = Math.max(maxArea, curArea);
        }

        return maxArea;
    }

    public static void main(String[] args) {
        int[] height = {2, 1, 5, 6, 2, 3};
        System.out.println(largestRectangleArea(height));
        System.out.println(largestRectangleArea2(height));
    }
}
