package com.xxd.algo.myself.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-22 16:50
 * @description:
 */
public class Problem_0076_MinimumWindowSubstringTest {


    public static String minWindow(String s, String t) {
        if (s.length() < t.length()) {
            return "";
        }
        int[] map = new int[256];

        char[] str = s.toCharArray();
        char[] target = t.toCharArray();
        for (char cha : target) {
            map[cha]++;
        }

        int L = 0;
        int R = 0;
        int minLen = -1;
        int ansL = -1;
        int ansR = -1;
        int all = target.length;
        while (R < str.length) {
            map[str[R]]--;
            if (map[str[R]] >= 0) {
                all--;
            }

            if (all == 0) {
                while (map[str[L]] < 0) {
                    map[str[L++]]++;
                }

                if (minLen == -1 || minLen > R - L + 1) {
                    minLen = R - L + 1;
                    ansL = L;
                    ansR = R;
                }
                all++;
                map[str[L++]]++;
            }

            R++;
        }

        return minLen == -1 ? "" : s.substring(ansL, ansR + 1);
    }

    public static String minWindow2(String s, String t) {
        if (s == null || s.equals("") || s.length() < t.length()) {
            return "";
        }
        char[] str = s.toCharArray();
        char[] target = t.toCharArray();

        int[] map = new int[256];
        int all = target.length;
        for (char tr : target) {
            map[tr]++;
        }

        int L = 0;
        int R = 0;

        int length = -1;
        int ansL = -1;
        int ansR = -1;
        while (R < str.length) {
            map[str[R]]--;
            if (map[str[R]] >= 0) {
                all--;
            }
            if (all == 0) {
                while (map[str[L]] < 0) {
                    map[str[L++]]++;
                }
                if (length == -1 || length > R - L + 1) {
                    length = R - L + 1;
                    ansL = L;
                    ansR = R;
                }
                all++;
                map[str[L++]]++;
            }
            R++;
        }

        return length == -1 ? "" : s.substring(ansL, ansR + 1);
    }

    public static void main(String[] args) {
        String a = "ADOBECODEBANC";
        String t = "ABC";
        System.out.println(minWindow2(a, t));
    }
}
