package com.xxd.algo.myself.leetcode;

import jdk.nashorn.internal.runtime.regexp.joni.constants.TargetInfo;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-03 17:16
 * @description:
 */
public class Problem_0171_ExcelSheetColumnNumberTest {

    public static int titleToNumber(String s) {
        int ans = 0;
        char[] str = s.toCharArray();
        for (int i = 0; i < str.length; i++) {
            ans = ans * 26 + (str[i] - 'A') + 1;
        }

        return ans;
    }

    public static void main(String[] args) {

        String a = "ZY";
        System.out.println(titleToNumber(a));
    }
}
