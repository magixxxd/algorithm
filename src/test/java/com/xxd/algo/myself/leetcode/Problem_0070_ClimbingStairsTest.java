package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-22 15:32
 * @description:
 */
public class Problem_0070_ClimbingStairsTest {

    public static int climbStairs(int n) {
        if (n < 1) {
            return 0;
        }

        if (n == 1 || n == 2) {
            return n;
        }

        int p1 = 2;
        int p2 = 1;

        int ans = 0;
        for (int i = 3; i <= n; i++) {
            ans = p1 + p2;
            p2 = p1;
            p1 = ans;
        }

        return ans;
    }


    public static void main(String[] args) {
        System.out.println(climbStairs(45));
    }
}
