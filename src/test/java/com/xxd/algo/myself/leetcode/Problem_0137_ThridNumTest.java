package com.xxd.algo.myself.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-30 11:29
 * @description:
 */
public class Problem_0137_ThridNumTest {

    public static int singleNumber(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();

        int ans = -1;
        for (int num : nums) {
            Integer count = map.get(num);
            if (count == null) {
                count = 1;
                map.put(num, 1);
            } else {
                map.put(num, ++count);
            }
            if (count == 3) {
                map.remove(num);
            }
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            ans = entry.getKey();
        }
        return ans;
    }


    public static void main(String[] args) {
        //int[] nums = {0, 0, 0, 1, 1, 1, 3};
        int[] nums = {30000, 500, 100, 30000, 100, 30000, 100};
        System.out.println(singleNumber(nums));

    }
}
