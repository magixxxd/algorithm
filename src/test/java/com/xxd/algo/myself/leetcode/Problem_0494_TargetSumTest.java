package com.xxd.algo.myself.leetcode;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-25 11:04
 * @description:
 */
public class Problem_0494_TargetSumTest {
    public static int findTargetSumWays1(int[] arr, int target) {
        HashMap<String, Integer> map = new HashMap<>();
        return process(arr, 0, target, map);
    }

    private static int process(int[] arr, int index,
                               int rest, HashMap<String, Integer> map) {
        if (index == arr.length) {
            return rest == 0 ? 1 : 0;
        }

        if (map.containsKey(index + "_" + rest)) {
            return map.get(index + "_" + rest);
        }

        int ans = process(arr, index + 1, rest - arr[index], map)
                + process(arr, index + 1, rest + arr[index], map);

        map.put(index + "_" + rest, ans);
        return ans;
    }

}
