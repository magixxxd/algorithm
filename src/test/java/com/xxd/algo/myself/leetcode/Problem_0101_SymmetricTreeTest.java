package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 17:52
 * @description:
 */
public class Problem_0101_SymmetricTreeTest {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public boolean isSymmetric(TreeNode root) {
       return process(root, root);
    }

    private boolean process(TreeNode head1, TreeNode head2) {
        if (head1 == null && head2 == null) {
            return true;
        }

        if (head1 != null && head2 != null) {
            return head1.val == head2.val
                    && process(head1.left, head2.right)
                    && process(head1.right, head2.left);
        }

        return false;
    }



    public boolean isSymmetric2(TreeNode root) {
        return process2(root, root);
    }

    private boolean process2(TreeNode head1, TreeNode head2) {
        if (head1 == null && head2 == null) {
            return true;
        }
        if (head1 != null && head2 != null) {
            return head1.val == head2.val
                    && process2(head1.left, head2.right)
                    && process2(head1.right, head2.left);
        }

        return false;
    }
}
