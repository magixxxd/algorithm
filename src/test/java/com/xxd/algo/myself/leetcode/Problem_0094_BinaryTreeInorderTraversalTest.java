package com.xxd.algo.myself.leetcode;


import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 17:37
 * @description:
 */
public class Problem_0094_BinaryTreeInorderTraversalTest {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        inorderTraversal(root, result);
        return result;
    }

    private static void inorderTraversal(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        inorderTraversal(root.left, result);
        result.add(root.val);
        inorderTraversal(root.right, result);
    }


    public static List<Integer> inorderTraversal2(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        inorderTraversal2(root, result);
        return result;
    }

    private static void inorderTraversal2(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }

        inorderTraversal2(root.left, result);
        result.add(root.val);
        inorderTraversal2(root.right, result);
    }
}