package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-19 16:39
 * @description:
 */
public class Problem_0050_PowXNTest {

    public static double myPow(double x, int n) {
        if (n == 0) {
            return 1D;
        }

        int pow = Math.abs(n == Integer.MIN_VALUE ? n + 1 : n);
        double ans = 1D;
        double t = x;
        while (pow != 0) {
            if ((pow & 1) == 1) { // 有值
                ans = ans * t;
            }
            t = t * t;
            pow >>= 1;
        }
        if (n == Integer.MIN_VALUE) {
            ans = ans * x;
        }

        return n < 0 ? (1D / ans) : ans;
    }


    public static double myPow1(double x, int n) {
        if (n == 0) {
            return 1D;
        }

        int pow = Math.abs(n == Integer.MIN_VALUE ? n + 1 : n);
        double ans = 1D;
        double t = x;
        while (pow != 0) {
            if ((pow & 1) != 0) {
                ans = ans * t;
            }
            pow >>= 1;
            t = t * t;
        }
        if (n == Integer.MIN_VALUE) {
            ans = ans * x;
        }

        return n < 0 ? (1D / ans) : ans;
    }



}
