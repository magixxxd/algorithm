package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-22 09:36
 * @description:
 */
public class Problem_0395_LongestSubstringWithAtLeastKRepeatingCharactersTest {

    public static int longestSubstring(String s, int k) {
        char[] str = s.toCharArray();
        int N = str.length;
        int ans = 0;
        for (int requires = 1; requires <= 26; requires++) {
            int R = -1;
            int[] count = new int[26];
            int collect = 0; // L .. R 上有多少种字符
            int satisfy = 0; // L .. r 上有多少种满足条件的字符
            for (int L = 0; L < N; L++) {
                while (R + 1 < N &&
                        !(collect == requires && count[str[R + 1] - 'a'] == 0)) {
                    R++;
                    if (count[str[R] - 'a'] == 0) {
                        collect++;
                    }
                    if (count[str[R] - 'a'] == k - 1) {
                        satisfy++;
                    }
                    count[str[R - 'a']]++;
                }

                if (satisfy == requires) { // 满足要求，开始统计长度
                    ans = Math.max(ans, R - L + 1);
                }
                // L++
                if (count[str[L] - 'a'] == 1) {
                    collect--;
                }
                if (count[str[L] - 'a'] == k) {
                    satisfy--;
                }
                count[str[L] - 'a']--;
            }
        }

        return ans;
    }

    public static int longestSubstring1(String s, int k) {
        char[] str = s.toCharArray();
        int N = str.length;
        int max = 0;
        for (int require = 0; require <= 26; require++) {
            int[] count = new int[26];
            int collect = 0; // 窗口内多少种字符
            int satisfy = 0;// 满足条件的有多少种字符

            int R = -1;
            for (int L = 0; L < N; L++) {
                while (R + 1 < N && !(collect == require && count[str[R + 1] - 'a'] == 0)) {
                    R++;
                    if (count[str[R] - 'a'] == 0) {
                        collect++;
                    }
                    if (count[str[R] - 'a'] == k - 1) {
                        satisfy++;
                    }
                    count[str[R] - 'a']++;
                }
                if (satisfy == require) {
                    max = Math.max(max, R - L + 1);
                }
                if (count[str[L] - 'a'] == 1) {
                    collect--;
                }
                if (count[str[L] - 'a'] == k) {
                    satisfy--;
                }
                count[str[L] - 'a']--;
            }
        }

        return max;
    }


    public static int longestSubstring2(String s, int k) {
        char[] str = s.toCharArray();
        return process(str, 0, str.length - 1, k);
    }

    private static int process(char[] str, int L, int R, int k) {
        if (L > R) {
            return 0;
        }


        return 0;
    }
}
