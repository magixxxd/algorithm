package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-08 07:49
 * @description:
 */
public class Problem_0020_ValidParenthesesTest {


    public static boolean isValid(String s) {
        if (s.length() < 2) {
            return false;
        }

        char[] str = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length; i++) {
            char cha = str[i];
            if (cha == '(' || cha == '{' || cha == '[') {
                stack.add(cha == '(' ? ')' : (cha == '[' ? ']' : '}'));
            } else {
                if (stack.isEmpty()) {
                    return false;
                }

                Character last = stack.pop();
                if (cha != last) {
                    return false;
                }
            }
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {
        String str = "()";
        System.out.println(isValid(str));
    }
}
