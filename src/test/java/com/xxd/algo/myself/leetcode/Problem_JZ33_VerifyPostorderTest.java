package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-08 10:52
 * @description:
 */
public class Problem_JZ33_VerifyPostorderTest {

    public static boolean verifyPostorder(int[] postorder) {
        int N = postorder.length;
        return process(postorder, 0, N - 1);
    }

    private static boolean process(int[] postorder, int start, int end) {
        if (start >= end) {
            return true;
        }
        int p = start;
        while (postorder[p] < postorder[end]) {
            p++;
        }

        int m = p;
        while (postorder[p] > postorder[end]) {
            p++;
        }

        return p == end && process(postorder, start, m - 1) && process(postorder, m, end - 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 2, 6, 8, 9, 5};
        int[] arr2 = {1, 6, 2, 6, 8, 9, 5};
        System.out.println(verifyPostorder(arr2));

    }
}
