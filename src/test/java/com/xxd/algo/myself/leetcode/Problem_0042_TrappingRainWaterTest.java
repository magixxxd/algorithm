package com.xxd.algo.myself.leetcode;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-17 18:27
 * @description: 接雨水
 */
public class Problem_0042_TrappingRainWaterTest {


    public static int trap3(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }
        int N = arr.length;
        int L = 1;
        int R = N - 2;

        int LM = arr[0];
        int RM = arr[arr.length - 1];

        int water = 0;
        while (L <= R) {
            if (LM <= RM) {
                // LM 是我的瓶颈
                water += Math.max(LM - arr[L], 0);
                LM = Math.max(LM, arr[L++]);
            } else {
                // RM 是我的瓶颈
                water += Math.max(RM - arr[R], 0);
                RM = Math.max(RM, arr[R--]);
            }
        }

        return water;
    }

    public static int trap(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }
        int N = arr.length;
        int L = 1;
        int R = N - 2;

        int leftMax = arr[0];
        int rightMax = arr[N - 1];
        int water = 0;
        while (L <= R) {
            if (leftMax <= rightMax) { // 计算L
                water += Math.max(0, leftMax - arr[L]);
                leftMax = Math.max(leftMax, arr[L++]);
            } else {
                water += Math.max(0, rightMax - arr[R]);
                rightMax = Math.max(rightMax, arr[R--]);
            }
        }

        return water;
    }


    public static int trap2(int[] arr) {
        if (arr == null || arr.length < 3) {
            return 0;
        }
        int water = 0;
        int N = arr.length;
        int L = 1;
        int R = N - 2;

        int leftMax = arr[0];
        int rightMax = arr[N - 1];

        while (L <= R) {
            if (leftMax <= rightMax) {
                water += Math.max(0, leftMax - arr[L]);
                leftMax = Math.max(arr[L++], leftMax);
            } else {
                water += Math.max(0, rightMax - arr[R]);
                rightMax = Math.max(arr[R--], rightMax);
            }
        }

        return water;
    }
}
