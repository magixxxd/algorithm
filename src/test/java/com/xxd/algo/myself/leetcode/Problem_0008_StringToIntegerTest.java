package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-28 15:10
 * @description:
 */
public class Problem_0008_StringToIntegerTest {

    public int myAtoi(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }

        s = removeHeadZero(s.trim());
        if (s.equals("")) {
            return 0;
        }

        char[] str = s.toCharArray();
        // str 是符合日常书写的，正经整数形式
        boolean posi = str[0] != '-';

        int minq = Integer.MIN_VALUE / 10;
        int minr = Integer.MIN_VALUE % 10;
        int res = 0;
        int cur = 0;
        for (int i = (str[0] == '-' || str[0] == '+') ? 1 : 0; i < str.length; i++) {
            cur = '0' - str[i];
            if ((res < minq) || (res == minq && cur < minr)) {
                return posi ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            res = res * 10 + cur;
        }
        // res 负
        if (posi && res == Integer.MIN_VALUE) {
            return Integer.MAX_VALUE;
        }
        return posi ? -res : res;
    }

    public static int myAtoi2(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }

        s = removeHeadZero(s.trim());
        if (s.equals("")) {
            return 0;
        }

        char[] str = s.toCharArray();
        // str 是符合日常书写的，正经整数形式

        int m = Integer.MAX_VALUE / 10;
        int o = Integer.MAX_VALUE % 10;

        boolean posi = str[0] != '-';

        int res = 0;
        int cur = 0;
        for (int i = (str[0] == '-' || str[0] == '+') ? 1 : 0; i < str.length; i++) {
            cur = str[i] - '0';
            if (res > m || (res == m && cur > o)) {
                return posi ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }

            res = res * 10 + cur;
        }
        // res 负

        return posi ? res : -res;
    }

    private static String removeHeadZero(String str) {
        boolean r = str.startsWith("+") || str.startsWith("-");

        int s = r ? 1 : 0;
        for (; s < str.length(); s++) { // 去掉前面的0 比如str 是 0003332 => 3332
            if (str.charAt(s) != 0) {
                break;
            }
        }

        int e = -1;
        for (int i = str.length() - 1; i >= (r ? 1 : 0); i--) {
            if (str.charAt(i) < '0' || str.charAt(i) > '9') { // 出现其他乱七八糟的字符 比如 23b2321assa e 的索引就是第一个b的位置
                e = i;
            }
        }

        return (r ? String.valueOf(str.charAt(0)) : "")
                + str.substring(s, e == -1 ? str.length() : e);
    }

    public static void main(String[] args) {
        String s = "-91283472332";
        String s2 = "987  aa";
        System.out.println(myAtoi2(s));

//        System.out.println(removeHeadZero(s));
//        System.out.println(removeHeadZero(s2));
    }
}