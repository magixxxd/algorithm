package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-04 11:19
 * @description:
 */
public class Problem_0204_CountPrimesTest {

    public static int countPrimes(int n) {
        if (n < 3) {
            return 0;
        }

        int count = 0;

        for (int i = 3; i < n; i++) {
            count += f(i) ? 1 : 0;
        }
        return count + 1;
    }

    public static boolean f(int n) {
        int sqrt = (int) (Math.sqrt(n) + 1);

        for (int i = 2; i < sqrt; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(countPrimes(20));
    }
}
