package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-29 19:06
 * @description:
 */
public class Problem_0718_FindLengthTest {


    public static int findLength(int[] A, int[] B) {
        int max = 0;
        int an = A.length;
        int bn = B.length;
        int len = 0;
        for (int i = 0; i < an; i++) {
            /**
             *       ######   ######    ######    ######
             *       ######    ######     ######     ######
             */
            len = Math.min(an - i, bn);
            max = Math.max(max, maxLen(A, i, B, 0, len));
        }

        for (int j = 0; j < bn; j++) {
            len = Math.min(bn - j, an);
            max = Math.max(max, maxLen(A, 0, B, j, len));
        }

        return max;
    }

    /**
     * a 数组 以i 开头，b数组以j 开头，此时最长的公共子串是多少
     *
     * @param a
     * @param i
     * @param b
     * @param j
     * @param len
     * @return
     */
    private static int maxLen(int[] a, int i, int[] b, int j, int len) {
        int count = 0; // 连续的重复子串长度，因为以i开头，会有很多重复的子串，所以要全部统计出来
        int max = 0; // 最长的重复子串长度
        for (int k = 0; k < len; k++) {
            if (a[i + k] == b[j + k]) {
                count++;
            } else {
                max = Math.max(max, count);
                count = 0;
            }
        }
        return count > 0 ? Math.max(max, count) : max;
    }

    public static void main(String[] args) {
        int[] nums1 = {0, 1, 1, 1, 0};
        int[] nums2 = {1, 1, 1, 1, 1};
        System.out.println(findLength(nums1, nums2));
    }

}
