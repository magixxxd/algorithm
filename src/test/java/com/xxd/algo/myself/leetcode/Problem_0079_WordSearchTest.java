package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 10:41
 * @description:
 */
public class Problem_0079_WordSearchTest {

    public static boolean exist(char[][] board, String word) {
        char[] w = word.toCharArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (process(board, i, j, w, 0)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean process(char[][] board, int i, int j, char[] w, int k) {
        if (k == w.length) {
            return true;
        }

        if (i < 0 || i == board.length || j < 0 || j == board[0].length) {
            return false;
        }

        if (board[i][j] != w[k]) {
            return false;
        }

        char temp = board[i][j];
        board[i][j] = 0;
        boolean ans = process(board, i - 1, j, w, k + 1)
                || process(board, i + 1, j, w, k + 1)
                || process(board, i, j - 1, w, k + 1)
                || process(board, i, j + 1, w, k + 1);

        board[i][j] = temp;
        return ans;
    }

    public static void main(String[] args) {
        char[][] board = {{'A', 'B', 'C', 'E' }, {'S', 'F', 'C', 'S' }, {'A', 'D', 'E', 'E' }};
        exist(board, "ABCCED");
        
    }
}
