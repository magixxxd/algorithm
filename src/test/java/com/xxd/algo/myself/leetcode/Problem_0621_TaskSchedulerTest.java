package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-26 12:02
 * @description:
 */
public class Problem_0621_TaskSchedulerTest {

    public static int leastInterval(char[] tasks, int free) {
        int[] count = new int[256];
        int maxCount = 0;
        for (char task : tasks) {
            count[task]++;
            maxCount = Math.max(maxCount, count[task]);
        }

        int maxKinds = 0;
        for (int i = 0; i < 256; i++) {
            if (count[i] == maxCount) {
                maxKinds++;
            }
        }

        int tasksExceptFinalTeam = tasks.length - maxKinds;
        int spaces = (free + 1) * (maxCount - 1);
        int restSpaces = Math.max(0, spaces - tasksExceptFinalTeam);


        return tasks.length + restSpaces;
    }
}
