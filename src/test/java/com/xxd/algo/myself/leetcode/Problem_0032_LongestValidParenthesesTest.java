package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-22 10:27
 * @description:
 */
public class Problem_0032_LongestValidParenthesesTest {


    public static int longestValidParentheses2(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }

        char[] chas = str.toCharArray();
        int[] dp = new int[chas.length];

        int res = 0;
        int pre = 0;
        for (int i = 1; i < chas.length; i++) {
            if (chas[i] == ')') {
                pre = i - dp[i - 1] - 1;
                if (pre >= 0 && chas[pre] == '(') {
                    dp[i] = dp[i - 1] + 2  + (pre - 1 > 0 ? dp[pre - 1] : 0);
                }
            }
            res = Math.max(dp[i], res);
        }

        return res;
    }

    public static int longestValidParentheses(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }

        char[] chas = str.toCharArray();
        int[] dp = new int[chas.length];

        int pre = 0;
        int res = 0;
        for (int i = 1; i < chas.length; i++) {
            if (chas[i] == ')') {
                pre = i - dp[i - 1] - 1;
                if (pre >= 0 && chas[pre] == '(') {
                    dp[i] = dp[i - 1] + 2 + (pre - 1 > 0 ? dp[pre - 1] : 0);
                }
            }

            res = Math.max(res, dp[i]);
        }

        return res;
    }
}
