package com.xxd.algo.myself.leetcode;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-03 10:02
 * @description:
 */
public class Problem_0166_FractionToRecurringDecimalTest {

    public String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) {
            return "0";
        }


        StringBuilder res = new StringBuilder();
        res.append(((numerator > 0) ^ (denominator > 0)) ? "-" : "");

        long num = Math.abs(numerator);
        long den = Math.abs(denominator);

        res.append(num / den);
        num %= den;
        if (num == 0) {
            return res.toString();
        }

        res.append(".");
        HashMap<Long, Integer> map = new HashMap<>();
        map.put(num, res.length());
        while (num != 0) {
            num *= 10;
            res.append(num / den);
            num %= den;

            if (map.containsKey(num)) {
                Integer index = map.get(num);
                res.insert(index, "(");
                res.append(")");
                break;
            } else {
                map.put(num, res.length());
            }
        }

        return res.toString();
    }
}
