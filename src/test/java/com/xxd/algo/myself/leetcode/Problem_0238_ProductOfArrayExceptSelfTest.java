package com.xxd.algo.myself.leetcode;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 18:12
 * @description:
 */
public class Problem_0238_ProductOfArrayExceptSelfTest {


    public int[] productExceptSelf(int[] nums) {
        int zeros = 0;
        int all = 1;
        for (int num : nums) {
            if (num == 0) {
                zeros++;
            } else {
                all *= num;
            }
        }

        if (zeros > 1) {
            Arrays.fill(nums, 0);
        } else {
            if (zeros == 0) {
                for (int i = 0; i < nums.length; i++) {
                    nums[i] = all / nums[i];
                }
            } else {
                for (int i = 0; i < nums.length; i++) {
                    nums[i] = nums[i] == 0 ? all : 0;
                }
            }
        }


        return nums;
    }

    public int[] productExceptSelf2(int[] nums) {
        int[] ans = new int[nums.length]; // i 左侧所有数的乘积
        ans[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            ans[i] = ans[i - 1] * nums[i - 1];
        }

        int R = 1; // 当前数右侧所有i的乘积
        for (int i = nums.length - 1; i >= 0; i--) {
            ans[i] = ans[i] * R;
            R = R * nums[i];
        }


        return ans;
    }
}
