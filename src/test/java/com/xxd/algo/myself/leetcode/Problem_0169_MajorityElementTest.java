package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-03 10:25
 * @description:
 */
public class Problem_0169_MajorityElementTest {

    public static int majorityElement(int[] nums) {
        int cand = 0;
        int HP = 0;
        for (int i = 0; i < nums.length; i++) {
            if (HP == 0) {
                cand = nums[i];
                HP = 1;
            } else if(nums[i] == cand) {
                HP++;
            } else {
                HP--;
            }
        }
        return cand;
    }


    public static void main(String[] args) {
        int[] nums = {2, 2, 1, 1, 1, 2, 2, 2, 3};
        System.out.println(majorityElement(nums));
    }
}
