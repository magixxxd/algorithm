package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-19 09:24
 * @description:
 */
public class Problem_0046_PermutationsTest {

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        process(nums, ans, 0);
        return ans;
    }

    public void process(int[] nums,
                        List<List<Integer>> ans,
                        int index) {
        if (index == nums.length) {
            List<Integer> path = new ArrayList<>();
            for (int num : nums) {
                path.add(num);
            }
            ans.add(path);
        } else {
            for (int i = index; i < nums.length; i++) { // 哪个位置当开头位置
                swap(nums, i, index); // 和当前位置交换
                process(nums, ans, index + 1);
                swap(nums, i, index); // 换回来
            }
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
