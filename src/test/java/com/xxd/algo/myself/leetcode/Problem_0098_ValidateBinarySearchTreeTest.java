package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 17:41
 * @description:
 */
public class Problem_0098_ValidateBinarySearchTreeTest {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public static class Info {
        int max;
        int min;
        boolean isBST;

        public Info(int max, int min, boolean isBST) {
            this.max = max;
            this.min = min;
            this.isBST = isBST;
        }
    }

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return false;
        }
        Info info = process(root);
        return info.isBST;
    }

    private Info process(TreeNode root) {
        if (root == null) {
            return null;
        }

        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);

        int max = root.val;
        int min = root.val;
        if (leftInfo != null) {
            max = Math.max(max, leftInfo.max);
            min = Math.min(min, leftInfo.min);
        }

        if (rightInfo != null) {
            max = Math.max(max, rightInfo.max);
            min = Math.min(min, rightInfo.min);
        }

        boolean isBST = false;
        boolean left = leftInfo == null || (leftInfo.isBST && leftInfo.max < root.val);
        boolean right = rightInfo == null || (rightInfo.isBST && rightInfo.min > root.val);

        isBST = left && right;

        return new Info(max, min, isBST);
    }


    public static boolean isValidBST1(TreeNode root) {
        if (root == null) {
            return false;
        }

        Info info = process1(root);

        return info.isBST;
    }

    private static Info process1(TreeNode x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process1(x.left);
        Info rightInfo = process1(x.right);

        int max = Integer.MIN_VALUE;
        if (leftInfo != null) {
            max = Math.max(max, leftInfo.max);
        }

        if (rightInfo != null) {
            max = Math.max(max, rightInfo.max);
        }
        max = Math.max(max, x.val);

        int min = Integer.MAX_VALUE;
        if (leftInfo != null) {
            min = Math.min(min, leftInfo.max);
        }

        if (rightInfo != null) {
            min = Math.min(min, rightInfo.max);
        }

        min = Math.min(min, x.val);

        boolean isBST = false;

        if (leftInfo == null && rightInfo == null) {
            isBST = true;
        }

        if (leftInfo != null && rightInfo != null) {
            isBST = leftInfo.isBST && rightInfo.isBST && (leftInfo.max < x.val && rightInfo.min > x.val);
        }

        if (leftInfo == null && rightInfo != null) {
            isBST = rightInfo.isBST && rightInfo.min > x.val;
        }

        if (leftInfo != null && rightInfo == null) {
            isBST = leftInfo.isBST && leftInfo.max < x.val;
        }

        return new Info(max, min, isBST);
    }


}
