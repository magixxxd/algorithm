package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0230_KthSmallestElementInBST;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 10:43
 * @description:
 */
public class Problem_0230_KthSmallestElementInBSTTest {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }


    }

    static AtomicInteger kth = null;

    public static int kthSmallest(TreeNode head, int k) {
        return process2(head, k);
    }

    private static int process(TreeNode x, int k) {
        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || x != null) {
            if (x != null) {
                stack.push(x);
                x = x.left;
            } else {
                x = stack.pop();
                k--;
                if (k == 0) {
                    return x.val;
                }
                x = x.right;
            }
        }

        return 0;
    }
    // 此方法更优
    public static int process2(TreeNode x, int k) {
        int leftN = findChild(x.left);
        if (leftN + 1 == k) {
            return x.val;
        }

        if (leftN >= k) {
            return process2(x.left, k);
        }

        return process2(x.right, k - leftN - 1);
    }

    private static int findChild(TreeNode node) {
        if (node == null) {
            return 0;
        }

        return findChild(node.left) + findChild(node.right) + 1;
    }


    public static void main(String[] args) {
        TreeNode x = new TreeNode(3);
        x.left = new TreeNode(1);
        x.left.left = new TreeNode(-2);
        x.left.right = new TreeNode(2);
        x.right = new TreeNode(4);

        System.out.println(kthSmallest(x, 1));
    }
}
