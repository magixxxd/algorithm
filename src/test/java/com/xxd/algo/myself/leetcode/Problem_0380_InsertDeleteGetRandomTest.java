package com.xxd.algo.myself.leetcode;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-21 10:05
 * @description:
 */
public class Problem_0380_InsertDeleteGetRandomTest {

    public static class RandomizedSet {

        private HashMap<Integer, Integer> keyIndexMap;
        private HashMap<Integer, Integer> indexKeyMap;
        private int size;

        public RandomizedSet() {
            keyIndexMap = new HashMap<>();
            indexKeyMap = new HashMap<>();
            size = 0;
        }

        public boolean insert(int val) {
            if (!keyIndexMap.containsKey(val)) {
                keyIndexMap.put(val, size);
                indexKeyMap.put(size++, val);
                return true;
            }

            return false;
        }


        public boolean remove(int val) {
            if (keyIndexMap.containsKey(val)) {
                int deleteIndex = keyIndexMap.get(val);
                int lastIndex = --size;
                int lastKey = indexKeyMap.get(lastIndex);

                keyIndexMap.put(lastKey, deleteIndex);
                indexKeyMap.put(deleteIndex, lastKey);

                keyIndexMap.remove(val);
                indexKeyMap.remove(lastIndex);
                return true;
            }

            return false;
        }

        public int getRandom() {
            if (size == 0) {
                return -1;
            }

            int randomIndex = (int) (Math.random() * size);
            return indexKeyMap.get(randomIndex);
        }
    }

    public static void main(String[] args) {
        RandomizedSet set = new RandomizedSet();
        set.insert(1);
        set.insert(2);
        set.insert(5);
        set.insert(6);
        set.insert(8);

        set.remove(6);
    }
}
