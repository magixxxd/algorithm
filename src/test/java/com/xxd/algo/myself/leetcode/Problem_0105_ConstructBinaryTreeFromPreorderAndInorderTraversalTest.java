package com.xxd.algo.myself.leetcode;


import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 18:29
 * @description:
 */
public class Problem_0105_ConstructBinaryTreeFromPreorderAndInorderTraversalTest {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }
    }

    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) {
            map.put(inorder[i], i);
        }
        return f(preorder, 0, preorder.length - 1,
                inorder, 0, inorder.length - 1,
                map);
    }

    private static TreeNode f(int[] pre, int L1, int R1,
                              int[] in, int L2, int R2,
                              HashMap<Integer, Integer> map) {

        if (L1 > R1) {
            return null;
        }
        TreeNode head = new TreeNode(pre[L1]);
        if (L1 == R1) {
            return head;
        }

        Integer findIndex = map.get(pre[L1]);
        head.left = f(pre, L1 + 1, findIndex - L2 + L1, in, L2, findIndex - 1, map);
        head.right = f(pre, findIndex - L2 + L1 + 1, R1, in, findIndex + 1, R2, map);
        return head;
    }


    public static TreeNode buildTree2(int[] preorder, int[] inorder) {
        Map<Integer, Integer> midMap = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) {
            midMap.put(inorder[i], i);
        }

        return process(preorder, 0, preorder.length - 1,
                inorder, 0, inorder.length - 1,
                midMap
        );

    }

    private static TreeNode process(int[] preorder, int L1, int R1, int[] inorder, int L2, int R2,
                                    Map<Integer, Integer> midMap) {
        if (L1 > R1) {
            return null;
        }

        int headVal = preorder[L1];
        TreeNode head = new TreeNode(headVal);
        if (L1 == R1) {
            return head;
        }

        Integer findIndex = midMap.get(headVal);
        head.left = process(
                preorder, L1 + 1, L1 + findIndex - L2,
                inorder, L2, findIndex - 1,
                midMap
        );

        head.right = process(preorder, L1 + findIndex - L2 + 1, R1,
                inorder, findIndex + 1, R2,
                midMap
        );


        return head;
    }
}
