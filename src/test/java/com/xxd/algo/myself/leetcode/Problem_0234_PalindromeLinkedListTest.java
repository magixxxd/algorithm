package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 11:28
 * @description:
 */
public class Problem_0234_PalindromeLinkedListTest {

    public static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public static boolean isPalindrome(ListNode head) {
        if (head == null || head.next == null) {
            return true;
        }

        Stack<Integer> stack = new Stack<>();

        ListNode cur = head;
        while (cur != null) {
            stack.push(cur.val);
            cur = cur.next;
        }

        cur = head;
        while (cur != null) {
            if (cur.val != stack.pop()) {
                return false;
            }

            cur = cur.next;
        }

        return true;
    }

    public static boolean isPalindrome2(ListNode head) {
        if (head == null || head.next == null) {
            return true;
        }

        ListNode n1 = head;
        ListNode n2 = head;
        while (n2.next != null && n2.next.next != null) {
            n1 = n1.next;
            n2 = n2.next.next;
        }

        n2 = n1.next;
        n1.next = null;
        n1 = null;
        ListNode next = null;
        while (n2 != null) {
            next = n2.next;
            n2.next = n1;
            n1 = n2;
            n2 = next;
        }

        n2 = n1;
        n1 = head;
        while (n1 != null && n2 != null) {
            if (n1.val != n2.val) {
                return false;
            }

            n1 = n1.next;
            n2 = n2.next;
        }

        return true;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(0);
        head.next.next = new ListNode(0);
        isPalindrome2(head);
    }
}
