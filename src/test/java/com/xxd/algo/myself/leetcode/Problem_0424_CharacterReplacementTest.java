package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-29 17:21
 * @description:
 */
public class Problem_0424_CharacterReplacementTest {

    public static int characterReplacement(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        if (s.length() - 1 <= k) {
            return s.length();
        }
        char[] str = s.toCharArray();
        int[] count = new int[26];
        int end = 0;
        int start = 0;
        int maxTimes = 0;
        while (end < str.length) {
            count[str[end] - 'A']++;

            // 子串出现的最大词频
            maxTimes = Math.max(maxTimes, count[str[end] - 'A']);

            // 统计此时的子串长度
            if (end - start + 1 - maxTimes > k) {
                // start 右移
                count[str[start] - 'A']--;
                start++;
            }
            end++;
        }

        return end - start;
    }

    public static void main(String[] args) {
        String s = "AABCABDB";
        int k = 2;
        System.out.println(characterReplacement(s, k));

    }
}
