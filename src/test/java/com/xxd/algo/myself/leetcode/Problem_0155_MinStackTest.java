package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-02 10:34
 * @description:
 */
public class Problem_0155_MinStackTest {

    Stack<Integer> data;
    Stack<Integer> min;

    public Problem_0155_MinStackTest () {
        data = new Stack<>();
        min = new Stack<>();
    }

    public void push(int x) {
        data.push(x);
        if (min.isEmpty()) {
            min.push(x);
        } else {
            min.push(Math.min(x, min.peek()));
        }
    }

    public void pop() {
        data.pop();
        min.pop();
    }

    public int top() {
        return data.peek();
    }

    public int getMin() {
        return min.peek();
    }
}
