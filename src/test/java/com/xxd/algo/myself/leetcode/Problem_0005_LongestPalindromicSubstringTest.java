package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-27 06:41
 * @description:
 */
public class Problem_0005_LongestPalindromicSubstringTest {


    public static String longestPalindrome(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char[] charArr = manacherString(str);

        int[] pArr = new int[charArr.length];
        int C = -1;
        int R = -1;
        int max = Integer.MIN_VALUE;
        int mid = -1;
        for (int i = 0; i < charArr.length; i++) {
            pArr[i] = R > i
                    ? Math.min(pArr[2 * C - i], R - i)
                    : 1;

            while (i + pArr[i] < charArr.length && i - pArr[i] > -1) {
                if (charArr[i + pArr[i]] == charArr[i - pArr[i]]) {
                    pArr[i]++;
                } else {
                    break;
                }
            }

            if (i + pArr[i] > R) {
                R = i + pArr[i];
                C = i;
            }

            if (max < pArr[i]) {
                max = pArr[i];
                mid = i;
            }
        }
        mid = (mid - 1) / 2;
        max = max - 1; //  如果max 是奇数说明我原先的字符串长度就是偶数

        return str.substring((max & 1) == 0 ? mid - (max / 2) + 1 : mid - (max / 2), mid + (max / 2) + 1);
    }

    private static char[] manacherString(String str) {
        char[] charArr = str.toCharArray();
        char[] res = new char[str.length() * 2 + 1];
        int index = 0;
        for (int i = 0; i != res.length; i++) {
            res[i] = (i & 1) == 0 ? '#' : charArr[index++];
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(longestPalindrome("cbbd"));


        String a = "abcd";
        System.out.println(a.substring(0, 2));
    }
}
