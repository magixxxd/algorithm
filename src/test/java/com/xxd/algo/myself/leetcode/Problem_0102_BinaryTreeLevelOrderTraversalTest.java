package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class Problem_0102_BinaryTreeLevelOrderTraversalTest {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }
    }

    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null) {
            return null;
        }

        List<List<Integer>> res = new ArrayList<>();

        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int size = 0;
        while (!queue.isEmpty()) {
            size = queue.size();
            List<Integer> cueLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode curNode = queue.pollFirst();
                cueLevel.add(curNode.val);
                if (curNode.left != null) {
                    queue.addLast(curNode.left);
                }

                if (curNode.right != null) {
                    queue.addLast(curNode.right);
                }
            }

            res.add(cueLevel);
        }

        return res;
    }


    public List<List<Integer>> levelOrder2(TreeNode root) {
        List<List<Integer>> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.addLast(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            TreeNode curNode = null;
            List<Integer> curLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                curNode = queue.pollFirst();
                curLevel.add(curNode.val);

                if (curNode.left != null) {
                    queue.addLast(curNode.left);
                }

                if (curNode.right != null) {
                    queue.addLast(curNode.right);
                }
            }
            ans.add(curLevel);
        }

        return ans;
    }

}
