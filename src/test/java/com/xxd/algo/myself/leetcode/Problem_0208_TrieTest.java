package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-07 19:21
 * @description:
 */
public class Problem_0208_TrieTest {

    public static class Node {
        public boolean end;

        public Node[] nexts;

        public Node() {
            end = false;
            nexts = new Node[26];
        }
    }

    Node root;

    public Problem_0208_TrieTest() {
        this.root = new Node();
    }

    public void insert(String word) {
        if (word == null) {
            return;
        }
        Node cur = root;
        char[] str = word.toCharArray();
        int path = 0;
        for (int i = 0; i < str.length; i++) {
            path = str[i] - 'a';
            if (cur.nexts[path] == null) {
                cur.nexts[path] = new Node();
            }
            cur = cur.nexts[path];
        }
        cur.end = true;
    }

    public boolean search(String word) {
        if (word == null) {
            return false;
        }

        Node cur = root;
        char[] str = word.toCharArray();
        int path = 0;
        for (int i = 0; i < str.length; i++) {
            path = str[i] - 'a';
            if (cur.nexts[path] == null) {
                return false;

            }
            cur = cur.nexts[path];
        }

        return cur.end;
    }

    public boolean startsWith(String prefix) {
        if (prefix == null) {
            return false;
        }

        Node cur = root;
        char[] str = prefix.toCharArray();
        int path = 0;
        for (int i = 0; i < str.length; i++) {
            path = str[i] - 'a';
            if (cur.nexts[path] == null) {
                return false;
            }
            cur = cur.nexts[path];
        }

        return true;
    }
}
