package com.xxd.algo.myself.leetcode;

import java.util.HashMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-25 09:57
 * @description:
 */
public class Problem_0560_SubarraySumEqualsKTest {


    public static int subarraySum(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        HashMap<Integer, Integer> preSumTimesMap = new HashMap<>();
        preSumTimesMap.put(0, 1);

        int all = 0;
        int ans = 0;
        for (int i = 0; i < nums.length; i++) {
            all += nums[i];
            int cha = all - k;
            if (preSumTimesMap.containsKey(cha)) {
                ans += preSumTimesMap.get(all - k);
            }

            if (!preSumTimesMap.containsKey(all)) {
                preSumTimesMap.put(all, 1);
            } else {
                preSumTimesMap.put(all, preSumTimesMap.get(all) + 1);
            }
        }

        return ans;
    }
}
