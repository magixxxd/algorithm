package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-23 10:36
 * @description:
 */
public class Problem_0221_MaximalSquareTest {

    public static int maximalSquare(char[][] m) {
        if (m == null || m.length == 0 || m[0].length == 0) {
            return 0;
        }

        int N = m.length;
        int M = m[0].length;
        int[][] dp = new int[N][M];
        int max = 0;
        for (int i = 0; i < N; i++) {
            if (m[i][0] == '1') {
                dp[i][0] = 1;
                max = 1;
            }
        }
        for (int j = 1; j < M; j++) {
            if (m[0][j] == '1') {
                dp[0][j] = 1;
                max = 1;
            }
        }
        for (int i = 1; i < N; i++) {
            for (int j = 1; j < M; j++) { // 普遍位置
                if (m[i][j] == '1') {
                    dp[i][j] = Math.min(
                            Math.min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j - 1]
                    ) + 1;

                    max = Math.max(max, dp[i][j]);
                }
            }
        }

        return max * max;
    }

    public static int maximalSquare2(char[][] m) {
        int row = m.length;
        int col = m[0].length;

        int[][] down = new int[row][col];
        int[][] right = new int[row][col];

        buildArray(m, down, right);


        // 枚举最大的边长
        for (int size = Math.min(row, col); size > 0; size--) {
            if (hasSizeOfBorder(size, right, down)) {
                return size;
            }
        }

        return 0;
    }

    private static boolean hasSizeOfBorder(int size, int[][] right, int[][] down) {
        // 从矩形的 左上角开始枚举
        for (int i = 0; i < right.length - size + 1; i++) {
            for (int j = 0; j < right[0].length - size + 1; j++) {
                if (right[i][j] >= size && down[i][j] >= size
                        && right[i + size - 1][j] >= size
                        && down[i][j + size - 1] >= size) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void buildArray(char[][] m, int[][] down, int[][] right) {
        int r = m.length;
        int c = m[0].length;
        if (m[r - 1][c - 1] == '1') {
            down[r - 1][c - 1] = 1;
            right[r - 1][c - 1] = 1;
        }
        // 最后一行的数据
        for (int i = c - 2; i >= 0; i--) {
            if (m[r - 1][i] == '1') {
                right[r - 1][i] = 1 + right[r - 1][i + 1];
                down[r - 1][i] = 1;
            }
        }

        // 最后一列的数据
        for (int i = r - 2; i >= 0; i--) {
            if (m[i][c - 1] == '1') {
                right[i][c - 1] = 1;
                down[i][c - 1] = 1 + down[i + 1][c - 1];
            }
        }

        // 从右下角开始遍历
        for (int i = r - 2; i >= 0; i--) {
            for (int j = c - 2; j >= 0; j--) {
                if (m[i][j] == '1') {
                    right[i][j] = right[i][j + 1] + 1;
                    down[i][j] = down[i + 1][j] + 1;
                }
            }
        }
    }

    public static void main(String[] args) {
        char[][] m = {
                {'1', '0', '1', '1', '0', '1'},
                {'1', '1', '1', '1', '1', '1'},
                {'0', '1', '1', '0', '1', '1'},
                {'1', '1', '1', '0', '1', '0'},
                {'0', '1', '1', '1', '1', '1'},
                {'1', '1', '0', '1', '1', '1'},
        };

        int max = maximalSquare2(m);
        System.out.println(max * max);
    }
}
