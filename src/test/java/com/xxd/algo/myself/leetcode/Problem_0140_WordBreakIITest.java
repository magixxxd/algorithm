package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0140_WordBreakII;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-01 17:55
 * @description:
 */
public class Problem_0140_WordBreakIITest {

    public static class Node {
        public String path;
        public boolean end;
        public Node[] nexts;

        public Node() {
            path = null;
            end = false;
            nexts = new Node[26];
        }
    }

    public static List<String> wordBreak(String s, List<String> wordDict) {
        char[] str = s.toCharArray();
        Node root = gettrie(wordDict);

        ArrayList<String> path = new ArrayList<>();
        List<String> ans = new ArrayList<>();
        process(str, 0, root, path, ans);
        return ans;
    }

    private static void process(char[] str,
                                int index,
                                Node root,
                                ArrayList<String> path,
                                List<String> ans) {

        if (index == str.length) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < path.size() - 1; i++) {
                builder.append(path.get(i) + " ");
            }
            builder.append(path.get(path.size() - 1));
            ans.add(builder.toString());
        } else {
            Node cur = root;
            for (int end = index; end < str.length; end++) {
                int road = str[end] - 'a';
                if (cur.nexts[road] == null) {
                    break;
                }

                cur = cur.nexts[road];

                if (cur.end) {
                    path.add(cur.path);
                    process(str, end + 1, root, path, ans);
                    path.remove(path.size() - 1);
                }
            }
        }
    }

    private static Node gettrie(List<String> wordDict) {
        Node root = new Node();

        for (String str : wordDict) {
            char[] chs = str.toCharArray();
            Node node = root;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = chs[i] - 'a';
                if (node.nexts[index] == null) {
                    node.nexts[index] = new Node();
                }
                node = node.nexts[index];
            }

            node.path = str;
            node.end = true;
        }
        return root;
    }
}
