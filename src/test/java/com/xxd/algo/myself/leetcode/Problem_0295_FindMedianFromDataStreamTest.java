package com.xxd.algo.myself.leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-15 10:49
 * @description:
 */
public class Problem_0295_FindMedianFromDataStreamTest {
    private PriorityQueue<Integer> max;

    private PriorityQueue<Integer> min;


    public Problem_0295_FindMedianFromDataStreamTest() {
        max = new PriorityQueue<>(new MaxHeapComparator());
        min = new PriorityQueue<>(new MinHeapComparator());
    }

    public void addNum(int num) {
        if (max.isEmpty()) {
            max.add(num);
        } else {
            if (max.peek() >= num) {
                max.add(num);
            } else {
                min.add(num);
            }
        }

        balance();
    }

    public double findMedian() {
        if (max.size() == min.size()) {
            return (max.peek() + min.peek()) / 2;
        } else {
            return max.size() > min.size() ? max.peek() : min.peek();
        }
    }

    private void balance() {
        if (max.size() == min.size() + 2) {
            min.add(max.poll());
        }

        if (min.size() == max.size() + 2) {
            max.add(min.poll());
        }
    }

    public static class MaxHeapComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2 - o1;
        }
    }

    public static class MinHeapComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o1 - o2;
        }
    }

}

