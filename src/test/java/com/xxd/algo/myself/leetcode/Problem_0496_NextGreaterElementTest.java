package com.xxd.algo.myself.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-09 09:47
 * @description:
 */
public class Problem_0496_NextGreaterElementTest {

    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int N = nums1.length;
        int M = nums2.length;

        Stack<Integer> stack = new Stack<>();
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < M; i++) {
            while (!stack.isEmpty() && stack.peek() < nums2[i]) {
                Integer cur = stack.pop();
                map.put(cur, nums2[i]);
            }

            stack.push(nums2[i]);
        }

        while (!stack.isEmpty()) {
            Integer cur = stack.pop();
            map.put(cur, -1);
        }

        for (int i = 0; i < N; i++) {
            nums1[i] = map.getOrDefault(nums1[i], -1);
        }

        return nums1;
    }

    public static void main(String[] args) {
        int[] nums1 = {2, 4};
        int[] nums2 = {1, 2, 3, 4};
        System.out.println(Arrays.toString(nextGreaterElement(nums1, nums2)));
    }
}
