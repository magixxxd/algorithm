package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-24 09:59
 * @description:
 */
public class Problem_0406_QueueReconstructionByHeightTest {

    public static class Unit {
        public int h;
        public int k;

        public Unit(int h, int k) {
            this.h = h;
            this.k = k;
        }
    }

    public static class UnitComparator implements Comparator<Unit> {

        @Override
        public int compare(Unit o1, Unit o2) {
            return o1.h != o2.h ? o2.h - o1.h : o1.k - o2.k;
        }
    }

    public static int[][] reconstructQueue(int[][] people) {
        int N = people.length;
        Unit[] units = new Unit[N];
        for (int i = 0; i < N; i++) {
            units[i] = new Unit(people[i][0], people[i][1]);
        }

        Arrays.sort(units, new UnitComparator());

        List<Unit> list = new ArrayList<>();
        for (Unit unit : units) {
            list.add(unit.k, unit);
        }

        int[][] ans = new int[N][2];
        int index = 0;
        for (Unit unit : list) {
            ans[index][0] = unit.h;
            ans[index++][1] = unit.k;
        }
        return ans;
    }

    public static void main(String[] args) {
        int[][] people = {
                {7, 0},
                {4, 4},
                {7, 1},
                {5, 0},
                {6, 1},
                {5, 2},
        };

        reconstructQueue(people);
    }
}
