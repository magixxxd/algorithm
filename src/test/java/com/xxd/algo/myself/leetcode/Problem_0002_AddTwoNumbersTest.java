package com.xxd.algo.myself.leetcode;

public class Problem_0002_AddTwoNumbersTest {

    // 不要提交这个类描述
    public static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int value) {
            this.val = value;
        }
    }

    public static ListNode addTwoNumbers2(ListNode head1, ListNode head2) {
        if (head1 == null || head2 == null) {
            return null;
        }

        ListNode head = new ListNode(0);
        ListNode listNode = head;

        int curVal = 0;
        int carry = 0;
        while (head1 != null && head2 != null) {
            curVal = head1.val + head2.val + carry;

            carry = curVal >= 10 ? 1 : 0; // 是否产生进位

            int res = curVal % 10;
            listNode.next = new ListNode(res);
            listNode = listNode.next;

            head1 = head1.next;
            head2 = head2.next;
        }

        while (head1 != null) {
            curVal = head1.val + carry;
            carry = curVal >= 10 ? 1 : 0;
            int res = curVal % 10;

            listNode = listNode.next = new ListNode(res);
            head1 = head1.next;
        }

        while (head2 != null) {
            curVal = head2.val + carry;
            carry = curVal >= 10 ? 1 : 0;
            int res = curVal % 10;

            listNode = listNode.next = new ListNode(res);
            head2 = head2.next;
        }

        if (carry == 1) {
            listNode.next = new ListNode(carry);
        }

        return head.next;
    }

    public static ListNode addTwoNumbers(ListNode head1, ListNode head2) {
        if (head1 == null || head2 == null) {
            return null;
        }

        ListNode headNode = new ListNode(0);
        ListNode listNode = headNode;
        int curVal = 0;
        int carry = 0;
        while (head1 != null && head2 != null) {
            curVal = head1.val + head2.val + carry;

            int res = curVal % 10;
            carry = curVal >= 10 ? 1 : 0;

            listNode.next = new ListNode(res);
            listNode = listNode.next;

            head1 = head1.next;
            head2 = head2.next;
        }

        while (head1 != null) {
            curVal = head1.val + carry;
            carry = curVal >= 10 ? 1 : 0;
            int res = curVal % 10;

            listNode = listNode.next = new ListNode(res);
            head1 = head1.next;
        }

        while (head2 != null) {
            curVal = head2.val + carry;
            carry = curVal >= 10 ? 1 : 0;
            int res = curVal % 10;

            listNode = listNode.next = new ListNode(res);
            head2 = head2.next;
        }

        if (carry == 1) {
            listNode.next = new ListNode(carry);
        }

        return headNode.next;
    }




    public static void main(String[] args) {
        ListNode head1 = new ListNode(9);
        head1.next = new ListNode(9);
        head1.next.next = new ListNode(9);
        head1.next.next.next = new ListNode(9);
        head1.next.next.next.next = new ListNode(9);
        head1.next.next.next.next.next = new ListNode(9);
        head1.next.next.next.next.next.next = new ListNode(9);

        ListNode head2 = new ListNode(9);
        head2.next = new ListNode(9);
        head2.next.next = new ListNode(9);
        head2.next.next.next = new ListNode(9);

        addTwoNumbers(head1, head2);
    }
}
