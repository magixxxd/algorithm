package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-07 07:35
 * @description:
 */
public class Problem_0015_3SumTest {

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            //  开始收集答案
            if (i == 0 || nums[i] != nums[i - 1]) {
                List<List<Integer>> nests = twoSum(nums, i + 1, -nums[i]);
                for (List<Integer> next : nests) {
                    next.add(0, nums[i]);
                    ans.add(next);
                }
            }
        }

        return ans;
    }

    /**
     * nums[begin......]范围上，找到累加和为target的所有二元组
     *
     * @param nums
     * @param begin
     * @param target
     * @return
     */
    public static List<List<Integer>> twoSum(int[] nums, int begin, int target) {
        int L = begin;
        int R = nums.length - 1;

        List<List<Integer>> ans = new ArrayList<>();

        while (L < R) {
            if (nums[L] + nums[R] > target) {
                R--;
            } else if (nums[L] + nums[R] < target) {
                L++;
            } else {
                // 收集答案
                if (L == begin || nums[L] != nums[L - 1]) {
                    List<Integer> cur = new ArrayList<>();
                    cur.add(nums[L]);
                    cur.add(nums[R]);
                    ans.add(cur);
                }
                L++;
            }
        }

        return ans;
    }
}
