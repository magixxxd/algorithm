package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-08-30 18:55
 * @description:
 */
public class Problem_0107_LevelOrderBottomTest {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }
    
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }

        Stack<List<Integer>> stack = new Stack<>();
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int size = 0;
        while (!queue.isEmpty()) {
            size = queue.size();
            List<Integer> cueLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode curNode = queue.pollFirst();
                cueLevel.add(curNode.val);
                if (curNode.left != null) {
                    queue.addLast(curNode.left);
                }

                if (curNode.right != null) {
                    queue.addLast(curNode.right);
                }
            }

            stack.add(cueLevel);
        }

        while (!stack.isEmpty()) {
            res.add(stack.pop());
        }

        return res;
    }
}
