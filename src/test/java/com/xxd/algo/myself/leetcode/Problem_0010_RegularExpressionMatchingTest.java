package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-29 10:09
 * @description:
 */
public class Problem_0010_RegularExpressionMatchingTest {

    public static boolean isMatch(String s, String p) {
        if (s == null || p == null) {
            return false;
        }

        char[] str = s.toCharArray();
        char[] pattern = p.toCharArray();

        return isValid(str, pattern) && process(str, pattern, 0, 0);
    }

    /**
     * str 不允许有 . * 只包含 a - z
     * pattern 开头不允许是* ， 且 不允许出现两个连续的*之类的
     *
     * @param str
     * @param pattern
     * @return
     */
    private static boolean isValid(char[] str, char[] pattern) {
        for (char s : str) {
            if (s == '.' || s == '*') {
                return false;
            }
        }
        for (int i = 0; i < pattern.length; i++) {
            if (pattern[i] == '*' && (i == 0 || pattern[i - 1] == '*')) {
                return false;
            }
        }
        return true;
    }

    /**
     * str [si ... ] 能否被 pattern[pi....] 变出来
     *
     * @param str
     * @param pattern
     * @param si
     * @param pi
     * @return
     */
    private static boolean process(char[] str,
                                   char[] pattern,
                                   int si,
                                   int pi) {
        // base case
        if (si == str.length) { // si 已经越界了
            if (pi == pattern.length) {
                return true;
            }

            // abccc  abc* 如果是这种情况的话，潜台词就是说当前位置 后面一个如果是*
            if (pi + 1 < pattern.length && pattern[pi + 1] == '*') {
                return process(str, pattern, si, pi + 2);
            }
            return false;
        }

        if (pi == pattern.length) {
            // si 没有越界，pi 已经走完了就肯定是 false了
            return false;
        }

        // 下一个位置不是*的情况
        if (pi + 1 >= pattern.length || pattern[pi + 1] != '*') {
            return (str[si] == pattern[pi] || pattern[pi] == '.') && process(str, pattern, si + 1, pi + 1);
        }

        // 下一个位置是 * 的情况
        if (process(str, pattern, si, pi + 2)) {
            return true;
        }

        // * 当 1 个 当 2 个
        while (si < str.length && (str[si] == pattern[pi] || pattern[pi] == '.')) {
            if (process(str, pattern, si + 1, pi + 2)) {
                return true;
            }
            si++;
        }

        return false;
    }

    public static void main(String[] args) {
        String a = "bbbba";
        String b = ".*a*a";
        System.out.println(isMatch(a, b));
        System.out.println("====");
    }
}
