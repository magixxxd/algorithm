package com.xxd.algo.myself.leetcode;

import java.util.LinkedList;
import java.util.TreeSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-02 09:23
 * @description:
 */
public class Problem_0220_ContainsNearbyAlmostDuplicateTest {

    public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        // 1. 使用有序集合来维护大小为k的滑动窗口
        int n = nums.length;
        TreeSet<Long> set = new TreeSet<Long>(); // long为了防止溢出

        // 2. 处理逻辑：遍历每个元素，滑动窗口包含了nums[i]的前k个元素，检查是否落在了[nums[i] - t,nums[i] + t]的区间内
        for (int i = 0; i < n; i++) {
            // 2.1 判断有序集合中是否存在符合条件的元素
            // ceiling()：方法返回在这个集合中大于或者等于给定元素的最小元素，如果不存在这样的元素,返回null.
            Long ceiling = set.ceiling((long) nums[i] - (long) t);

            if (ceiling != null && ceiling <= (long) nums[i] + (long) t) {  // 存在这样一对元素就直接返回
                return true;
            }

            // 2.2 不存在的话暂时添加到有序集合中
            set.add((long) nums[i]);

            // 2.3 维护大小为k的滑动窗口，删除超出范围的最早的元素
            if (i >= k) {
                set.remove((long) nums[i - k]);
            }
        }
        // 3. 都不满足返回false
        return false;
    }


    public static boolean containsNearbyAlmostDuplicate2(int[] nums, int k, int t) {
        int n = nums.length;
        LinkedList<Integer> minQueue = new LinkedList<>(); // 最小值的双端队列
        LinkedList<Integer> maxQueue = new LinkedList<>(); // 最大值的双端队列

        int R = 0;
        while (R < n) {
            while (!minQueue.isEmpty() && nums[minQueue.peekFirst()] >= nums[R]) {
                minQueue.pollLast();
            }
            minQueue.offerLast(R);

            while (!maxQueue.isEmpty() && nums[maxQueue.peekFirst()] <= nums[R]) {
                maxQueue.pollLast();
            }
            maxQueue.offerLast(R);

            if (maxQueue.peekFirst() != minQueue.peekFirst() && nums[maxQueue.peekFirst()] - nums[minQueue.peekFirst()] <= t) {
                return true;
            }

            if (minQueue.peekFirst() == R - k) { // 窗口过期数据
                minQueue.pollFirst();
            }

            if (maxQueue.peekFirst() == R - k) { // 窗口过期数据
                maxQueue.pollFirst();
            }

            R++;
        }

        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,1};
        int k = 3;
        int t = 0;
        System.out.println(containsNearbyAlmostDuplicate(nums, k, t));
        System.out.println(containsNearbyAlmostDuplicate2(nums, k, t));
    }
}
