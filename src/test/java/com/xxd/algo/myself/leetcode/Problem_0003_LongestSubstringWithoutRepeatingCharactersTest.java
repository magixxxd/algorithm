package com.xxd.algo.myself.leetcode;


import java.util.HashSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-22 10:00
 * @description:
 */
public class Problem_0003_LongestSubstringWithoutRepeatingCharactersTest {

    /**
     * 无重复字符的最长子串
     *
     * @param s
     * @return
     */
    public static int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() < 1) {
            return 0;
        }
        //Map<Character, Integer> map = new HashMap<>();
        int[] map = new int[256];
        for (int i = 0; i < 256; i++) {
            map[i] = -1;
        }

        int pre = -1; // i - 1 位置结尾的情况下，往前推到不能推的位置停止
        int cur = 0;
        int len = 0; // 答案
        for (int i = 0; i < s.length(); i++) {
            // i位置结尾的情况下，往左推，推不动的位置是谁
            // pre (i-1信息) -> pre(i 结尾信息)
            pre = Math.max(pre, map[s.charAt(i)]); // 哪个离我近
            cur = i - pre;
            len = Math.max(len, cur);
            map[s.charAt(i)] = i;
        }

        return len;
    }


    public static int lengthOfLongestSubstring5(String s) {
        if (s == null || s.length() < 1) {
            return 0;
        }

        int[] map = new int[256];
        for (int i = 0; i < 256; i++) {
            map[i] = -1;
        }
        char[] str = s.toCharArray();

        int[] dp = new int[str.length];
        dp[0] = 1;
        int ans = dp[0];
        map[str[0]] = 0;
        for (int i = 1; i < str.length; i++) {
            int preIndex = map[str[i]]; // 4
            int length = dp[i - 1]; // 3    i 6
            if (preIndex < i - length) {
                dp[i] = dp[i - 1] + 1;
            } else {
                dp[i] = i - preIndex;
            }
            ans = Math.max(ans, dp[i]);
            map[str[i]] = i;
        }

        return ans;
    }

    public static int lengthOfLongestSubstring2(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }

        int[] map = new int[256];
        int ans = 1;
        int R = 0;
        for (int L = 0; L < s.length(); L++) {
            while (R < s.length()) {
                if (map[s.charAt(R)] == 1) {
                    break;
                }

                map[s.charAt(R++)]++;
            }

            ans = Math.max(ans, R - L);
            if (R == s.length()) {
                return ans;
            }
            map[s.charAt(L)]--;
        }

        return ans;
    }

    public static int lengthOfLongestSubstring3(String s) {
        if (s == null || s.length() < 1) {
            return 0;
        }

        int[] map = new int[256];
        for (int i = 0; i < 256; i++) {
            map[i] = -1;
        }

        char[] str = s.toCharArray();
        int pre = -1; // i - 1 位置结尾的情况下，往前推到不能推的位置停止
        int ans = 0;
        int cur = 0;
        for (int i = 0; i < str.length; i++) {
            pre = Math.max(pre, map[str[i]]);
            cur = i - pre;
            ans = Math.max(ans, cur);
            map[str[i]] = i;
        }

        return ans;
    }

    public static void main(String[] args) {
        String str = "pwwkew";
        System.out.println(lengthOfLongestSubstring2(str));
        System.out.println(lengthOfLongestSubstring5(str));
    }
}
