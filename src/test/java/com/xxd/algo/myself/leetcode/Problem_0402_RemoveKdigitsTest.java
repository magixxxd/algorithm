package com.xxd.algo.myself.leetcode;

import java.util.LinkedList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-06 17:06
 * @description:
 */
public class Problem_0402_RemoveKdigitsTest {

    public static String removeKdigits(String num, int k) {
        int N = num.length();
        if (N <= k) {
            return "0";
        }

        char[] str = num.toCharArray();
        LinkedList<Character> queue = new LinkedList<>();
        for (int i = 0; i < str.length; i++) {
            while (k > 0 && !queue.isEmpty() && queue.peekLast() > str[i]) {
                queue.pollLast();
                k--;
            }

            queue.addLast(str[i]);
        }

        while (k > 0) {
            queue.pollLast();
            k--;
        }

        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()) {
            Character cha = queue.pollFirst();
            if (cha == '0' && sb.length() == 0) {
                continue;
            }
            sb.append(cha);
        }

        return sb.length() == 0 ? "0" : sb.toString();
    }

    public static void main(String[] args) {
        String num = "10001";
        int k = 4;
        System.out.println(removeKdigits(num, k));
    }
}
