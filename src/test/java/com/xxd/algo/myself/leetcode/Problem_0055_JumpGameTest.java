package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-18 09:50
 * @description:
 */
public class Problem_0055_JumpGameTest {

    public boolean canJump(int[] nums) {
        if (nums == null || nums.length < 2) {
            return true;
        }

        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            if (i > max) {
                return false;
            }
            max = Math.max(max, i + nums[i]);
        }

        return true;
    }

    public boolean canJump2(int[] nums) {
        if (nums == null || nums.length < 2) {
            return true;
        }

        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            if (i > max) {
                return false;
            }
            max = Math.max(max, i + nums[i]);
        }

        return true;
    }

}
