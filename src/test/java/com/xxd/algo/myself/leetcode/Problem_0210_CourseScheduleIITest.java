package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0210_CourseScheduleII;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-07 20:23
 * @description:
 */
public class Problem_0210_CourseScheduleIITest {

    public static class Node {
        public int name;
        public int in;
        public ArrayList<Node> nexts;

        public Node(int name) {
            this.name = name;
            in = 0;
            nexts = new ArrayList<>();
        }
    }

    public static int[] findOrder(int numCourses, int[][] prerequisites) {
        int[] ans = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            ans[i] = i;
        }
        if (prerequisites == null || prerequisites.length == 0) {// 没有先修的要求
            return ans;
        }

        HashMap<Integer, Node> map = new HashMap<>();
        for (int[] arr : prerequisites) {
            int to = arr[0];
            int from = arr[1];

            if (!map.containsKey(to)) {
                map.put(to, new Node(to));
            }
            if (!map.containsKey(from)) {
                map.put(from, new Node(from));
            }

            Node t = map.get(to);
            Node f = map.get(from);

            f.nexts.add(t);
            t.in++;
        }

        LinkedList<Node> zeroInQueue = new LinkedList<>();
        int index = 0;
        for (int i = 0; i < numCourses; i++) {
            if (!map.containsKey(i)) {
                ans[index++] = i;
            } else {
                if (map.get(i).in == 0) {
                    zeroInQueue.add(map.get(i));
                }
            }
        }
        int needPrerequisiteNums = map.size();
        int count = 0;
        while (!zeroInQueue.isEmpty()) {
            Node cur = zeroInQueue.poll();
            ans[index++] = cur.name;
            count++;
            for (Node next : cur.nexts) {
                if (--next.in == 0) {
                    zeroInQueue.add(next);
                }
            }
        }

        return count == needPrerequisiteNums ? ans : new int[0];
    }

    public static void main(String[] args) {
        int numCourses = 4;
        int[][] prerequisites = {
                {5, 2},
                {6, 2},
                {7, 5},
                {7, 6}
        };
        System.out.println(Arrays.toString(findOrder(numCourses, prerequisites)));
    }
}
