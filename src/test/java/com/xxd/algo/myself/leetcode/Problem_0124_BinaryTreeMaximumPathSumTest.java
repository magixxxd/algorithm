package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0124_BinaryTreeMaximumPathSum;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-26 09:49
 * @description:
 */
public class Problem_0124_BinaryTreeMaximumPathSumTest {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public static int maxPathSum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return process(root).maxPathSum;
    }

    private static Info process(TreeNode x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);

        int p1 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p1 = leftInfo.maxPathSum;
        }

        int p2 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p2 = rightInfo.maxPathSum;
        }

        int p3 = x.val;
        int p4 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p4 = x.val + leftInfo.maxPathSumFromHead;
        }

        int p5 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p5 = x.val + rightInfo.maxPathSumFromHead;
        }

        int p6 = Integer.MIN_VALUE;
        if (leftInfo != null && rightInfo != null) {
            p6 = x.val + leftInfo.maxPathSumFromHead + rightInfo.maxPathSumFromHead;
        }


        int maxPathSum = Math.max(Math.max(Math.max(p1, p2), Math.max(p3, p4)), Math.max(p5, p6));
        int maxPathSumFromHead = Math.max(p3, Math.max(p4, p5));

        return new Info(maxPathSum, maxPathSumFromHead);
    }


    public static class Info {
        // 不需要以头结点出发的最大路径和
        int maxPathSum;

        // 必须以头结点出发的最大路劲和
        int maxPathSumFromHead;

        public Info(int maxPathSum, int maxPathSumFromHead) {
            this.maxPathSum = maxPathSum;
            this.maxPathSumFromHead = maxPathSumFromHead;
        }
    }
}
