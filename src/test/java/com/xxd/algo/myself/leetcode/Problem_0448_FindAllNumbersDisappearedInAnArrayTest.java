package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-25 11:45
 * @description:
 */
public class Problem_0448_FindAllNumbersDisappearedInAnArrayTest {


    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> ans = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return ans;
        }

        int N = nums.length;
        for (int i = 0; i < N; i++) {
            walk(nums, i);
        }

        for (int i = 0; i < N; i++) {
            if (nums[i] != i + 1) {
                ans.add(i + 1);
            }
        }

        return ans;
    }

    private static void walk(int[] nums, int i) {
        while (nums[i] != i + 1) {
            int next = nums[i] - 1;
            if (nums[next] == next + 1) {
                break;
            }
            swap(nums, i, next);
        }
    }

    public static void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}
