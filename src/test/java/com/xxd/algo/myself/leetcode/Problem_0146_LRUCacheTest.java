package com.xxd.algo.myself.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-01 21:07
 * @description:
 */
public class Problem_0146_LRUCacheTest {

    private MyCache<Integer, Integer> cache;

    public Problem_0146_LRUCacheTest(int capacity) {
        cache = new MyCache<>(capacity);
    }

    public int get(int key) {
        Integer ans = cache.get(key);
        return ans == null ? -1 : ans;
    }

    public void put(int key, int value) {
        cache.set(key, value);
    }

    public static class MyCache<K, V> {
        Map<K, Node<K, V>> keyNodeMap;
        NodeDoubleLinkedList<K, V> nodeList;

        private int size = 0;

        public MyCache(int cap) {
            if (cap < 1) {
                throw new RuntimeException("should be more than 0.");
            }
            keyNodeMap = new HashMap<>(cap);
            nodeList = new NodeDoubleLinkedList<>();
        }


        public V get(K key) {
            if (keyNodeMap.containsKey(key)) {
                Node<K, V> res = keyNodeMap.get(key);
                nodeList.moveNodeToTail(res);
                return res.value;
            }

            return null;
        }


        public void set(K key, V value) {
            if (keyNodeMap.containsKey(key)) {
                Node<K, V> node = keyNodeMap.get(key);
                node.value = value;
                nodeList.moveNodeToTail(node);
            } else {
                if (keyNodeMap.size() == size) {
                    removeMostUnusedCache();
                }
                Node<K, V> newNode = new Node<K, V>(key, value);
                keyNodeMap.put(key, newNode);
                nodeList.addNode(newNode);
            }
        }

        private void removeMostUnusedCache() {
            Node<K, V> removeNode = nodeList.removeHead();
            keyNodeMap.remove(removeNode.key);
        }
    }

    public static class Node<K, V> {
        K key;
        V value;

        Node<K, V> last;
        Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    public static class NodeDoubleLinkedList<K, V> {
        Node<K, V> head;
        Node<K, V> tail;

        public NodeDoubleLinkedList() {
            head = null;
            tail = null;
        }

        /**
         * 加入到头结点
         * @param newNode
         */
        public void addNode(Node<K, V> newNode) {
            if (newNode == null) {
                return;
            }

            if (head == null) {
                head = newNode;
                tail = newNode;
            } else {
                tail.next = newNode;
                newNode.last = tail;
                tail = newNode;
            }
        }

        public void moveNodeToTail(Node<K, V> node) {
            if (tail == node) {
                return;
            }

            if (head == node) {
                head = head.next;
                head.last = null;
            } else {
                node.last.next = node.next;
                node.next.last = node.last;
            }

            node.last = tail;
            node.next = null;
            tail.next = node;
            tail = node;
        }

        public Node<K,V> removeHead() {
            if (head == null) {
                return null;
            }

            Node res = head;
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                head = res.next;
                head.last = null;
                res.next = null;
            }

            return res;
        }
    }
}