package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-27 07:42
 * @description:
 */
public class Problem_0007_ReverseIntegerTest {

    public static int reverse(int x) {
        int m = Integer.MAX_VALUE / 10;
        int o = Integer.MAX_VALUE % 10;

        int p = Integer.MIN_VALUE / 10;
        int q = Integer.MIN_VALUE % 10;

        int res = 0;
        while (x != 0) {
            if (res > m || (res == m && x % 10 > o)) {
                return 0;
            }

            if (res < p || (res == p && x % 10 < q)) {
                return 0;
            }

            res = res * 10 + x % 10;
            x /= 10;
        }
        return res ;
    }


    public static int reverse1(int x) {
        boolean neg = ((x >> 31) & 1) == 1; // 是否是奇数
        x = neg ? x : -x; // 最高符号位

        int m = Integer.MIN_VALUE / 10;
        int o = Integer.MIN_VALUE % 10;

        int res = 0;

        while (x != 0) {
            res = res * 10 + x % 10;
            x /= 10;
        }

        return neg ? res : Math.abs(res);
    }

    public static void main(String[] args) {
        // int x = 123;
        System.out.println(reverse(-2147483648));
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        //reverse1(-234);
    }
}
