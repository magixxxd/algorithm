package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0236_LowestCommonAncestorOfBinaryTree;

import java.awt.image.RGBImageFilter;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 17:56
 * @description:
 */
public class Problem_0236_LowestCommonAncestorOfBinaryTreeTest {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
    }

    public static class Info {
        public TreeNode ans;
        public boolean hasO1;
        public boolean hasO2;

        public Info(TreeNode ans, boolean hasO1, boolean hasO2) {
            this.ans = ans;
            this.hasO1 = hasO1;
            this.hasO2 = hasO2;
        }
    }

    public static TreeNode lowestCommonAncestor(TreeNode head, TreeNode o1, TreeNode o2) {
        return process(head, o1, o2).ans;
    }

    private static Info process(TreeNode head, TreeNode o1, TreeNode o2) {
        if (head == null) {
            return new Info(null, false, false);
        }

        Info leftInfo = process(head.left, o1, o2);
        Info rightInfo = process(head.right, o1, o2);

        boolean hasO1 = head == o1 || leftInfo.hasO1 || rightInfo.hasO1;
        boolean hasO2 = head == o2 || leftInfo.hasO2 || rightInfo.hasO2;

        TreeNode ans = null;

        if (leftInfo.ans != null) {
            ans = leftInfo.ans;
        }

        if (rightInfo.ans != null) {
            ans = rightInfo.ans;
        }

        if (ans == null) {
            if (hasO1 && hasO2) {
                ans = head;
            }
        }

        return new Info(ans, hasO1, hasO2);
    }

}
