package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-26 11:32
 * @description:
 */
public class Problem_0543_DiameterOfBinaryTreeTest {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
    }

    public static class Info {
        int height;
        int maxDistance;

        public Info(int height, int maxDistance) {
            this.height = height;
            this.maxDistance = maxDistance;
        }
    }

    public static int diameterOfBinaryTree(TreeNode root) {
        return process(root).maxDistance;
    }

    private static Info process(TreeNode x) {
        if (x == null) {
            return new Info(0, 0);
        }
        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);

        int height = Math.max(leftInfo.height, rightInfo.height) + 1;
        int maxDistance = Math.max(
                Math.max(leftInfo.maxDistance, rightInfo.maxDistance),
                leftInfo.height + rightInfo.height
        );

        return new Info(height, maxDistance);
    }

}
