package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 18:07
 * @description:
 */
public class Problem_0237_DeleteNodeInLinkedListTest {

    public static class ListNode {
        int val;
        ListNode next;
    }

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
