package com.xxd.algo.myself.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-04-21 22:29
 * @description:
 */
public class Problem_0001_TwoSumTest {
    /**
     * 2021-04-21 22:29
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        if (nums == null || nums.length < 2) {
            return null;
        }

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int cur = nums[i];
            int complement = target - cur;
            if (map.containsKey(complement)) {
                return new int[]{i, map.get(complement)};
            }
            map.put(cur, i);
        }

        return null;
    }
}
