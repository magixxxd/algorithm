package com.xxd.algo.myself.leetcode;

import javax.swing.table.TableRowSorter;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-01 10:29
 * @description:
 */
public class Problem_0209_MinSubArrayLenTest {

    public static int minSubArrayLen(int target, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        int L = 0; // 窗口不固定，声明两个变量
        int R = 0;
        int sum = 0;
        int ans = Integer.MAX_VALUE;
        while (R < nums.length) {
            sum += nums[R];
            // 如果满足了
            // L往右扩，扩到不能满足了为止
            while (sum >= target) {
                ans = Math.min(ans, R - L + 1);
                sum -= nums[L];
                L++;
            }
            R++;
        }
        return ans == Integer.MAX_VALUE ? 0 : ans;
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, 1, 2, 4, 3};
        int target = 7;
        System.out.println(minSubArrayLen(target, arr));
    }
}
