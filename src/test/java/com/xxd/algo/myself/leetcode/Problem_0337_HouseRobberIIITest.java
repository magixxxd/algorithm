package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.toplikedquestions.Problem_0337_HouseRobberIII;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-24 09:29
 * @description:
 */
public class Problem_0337_HouseRobberIIITest {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
    }

    public static class Info {
        int no;
        int yes;

        public Info(int no, int yes) {
            this.no = no;
            this.yes = yes;
        }
    }

    public static int rob(TreeNode root) {
        Info info = process(root);
        return Math.max(info.no, info.yes);
    }

    private static Info process(TreeNode x) {
        if (x == null) {
            return new Info(0, 0);
        }


        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);
        int yes = leftInfo.no + rightInfo.no + x.val;
        int no = Math.max(leftInfo.no, leftInfo.yes) + Math.max(rightInfo.yes, rightInfo.no);

        return new Info(no, yes);
    }
}
