package com.xxd.algo.myself.leetcode;

import java.awt.geom.RectangularShape;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-03 17:49
 * @description:
 */
public class Problem_0189_RotateArrayTest {

    public static void rotate(int[] nums, int k) {
        int N = nums.length;
        k = k % N;
        reverse(nums, 0, N - k - 1);
        reverse(nums, N - k, N - 1);
        reverse(nums, 0, N - 1);
    }

    public static void reverse(int[] nums, int L , int R) {
        while (L < R) {
            int tmp = nums[L];
            nums[L++] = nums[R];
            nums[R--] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        rotate(nums, 3);
        for (int num : nums) {
            System.out.print(num + "\t");
        }
    }
}
