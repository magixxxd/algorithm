package com.xxd.algo.myself.leetcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-08 09:52
 * @description:
 */
public class Problem_0456_FInd132patternTest {

    public static  boolean find132pattern(int[] nums) {
        int n = nums.length;
        if (n < 3) {
            return false;
        }

        // 左侧最小值
        int leftMin = nums[0];

        // 右侧所有元素
        TreeMap<Integer, Integer> rightAll = new TreeMap<Integer, Integer>();

        for (int k = 2; k < n; ++k) {
            rightAll.put(nums[k], rightAll.getOrDefault(nums[k], 0) + 1);
        }

        // 枚举3
        for (int j = 1; j < n - 1; j++) {
            if (leftMin < nums[j]) {
                Integer next = rightAll.ceilingKey(leftMin + 1);// 大于等于 leftMin + 1
                if (next != null && next < nums[j]) {
                    return true;
                }
            }
            leftMin = Math.min(leftMin, nums[j]); // 更新最小值
            rightAll.put(nums[j + 1], rightAll.get(nums[j + 1]) - 1);  // j 右边这个数，在也不可能作为右边的值了
            if (rightAll.get(nums[j + 1]) == 0) {
                rightAll.remove(nums[j + 1]);
            }
        }

        return false;
    }


    public static boolean find132pattern2(int[] nums) {
        if (nums.length < 3) {
            return false;
        }

        int leftMin = nums[0];
        int N = nums.length;
        TreeMap<Integer, Integer> allRight = new TreeMap<>();
        for (int i = 2; i < N; i++) { // 只有2 开始才有可能作为第三个坐标
            allRight.put(nums[i], allRight.getOrDefault(nums[i], 0) + 1);
        }

        // 开始枚举中间的那个2
        for (int i = 1; i < N - 1; i++) {
            if (leftMin < nums[i]) {
                // 看下右边有没有大于leftMin的
                Integer right = allRight.ceilingKey(leftMin + 1);
                if (right != null && right < nums[i]) {
                    return true;
                }
            }
            // 更新 leftMin
            leftMin = Math.min(leftMin, nums[i]);
            // 更新 有序表
            allRight.put(nums[i + 1], allRight.get(nums[i + 1]) - 1);

            if (allRight.get(nums[i + 1]) == 0) {
                allRight.remove(nums[i + 1]);
            }
        }

        return false;
    }


    public boolean find132pattern3(int[] nums) {
        int n = nums.length;
        Deque<Integer> candidateK = new LinkedList<Integer>(); // 2 的候选
        candidateK.push(nums[n - 1]);
        int maxK = Integer.MIN_VALUE; // 所有被移除元素的最大值

        for (int i = n - 2; i >= 0; i--) { // 枚举1
            if (nums[i] < maxK) { // 当前值可以作为1
                return true;
            }

            // 当前值是否可用做为3
            while (!candidateK.isEmpty() && nums[i] > candidateK.peek()) {// 从左到右 递减
                maxK = candidateK.pop();
            }

            if (nums[i] > maxK) {
                candidateK.push(nums[i]);
            }
        }

        return false;
    }

    public static boolean find132pattern4(int[] nums) {
        int n = nums.length;
        LinkedList<Integer> candidateK = new LinkedList<Integer>(); // 2 的候选
        candidateK.addLast(nums[n - 1]);

        int maxK = Integer.MIN_VALUE;
        for (int i = n - 2; i >= 0; i--) {
            if (nums[i] < maxK) { // 是不是1 的候选
                return true;
            }
            // 是否能作为3的候选
            while (!candidateK.isEmpty() && nums[i] > candidateK.peekLast()) {
                maxK = candidateK.pollLast();
            }

            candidateK.addLast(nums[i]);
        }

        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        System.out.println(find132pattern(arr));
        System.out.println(find132pattern2(arr));
        System.out.println(find132pattern4(arr));
    }
}
