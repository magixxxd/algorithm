package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-04 09:42
 * @description:
 */
public class Problem_0191_NumberOf1BitsTest {

    // n & (~n + 1)
    public static int hammingWeight1(int n) {
        int bits = 0;
        int rightOne = 0;
        while (n != 0) {
            rightOne = n & (-n);
            n = n ^ rightOne;
            bits++;
        }


        return bits;
    }
}