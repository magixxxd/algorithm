package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0138_CopyListWithRandomPointer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-01 09:43
 * @description:
 */
public class Problem_0138_CopyListWithRandomPointerTest {

    public static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }


    public static Node copyRandomList1(Node head) {
        if (head == null) {
            return null;
        }

        Map<Node, Node> map = new HashMap<>();
        Node cur = head;
        while (cur != null) {
            map.put(cur, new Node(cur.val));
            cur = cur.next;
        }

        cur = head;
        Node next = null;
        Node random = null;

        while (cur != null) {
            Node curCopy = map.get(cur);

            next = map.get(cur.next);
            random = map.get(cur.random);

            curCopy.next = next;
            curCopy.random = random;

            cur = cur.next;
        }

        return map.get(head);
    }


    public static Node copyRandomList2(Node head) {
        if (head == null) {
            return null;
        }

        Node cur = head;
        Node next = null;
        while (cur != null) {
            next = cur.next;

            cur.next = new Node(cur.val);
            cur.next.next = next;

            cur = next;
        }

        cur = head;
        Node copy = null;
        while (cur != null) {
            next = copy.next.next;

            copy = cur.next;
            copy.random = cur.random != null ? cur.random.next : null;

            cur = next;
        }

        Node res = head.next;
        cur = head;
        while (cur != null) {
            next = cur.next.next;
            copy = cur.next;

            cur.next = next;

            copy.next = next != null ? next.next : null;
            cur = next;
        }

        return res;
    }
}
