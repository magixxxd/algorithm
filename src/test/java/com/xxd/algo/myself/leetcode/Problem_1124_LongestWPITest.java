package com.xxd.algo.myself.leetcode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-10 09:36
 * @description:
 */
public class Problem_1124_LongestWPITest {

    public static int longestWPI(int[] hours) {
        int n = hours.length;
        int[] score = new int[n];
        for (int i = 0; i < n; i++) score[i] = hours[i] > 8 ? 1 : -1;

        int[] presum = new int[n + 1];
        presum[0] = 0;
        for (int i = 1; i < n + 1; i++) presum[i] = presum[i - 1] + score[i - 1];

        int result = 0;
        Deque<Integer> stack = new LinkedList<>();
        stack.addLast(0);
        for (int i = 1; i < n + 1; i++) {
            if (stack.isEmpty() || presum[stack.peekLast()] > presum[i]) {
                stack.addLast(i); // 栈中索引指向的元素严格单调递减
            }
        }
        for (int i = n; i >= 0; i--) { // 从后往前遍历 presum 数组
            while (!stack.isEmpty() && presum[i] > presum[stack.peekLast()]) {//说明栈顶索引到i位置的和是大于0的，是表现良好的时间段
                result = Math.max(result, i - stack.pollLast());//与栈顶索引指向元素比较，如果相减结果大于 0，则一直出栈，直到不大于 0 为止，然后更新当前最大宽度
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] arr = {9, 9, 6, 0, 0, 0, 0, 0, 9, 9, 9, 6, 6, 4, 9};
        System.out.println(longestWPI(arr));
    }
}
