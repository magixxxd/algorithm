package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-26 11:21
 * @description:
 */
public class Problem_0739_DailyTemperaturesTest {

    public static int[] dailyTemperatures(int[] arr) {
        if (arr == null || arr.length == 0) {
            return new int[0];
        }

        int N = arr.length;
        int[] ans = new int[N];
        // 从底 到 上 大 到 小
        Stack<List<Integer>> stack = new Stack<List<Integer>>();

        for (int i = 0; i < N; i++) {
            while (!stack.isEmpty() && arr[stack.peek().get(0)] < arr[i]) {
                List<Integer> popIs = stack.pop();
                for (Integer popI : popIs) {
                    ans[popI] = i - popI;
                }
            }

            // 如果加进来是一样的
            if (!stack.isEmpty() && arr[stack.peek().get(0)] == arr[i]) {
                stack.peek().add(i);
            } else{
                ArrayList<Integer> list = new ArrayList<Integer>();
                list.add(i);
                stack.push(list);
            }
        }

        return ans;
    }


    public static int[] dailyTemperatures2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return new int[0];
        }

        int N = arr.length;
        int[] ans = new int[N];
        // 从底 到 上 大 到 小
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < N; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] < arr[i]) {
                Integer index = stack.pop();
                ans[index] = i - index;
            }

            stack.push(i);
        }

        return ans;
    }
}
