package com.xxd.algo.myself.leetcode;


import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-07 08:08
 * @description:
 */
public class Problem_0019_RemoveNthNodeFromEndofListTest {
    public static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }


    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode cur = head;
        ListNode pre = head;
        while (n > 0 && cur != null) {
            cur = cur.next;
            n--;
        }
        // 比如 链表是  1 -> 2 -> 3 -> 4 -> 5 然后n = 5
        if (n == 0 && cur == null) { // 如果说要删除的节点 是头节点
            return head.next;
        }

        if (n > 0) { // 要删除的节点太大了，比链表的长度还长哦
            return head;
        }

        // n = 2 cur 往前走了两步，此时就是3
        while (cur != null && cur.next != null) {
            pre = pre.next;
            cur = cur.next;
        }

        pre.next = pre.next.next;
        return head;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        ListNode head2 = removeNthFromEnd(head, 2);

        System.out.println(22);


    }

}
