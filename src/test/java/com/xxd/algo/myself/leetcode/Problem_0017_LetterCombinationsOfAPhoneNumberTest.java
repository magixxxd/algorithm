package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-07 07:56
 * @description:
 */
public class Problem_0017_LetterCombinationsOfAPhoneNumberTest {

    public static char[][] phone = {
            {'a', 'b', 'c'}, // 2    0
            {'d', 'e', 'f'}, // 3    1
            {'g', 'h', 'i'}, // 4    2
            {'j', 'k', 'l'}, // 5    3
            {'m', 'n', 'o'}, // 6
            {'p', 'q', 'r', 's'}, // 7
            {'t', 'u', 'v'},   // 8
            {'w', 'x', 'y', 'z'}, // 9
    };

    public List<String> letterCombinations(String digits) {
        List<String> ans = new ArrayList<>();
        if (digits == null || digits.length() == 0) {
            return ans;
        }

        char[] str = digits.toCharArray();
        char[] path = new char[str.length];
        process(str, 0, path, ans);

        return ans;
    }

    public static void process(char[] str, int index, char[] path, List<String> ans) {
        if (index == str.length) {
            ans.add(String.valueOf(path));
        } else {
            char[] cands = phone[str[index] - '2'];
            for (char cur : cands) {
                path[index] = cur;
                process(str, index + 1, path, ans);
            }
        }
    }
}
