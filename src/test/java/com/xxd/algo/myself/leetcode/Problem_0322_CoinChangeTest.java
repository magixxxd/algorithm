package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-16 11:18
 * @description:
 */
public class Problem_0322_CoinChangeTest {

    public static int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0 || amount < 0) {
            return -1;
        }

        int N = coins.length;
        int[][] dp = new int[N][amount + 1];
        for (int i = 1; i < amount + 1; i++) {
            if (i % coins[0] != 0) {
                dp[0][i] = -1;
            } else {
                dp[0][i] = i / coins[0];
            }
        }

        for (int i = 1; i < N; i++) {
            for (int j = 1; j <= amount; j++) {
                dp[i][j] = Integer.MAX_VALUE;
                if (dp[i - 1][j] != -1) {
                    dp[i][j] = dp[i - 1][j];
                }
                if (j - coins[i] >= 0 && dp[i][j - coins[i]] != -1) {
                    dp[i][j] = Math.min(dp[i][j], dp[i][j - coins[i]] + 1);
                }
                if (dp[i][j] == Integer.MAX_VALUE) {
                    dp[i][j] = -1;
                }
            }
        }

        return dp[N - 1][amount];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 5};
        coinChange(arr, 11);
    }
}
