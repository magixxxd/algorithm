package com.xxd.algo.myself.leetcode;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-28 11:20
 * @description:
 */
public class Problem_1856_MaxSumMinProductTest {

    public int maxSumMinProduct(int[] arr) {
        int size = arr.length;
        long[] sums = new long[size];
        sums[0] = arr[0];
        for (int i = 1; i < size; i++) {
            sums[i] += sums[i - 1] + arr[i];
        }
        long max = Integer.MIN_VALUE;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < size; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] >= arr[i]) {
                int j = stack.pop();
                long sum = stack.isEmpty() ? sums[i - 1] : sums[i - 1] - sums[stack.peek()];
                max = Math.max(max, sum * arr[j]);
            }
            stack.push(i);
        }

        while (!stack.isEmpty()) {
            int j = stack.pop();
            long sum = stack.isEmpty() ? sums[size - 1] : sums[size - 1] - sums[stack.peek()];
            max = Math.max(max, sum * arr[j]);
        }
        return (int) (max % (int) (1e9 + 7));
    }
}

