package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-11 09:04
 * @description:
 */
public class Problem_0026_RemoveDuplicatesFromSortedArrayTest {

    public static int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }

        if (nums.length < 2) {
            return nums.length;
        }

        int done = 0;

        for (int index = 1; index < nums.length; index++) {
            if (nums[index] != nums[done]) {
                nums[++done] = nums[index];
            }
        }

        return done + 1;
    }
}