package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-30 10:28
 * @description:
 */
public class Problem_1052_MaxSatisfiedTest {

    public static int maxSatisfied(int[] customers, int[] grumpy, int minutes) {
        int total = 0;
        for (int i = 0; i < customers.length; i++) {
            if (grumpy[i] == 0) {
                total += customers[i];
            }
        }

        int increment = 0;
        int max = 0;
        for (int i = 0; i < minutes - 1; i++) {
            if (grumpy[i] == 1) {
                increment += customers[i];
            }
        }

        for (int L = 0, R = minutes - 1; R < customers.length; L++, R++) {
            if (grumpy[R] == 1) {
                increment += customers[R];
            }

            max = Math.max(max, increment);

            if (grumpy[L] == 1) {
                increment -= customers[L];
            }
        }

        return total + max;
    }

    public static void main(String[] args) {
        int[] customer = {1, 0, 1, 2, 1, 1, 7, 5};
        int[] grumpy = {0, 1, 0, 1, 0, 1, 0, 1};
        int minutes = 3;
        System.out.println(maxSatisfied(customer, grumpy, minutes));
    }
}
