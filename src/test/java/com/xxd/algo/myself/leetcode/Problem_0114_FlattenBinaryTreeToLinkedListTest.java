package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.toplikedquestions.Problem_0114_FlattenBinaryTreeToLinkedList;
import javafx.scene.transform.Rotate;

import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-23 10:24
 * @description:
 */
public class Problem_0114_FlattenBinaryTreeToLinkedListTest {


    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int value) {
            val = value;
        }
    }

    public static void flatten(TreeNode root) {
        if (root == null) {
            return;
        }
        LinkedList<TreeNode> linkedList = new LinkedList<>();
        process(root, linkedList);
        root = linkedList.pollFirst();
        TreeNode cur = null;
        while (!linkedList.isEmpty()) {
            cur = linkedList.pollFirst();
            root.left = null;
            root.right = cur;
            root = cur;
        }
    }

    private static void process(TreeNode node,
                                LinkedList<TreeNode> linkedList) {
        if (node == null) {
            return;
        }

        linkedList.addLast(node);
        process(node.left, linkedList);
        process(node.right, linkedList);
    }

}