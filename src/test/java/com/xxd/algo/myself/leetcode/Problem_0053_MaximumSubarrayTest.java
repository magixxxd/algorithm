package com.xxd.algo.myself.leetcode;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-20 09:32
 * @description:
 */
public class Problem_0053_MaximumSubarrayTest {
    /**
     * 必须以i结尾，
     *
     * @param nums
     * @return
     */
    public int maxSubArray(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int max = dp[0];
        for (int i = 1; i < nums.length; i++) {
            int p1 = nums[i];
            int p2 = dp[i - 1] + p1; // 之前的最大值，加上之后的值
            dp[i] = Math.max(p1, p2);
            max = Math.max(dp[i], max);
        }

        return max;
    }


    /**
     * 必须以i结尾，
     *
     * @param nums
     * @return
     */
    public static int maxSubArray2(int[] nums) {
        int cur = nums[0]; // dp[i - 1]
        int max = cur;
        for (int i = 1; i < nums.length; i++) {
            int p1 = nums[i];
            cur += p1; // dp[i - 1] + cur 跟当前有关
            cur = Math.max(cur, p1);
            max = Math.max(max, cur);
        }

        return max;
    }

    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        System.out.println(maxSubArray2(nums));
    }
}
