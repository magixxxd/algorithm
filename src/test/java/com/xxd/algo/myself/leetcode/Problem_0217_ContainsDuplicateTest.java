package com.xxd.algo.myself.leetcode;

import java.util.HashSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-09 09:25
 * @description:
 */
public class Problem_0217_ContainsDuplicateTest {


    public boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length < 2) {
            return false;
        }
        HashSet<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                return true;
            }
            set.add(num);
        }
        return false;
    }

}
