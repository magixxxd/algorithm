package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-17 10:49
 * @description:
 */
public class Problem_0329_LongestIncreasingPathInAMatrixTest {
    public static int longest(int[][] matrix) {
        int longest = 0;
        int[][] dp = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                longest = Math.max(longest, process(matrix, i , j, dp));
            }
        }


        return longest;
    }

    private static int process(int[][] matrix, int i, int j, int[][] dp) {
        if (dp[i][j] != 0) {
            return dp[i][j];
        }
        // 不越界
        int next = 0; // 往左右上下四个方向，能走出最长的后续是多少？
        // i, j i-1,j
        if (i > 0 && matrix[i - 1][j] > matrix[i][j]) {
            next = process(matrix, i - 1, j, dp);
        }
        // i + 1
        if (i + 1 < matrix.length && matrix[i + 1][j] > matrix[i][j]) {
            next = Math.max(next, process(matrix, i + 1, j, dp));
        }
        if (j > 0 && matrix[i][j - 1] > matrix[i][j]) {
            next = Math.max(next, process(matrix, i, j - 1, dp));
        }
        if (j + 1 < matrix[0].length && matrix[i][j + 1] > matrix[i][j]) {
            next = Math.max(next, process(matrix, i, j + 1, dp));
        }
        dp[i][j] = 1 + next;

        return 1 + next;
    }

}
