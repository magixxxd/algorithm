package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-22 10:31
 * @description:
 */
public class Problem_0039_CombinationSumTest {

    public static List<List<Integer>> combinationSum1(int[] candidates, int target) {
        return process(candidates, candidates.length - 1, target);
    }

    private static List<List<Integer>> process(int[] arr, int index, int target) {
        List<List<Integer>> ans = new ArrayList<>();
        if (target == 0) { // index == -1
            ans.add(new ArrayList<>());
            return ans;
        }
        if (index == - 1) {
            return ans;
        }

        for (int zhang = 0; zhang * arr[index] <= target; zhang++) {
            List<List<Integer>> preLists = process(arr, index - 1, target - (zhang * arr[index]));
            for (List<Integer> pre : preLists) {
                for (int i = 0; i < zhang; i++) {
                    pre.add(arr[index]);
                }
                ans.add(pre);
            }
        }


        return ans;
    }

}
