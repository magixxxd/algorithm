package com.xxd.algo.myself.leetcode;

import java.util.HashSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-04 10:45
 * @description:
 */
public class Problem_0202_HappyNumberTest {

    public static boolean isHappy(int n) {
        HashSet<Integer> set = new HashSet<>();

        while (n != 1) {
            int sum = 0;
            while (n != 0) {
                int r = n % 10;
                sum += r * r;
                n /= 10;
            }
            n = sum;
            if (set.contains(sum)) {
                break;
            }
            set.add(n);
        }

        return n == 1;
    }

}
