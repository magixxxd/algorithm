package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0116_PopulatingNextRightPointersInEachNode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-25 09:28
 * @description:
 */
public class Problem_0116_PopulatingNextRightPointersInEachNodeTest {
    public static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;
    }

    public static class MyQueue {
        public Node head;
        public Node tail;
        int size;

        public MyQueue() {
            head = null;
            tail = null;
            size = 0;
        }

        public void offer(Node node) {
            size++;
            if (head == null) {
                head = node;
                tail = node;
            } else {
                tail.next = node;
                tail = node;
            }
        }

        public Node poll() {
            if (size <= 0) {
                return null;
            }
            size--;
            Node ans = head;
            head = head.next;
            ans.next = null;
            return ans;
        }

        public boolean isEmpty() {
            return size == 0;
        }
    }

    public static Node connect(Node root) {
        if (root == null) {
            return root;
        }
        
        MyQueue queue = new MyQueue();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size;
            Node pre = null;
            for (int i = 0; i < size; i++) {
                Node cur = queue.poll();
                if (cur.left != null) {
                    queue.offer(cur.left);
                }

                if (cur.right != null) {
                    queue.offer(cur.right);
                }
                if (pre != null) {
                    pre.next = cur;
                }
                pre = cur;
            }
        }
        return root;
    }
}
