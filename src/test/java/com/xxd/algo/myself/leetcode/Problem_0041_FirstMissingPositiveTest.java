package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-17 09:26
 * @description:
 */
public class Problem_0041_FirstMissingPositiveTest {

    public static int firstMissingPositive(int[] arr) {
        int l = 0;
        int r = arr.length;

        while (l < r) {
            if (arr[l] == l + 1) {
                l++;
            } else if (arr[l] <= l || arr[l] > r || arr[l] == arr[arr[l] - 1]) { // 丢到垃圾区
                swap(arr, l, --r);
            } else { // 交易到合法位置
                swap(arr, l, arr[l] - 1);
            }
        }
        return l + 1;
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, -1, 1, 6, 5};
        firstMissingPositive(arr);
    }
}
