package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-22 15:26
 * @description:
 */
public class Problem_0069_SqrtXTest {

    // x一定非负，输入可以保证
    public static int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        if (x == 1) {
            return 1;
        }

        long L = 1;
        long R = x;
        long ans = 1;
        long M = 0;
        while (L <= R) {
            M = (L + R) / 2;
            if (M * M <= x) {
                L = M + 1;
                ans = M;
            } else if (M * M > x) {
                R = M - 1;
            }
        }

        return (int) ans;
    }
}
