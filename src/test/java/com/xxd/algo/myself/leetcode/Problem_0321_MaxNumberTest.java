package com.xxd.algo.myself.leetcode;

import com.xxd.algo.msb.interview.trainingcamp003.class22.Main;

import java.util.Arrays;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-07 11:44
 * @description:
 */
public class Problem_0321_MaxNumberTest {

    public int[] maxNumber(int[] nums1, int[] nums2, int k) {
        int n1 = nums1.length;
        int n2 = nums2.length;

        int start = Math.max(0, k - n2);
        int end = Math.min(k, n1);
        int[] maxSubsequence = new int[k];
        // 开始枚举 n1 留多少个
        for (int i = start; i <= end; i++) {
            int[] subSequence1 = maxSequence(nums1, i);
            int[] subSequence2 = maxSequence(nums2, k - i);
            int[] curMaxSequence = merge(subSequence1, subSequence2);
            if (compare(curMaxSequence, 0, maxSubsequence, 0) > 0) {
                System.arraycopy(curMaxSequence, 0, maxSubsequence, 0, k);
            }
        }

        return maxSubsequence;
    }

    private static int compare(int[] subsequence1, int index1, int[] subsequence2, int index2) {
        int n1 = subsequence1.length;
        int n2 = subsequence2.length;
        while (index1 < n1 && index2 < n2) {
            int difference = subsequence1[index1] - subsequence2[index2];
            if (difference != 0) {
                return difference;
            }
            index1++;
            index2++;
        }

        return (n1 - index1) - (n2 - index2);
    }

    private static int[] merge(int[] nums1, int[] nums2) {
        int n1 = nums1.length;
        int n2 = nums2.length;
        int[] res = new int[n1 + n2];
        int index1 = 0;
        int index2 = 0;
        int index = 0;
        while (index1 < n1 && index2 < n2) {
            if (compare(nums1, index1, nums2, index2) > 0) {
                res[index++] = nums1[index1++];
            } else {
                res[index++] = nums2[index2++];
            }
        }

        while (index1 < n1) {
            res[index++] = nums1[index1++];
        }

        while (index2 < n2) {
            res[index++] = nums2[index2++];
        }

        return res;
    }

    private static int[] maxSequence(int[] nums, int k) {
        int length = nums.length;
        int[] res = new int[k];
        int remain = length - k;
        int top = -1;
        for (int i = 0; i < length; i++) {
            while (top != -1 && remain > 0 && res[top] < nums[i]) {
                top--;
                remain--;
            }

            if (top < k - 1) {
                res[++top] = nums[i];
            } else {
                remain--;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] nums = {4, 3, 2, 1, 4};
        System.out.println(Arrays.toString(maxSequence(nums, 3)));

        int[] nums1 = {5, 6, 4};
        int[] nums2 = {5, 4, 8, 3};
        System.out.println(Arrays.toString(merge(nums1, nums2)));
    }

}
