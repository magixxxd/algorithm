package com.xxd.algo.myself.leetcode;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-22 09:50
 * @description:
 */
public class Problem_0334_IncreasingTripletSubsequenceTest {

    public static boolean increasingTriplet(int[] arr) {
        if (arr == null || arr.length < 3) {
            return false;
        }

        int small = Integer.MAX_VALUE;
        int mid = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < small) {
                small = arr[i];
            } else if (arr[i] < mid) {
                mid = arr[i];
            } else if(arr[i] > mid){
                return true;
            }
        }
        return false;
    }
}
