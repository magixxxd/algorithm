package com.xxd.algo.myself.leetcode;

import java.util.ArrayList;
import java.util.List;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-20 10:09
 * @description:
 */
public class Problem_0054_SpiralMatrixTest {

    /**
     * @param matrix
     * @return
     */
    public static List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> ans = new ArrayList<>();
        if (matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0) {
            return ans;
        }

        int a = 0;
        int b = 0;
        int c = matrix.length - 1;
        int d = matrix[0].length - 1;
        while (a <= c && b <= d) {
            addEdge(matrix, a++, b++, c--, d--, ans);
        }

        return ans;
    }

    private static void addEdge(int[][] matrix, int a, int b, int c, int d, List<Integer> ans) {
        if (a == c) {
            for (int i = b; i <= d; i++) {
                ans.add(matrix[a][i]);
            }
        } else if (b == d) {
            for (int i = a; i <= c; i++) {
                ans.add(matrix[i][b]);
            }
        } else {
            int times = d - b;
            for (int i = 0; i < times; i++) {
                ans.add(matrix[a][b + i]);
            }

            times = c - a;
            for (int i = 0; i < times; i++) {
                ans.add(matrix[a + i][d]);
            }

            times = d - b;
            for (int i = 0; i < times; i++) {
                ans.add(matrix[c][d - i]);
            }

            times = c - a;
            for (int i = 0; i < times; i++) {
                ans.add(matrix[c - i][b]);
            }
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};
        System.out.println(spiralOrder(matrix));
    }
}
