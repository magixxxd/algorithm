package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-17 10:16
 * @description:
 */
public class Problem_0326_PowerOfThreeTest {

    public static boolean isPowerOfThree(int n) {
        return (n > 0 && 1162261467 % n == 0);
    }
}
