package com.xxd.algo.myself.leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-02 10:17
 * @description:
 */
public class Problem_0480_MedianSlidingWindowTest {

    public static double[] medianSlidingWindow2(int[] nums, int k) {
        double[] ans = new double[nums.length - k + 1];
        DualHeap2 dualHeap = new DualHeap2(k);
        for (int i = 0; i < k; i++) {
            dualHeap.insert(nums[i]);
        }
        ans[0] = dualHeap.getMedian();
        for (int i = k; i < nums.length; i++) {
            dualHeap.insert(nums[i]);
            dualHeap.remove(nums[i - k]);
            ans[i - k + 1] = dualHeap.getMedian();
        }
        return ans;
    }

    static class DualHeap2 {
        private PriorityQueue<Integer> small;
        private PriorityQueue<Integer> big;
        private boolean odd; // 是否是奇数，奇数 true 偶数 false

        public DualHeap2(int k) {
            small = new PriorityQueue<>(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2.compareTo(o1); // o2 - o1
                }
            });

            big = new PriorityQueue<>(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1.compareTo(o2);
                }
            });

            odd = (k & 1) == 1;
        }

        public void insert(int num) {
            if (small.isEmpty() || num <= small.peek()) {
                small.offer(num);
            } else {
                big.offer(num);
            }
            makeBalance();
        }

        private void makeBalance() {
            if (small.size() > big.size() + 1) {
                big.offer(small.poll());
            } else if (small.size() == big.size() - 1) {
                small.offer(big.poll());
            }
        }

        private double getMedian() {
            return odd ? small.peek() : ((double) small.peek() + (double) big.peek()) / 2;
        }

        public void remove(int num) {
            if (num <= small.peek()) {
                small.remove(num);
            } else {
                big.remove(num);
            }
            makeBalance();
        }
    }

    public static double[] medianSlidingWindow(int[] nums, int k) {
        DualHeap dh = new DualHeap(k);
        for (int i = 0; i < k; i++) {
            dh.insert(nums[i]);
        }
        double[] ans = new double[nums.length - k + 1];
        ans[0] = dh.getMedian();
        for (int i = k; i < nums.length; i++) {
            dh.insert(nums[i]);
            dh.erase(nums[i - k]);
            ans[i - k + 1] = dh.getMedian();
        }
        return ans;
    }

    static class DualHeap {
        // 大根堆，维护较小的一半元素
        private PriorityQueue<Integer> small;

        // 小根堆，维护较大的一半元素
        private PriorityQueue<Integer> large;

        // 哈希表，记录「延迟删除」的元素，key 为元素，value 为需要删除的次数
        private Map<Integer, Integer> delayed;

        private int k;

        // small 和 large 当前包含的元素个数，需要扣除被「延迟删除」的元素
        private int smallSize, largeSize;

        public DualHeap(int k) {
            this.small = new PriorityQueue<Integer>(new Comparator<Integer>() {
                public int compare(Integer num1, Integer num2) {
                    return num2.compareTo(num1);
                }
            });
            this.large = new PriorityQueue<Integer>(new Comparator<Integer>() {
                public int compare(Integer num1, Integer num2) {
                    return num1.compareTo(num2);
                }
            });
            this.delayed = new HashMap<Integer, Integer>();
            this.k = k;
            this.smallSize = 0;
            this.largeSize = 0;
        }

        public double getMedian() {
            return (k & 1) == 1 ? small.peek() : ((double) small.peek() + large.peek()) / 2;
        }

        public void insert(int num) {
            if (small.isEmpty() || num <= small.peek()) {
                small.offer(num);
                ++smallSize;
            } else {
                large.offer(num);
                ++largeSize;
            }
            makeBalance();
        }

        public void erase(int num) {
            delayed.put(num, delayed.getOrDefault(num, 0) + 1);
            if (num <= small.peek()) {
                --smallSize;
                if (num == small.peek()) {
                    prune(small);
                }
            } else {
                --largeSize;
                if (num == large.peek()) {
                    prune(large);
                }
            }
            makeBalance();
        }

        // 不断地弹出 heap 的堆顶元素，并且更新哈希表
        private void prune(PriorityQueue<Integer> heap) {
            while (!heap.isEmpty()) {
                int num = heap.peek();
                if (delayed.containsKey(num)) {
                    delayed.put(num, delayed.get(num) - 1);
                    if (delayed.get(num) == 0) {
                        delayed.remove(num);
                    }
                    heap.poll();
                } else {
                    break;
                }
            }
        }

        // 调整 small 和 large 中的元素个数，使得二者的元素个数满足要求
        private void makeBalance() {
            if (smallSize > largeSize + 1) {
                // small 比 large 元素多 2 个
                large.offer(small.poll());
                --smallSize;
                ++largeSize;

                // small 堆顶元素被移除，需要进行 prune
                prune(small);
            } else if (smallSize < largeSize) {
                // large 比 small 元素多 1 个
                small.offer(large.poll());
                ++smallSize;
                --largeSize;

                // large 堆顶元素被移除，需要进行 prune
                prune(large);
            }
        }
    }

    public static void main(String[] args) {
        int[] nums = {-2147483648, -2147483648, 2147483647, -2147483648, -2147483648, -2147483648, 2147483647, 2147483647, 2147483647, 2147483647, -2147483648, 2147483647, -2147483648};
        int k = 3;
        double[] arr = medianSlidingWindow2(nums, k);
        System.out.println(Arrays.toString(arr));
    }
}

