package com.xxd.algo.myself.leetcode;

public class Problem_0206_ReverseListTest {

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode last = head;
        ListNode cur = null;
        ListNode pre = head;
        while (last != null) {
            last = last.next;
            pre.next = cur;
            cur = pre;
            pre = last;
        }

        return cur;
    }
}
