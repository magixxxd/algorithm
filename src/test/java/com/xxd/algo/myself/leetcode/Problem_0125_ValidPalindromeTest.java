package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-27 09:36
 * @description:
 */
public class Problem_0125_ValidPalindromeTest {


    public static boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }

        char[] str = s.toLowerCase().toCharArray();
        int L = 0;
        int R = str.length - 1;

        while (L < R) {
            // 如果是字符串，在判断相等
            if (validChar(str[L]) && validChar(str[R])) {
                if (!equal(str[L], str[R])) {
                    return false;
                }
                L++;
                R--;
            } else {
                L += validChar(str[L]) ? 0 : 1;
                R -= validChar(str[R]) ? 0 : 1;
            }
        }
        return true;
    }
    public static boolean equal(char c1, char c2) {
        if (isNumber(c1) || isNumber(c2)) {
            return c1 == c2;
        }

        return (c1 == c2);
    }
    public static boolean validChar(char c) {
        return isLetter(c) || isNumber(c);
    }

    public static boolean isLetter(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean isNumber(char c) {
        return c >= '0' && c <= '9';
    }

    public static void main(String[] args) {

    }
}
