package com.xxd.algo.myself.leetcode;

import com.xxd.algo.leetcode.topinterviewquestions.Problem_0056_MergeIntervals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-21 10:27
 * @description:
 */
public class Problem_0056_MergeIntervalsTest {
    public static class Range implements Comparable<Range> {
        int start;
        int end;

        public Range(int start, int end) {
            this.start = start;
            this.end = end;
        }
        @Override
        public int compareTo(Range o2) {
            return this.start - o2.start;
        }
    }

    public static int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][0];
        }
        Range[] array = new Range[intervals.length];
        for (int i = 0; i < intervals.length; i++) {
            array[i] = new Range(intervals[i][0], intervals[i][1]);
        }
        Arrays.sort(array);

        ArrayList<Range> ans = new ArrayList<>();
        int s = array[0].start;
        int e = array[0].end;
        for (int i = 1; i < array.length; i++) {
            if (array[i].start > e) {
                ans.add(new Range(s, e));
                s = array[i].start;
                e = array[i].end;
            } else {
                e = Math.max(e, array[i].end);
            }
        }

        ans.add(new Range(s, e));

        return generateMatrix(ans);
    }

    private static int[][] generateMatrix(ArrayList<Range> list) {
        int[][] matrix = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            matrix[i] = new int[] { list.get(i).start, list.get(i).end };
        }
        return matrix;
    }

    public static void main(String[] args) {
        int[][] intervals = {
                {1, 3},
                {2, 6},
                {8, 10},
                {15, 18}
        };

        System.out.println(merge(intervals));
    }
}
