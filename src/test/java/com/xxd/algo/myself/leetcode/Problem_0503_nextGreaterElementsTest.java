package com.xxd.algo.myself.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-09 10:10
 * @description:
 */
public class Problem_0503_nextGreaterElementsTest {

    public static int[] nextGreaterElements(int[] nums) {
        int N = nums.length;
        int[] ans = new int[N];
        Arrays.fill(ans, -1);

        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < 2 * N - 1; i++) {
            while (!stack.isEmpty() && nums[stack.peek()] < nums[i % N]) {
                ans[stack.pop()] = nums[i % N];
            }
            stack.push(i % N); // 大到小
        }

        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {5, 4, 3, 2, 1};
        System.out.println(Arrays.toString(nextGreaterElements(arr)));
    }
}
