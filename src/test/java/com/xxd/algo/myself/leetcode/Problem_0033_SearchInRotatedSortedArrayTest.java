package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-13 07:34
 * @description:
 */
public class Problem_0033_SearchInRotatedSortedArrayTest {

    public static int search(int[] arr, int num) {
        int L = 0;
        int R = arr.length - 1;
        int M = 0;
        while (L <= R) {
            M = (L + R) / 2;

            if (num == arr[M]) {
                return M;
            }

            if (arr[M] > arr[L]) { // 左边一定是有序的
                if (num >= arr[L] && num < arr[M]) {
                    R = M - 1;
                } else {
                    L = M + 1;
                }
            } else if (arr[M] < arr[L]) { // 右边一定是有序的
                if (num > arr[M] && num <= arr[R]) {
                    L = M + 1;
                } else {
                    R = M - 1;
                }
            } else if (arr[M] == arr[L]) { // 说明只有几个数
                return num == arr[R] ? R : -1;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
         // int[] arr = {4, 5, 6, 7, 0, 1, 2};
        //int[] arr = {3, 1};
        // int[] arr = {6, 7, 2, 3, 4, 5};
        int[] arr = {5,1,3};
        System.out.println(search(arr, 3));
    }
}
