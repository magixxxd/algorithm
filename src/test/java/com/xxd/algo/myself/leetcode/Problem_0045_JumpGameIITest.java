package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-18 09:56
 * @description:
 */
public class Problem_0045_JumpGameIITest {


    public static int jump(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 1;
        }

        // 你到目前 i 的地方，你最少需要跳多少次
        int step = 0;

        // 你在跳了step步的情况下 理论能跳的最远距离是多少
        int cur = 0;

        // 在i这个位置，如果此时你在跳一步能到的最远距离
        int next = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (cur < i) {
                step++;
                cur = next;
            }
            next = Math.max(next, i + nums[i]);
        }

        return step;
    }
}
