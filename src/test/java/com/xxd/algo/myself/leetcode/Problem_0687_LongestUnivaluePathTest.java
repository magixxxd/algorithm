package com.xxd.algo.myself.leetcode;


/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-19 16:57
 * @description:
 */
public class Problem_0687_LongestUnivaluePathTest {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int v) {
            val = v;
        }
    }

    public static int longestUnivaluePath(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return process(root).max - 1;
    }
    
    
    public static class Info {
        // 必须从x出发，最大距离
        int len;

        // 不一定是从x出发的，最大距离
        int max;

        public Info(int len, int max) {
            this.max = max;
            this.len = len;
        }
    }

    public static Info process(TreeNode x) {
        if (x == null) {
            return new Info(0, 0);
        }

        TreeNode left = x.left;
        TreeNode right = x.right;
        Info leftInfo = process(left);
        Info rightInfo = process(right);

        int len = 1;
        if (left != null && left.val == x.val) {
            len = leftInfo.len + 1;
        }
        if (right != null && right.val == x.val) {
            len = Math.max(len, rightInfo.len + 1);
        }

        int max = Math.max(Math.max(leftInfo.max, rightInfo.max), len);
        if (left != null && right != null
                && left.val == x.val && right.val == x.val) {
            max = Math.max(max, leftInfo.len + rightInfo.len + 1);
        }

        return new Info(len, max);
    }

    public static void main(String[] args) {

    }
}