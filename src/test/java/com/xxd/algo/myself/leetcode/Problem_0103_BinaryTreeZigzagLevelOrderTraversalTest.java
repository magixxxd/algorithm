package com.xxd.algo.myself.leetcode;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-24 18:08
 * @description:
 */
public class Problem_0103_BinaryTreeZigzagLevelOrderTraversalTest {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }

        LinkedList<TreeNode> deque = new LinkedList<>();
        deque.add(root);
        int size = 0;
        boolean reserve = true;
        while (!deque.isEmpty()) {
            size = deque.size();
            TreeNode curNode = null;
            List<Integer> curLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                if (reserve) {
                    curNode = deque.pollFirst();
                    curLevel.add(curNode.val);

                    if (curNode.left != null) {
                        deque.addLast(curNode.left);
                    }
                    if (curNode.right != null) {
                        deque.addLast(curNode.right);
                    }
                } else {
                    curNode = deque.pollLast();
                    curLevel.add(curNode.val);

                    if (curNode.right != null) {
                        deque.addFirst(curNode.right);
                    }
                    if (curNode.left != null) {
                        deque.addFirst(curNode.left);
                    }
                }
            }

            ans.add(curLevel);
            reserve = !reserve;
        }

        return ans;
    }
}
