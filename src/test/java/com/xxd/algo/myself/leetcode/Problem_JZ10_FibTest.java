package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-08 17:36
 * @description:
 */
public class Problem_JZ10_FibTest {

    public static int fib(int n) {
        if (n < 1) {
            return 0;
        }

        if (n == 1 || n == 2) {
            return 1;
        }

        int pre1 = 1;
        int pre2 = 1;
        for (int i = 3; i <= n; i++) {
            int temp = pre1 + pre2;
            pre1 = pre2;
            pre2 = temp;
        }

        return (int) ((pre2) % (1e9 + 7));
    }

    public static void main(String[] args) {
        System.out.println(fib(5));
    }
}
