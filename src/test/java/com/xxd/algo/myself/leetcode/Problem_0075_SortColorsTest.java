package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-22 16:38
 * @description:
 */
public class Problem_0075_SortColorsTest {

    public static void sortColors(int[] nums) {
        int less = -1;
        int more = nums.length;
        int index = 0;
        while (index < more) {
            if (nums[index] > 1) {
                swap(nums, index, --more);
            } else if (nums[index] < 1) {
                swap(nums, index++, ++less);
            } else {
                index++;
            }
        }
    }


    public static void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}
