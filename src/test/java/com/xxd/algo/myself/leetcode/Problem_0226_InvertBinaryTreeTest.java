package com.xxd.algo.myself.leetcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-23 11:31
 * @description:
 */
public class Problem_0226_InvertBinaryTreeTest {


    public class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
    }

    public static TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }

        TreeNode left = root.left;
        root.left = invertTree(root.right);
        root.right = invertTree(left);

        return root;
    }
}
