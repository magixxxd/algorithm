package com.xxd.algo.myself.leetcode;

import javax.sound.midi.MidiChannel;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-13 08:10
 * @description:
 */
public class Problem_0034_FindFirstAndLastPositionOfElementInSortedArrayTest {

    public static int[] searchRange(int[] nums, int target) {
        int[] ans = {-1, -1};

        ans[0] = findFirst(nums, target);
        ans[1] = findLast(nums, target);
        return ans;
    }

    private static int findLast(int[] arr, int target) {
        int L = 0;
        int R = arr.length - 1;
        int M = 0;
        int ans = -1;
        while (L <= R) {
            M = (L + R) / 2;
            M = (L + R) / 2;
            if (target > arr[M]) {
                L = M + 1;
            } else if (target < arr[M]) {
                R = M - 1;
            } else {
                ans = M;
                L = M + 1;
            }
        }
        return ans;
    }

    private static int findFirst(int[] arr, int target) {
        int L = 0;
        int R = arr.length - 1;
        int M = 0;
        int ans = -1;
        while (L <= R) {
            M = (L + R) / 2;
            if (target > arr[M]) {
                L = M + 1;
            } else if (target < arr[M]) {
                R = M - 1;
            } else {
                ans = M;
                R = M - 1;
            }
        }
        return ans;
    }
}
