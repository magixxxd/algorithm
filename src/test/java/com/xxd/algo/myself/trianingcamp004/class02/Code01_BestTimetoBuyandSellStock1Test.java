package com.xxd.algo.myself.trianingcamp004.class02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-01 08:20
 * @description:
 */
public class Code01_BestTimetoBuyandSellStock1Test {

    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < prices.length; i++) {
            max = Math.max(max, prices[i]);
            min = Math.min(min, prices[i]);
        }

        return max - min;
    }

    public static void main(String[] args) {
        int[] arr = {1,4,5,2,34,5,76,8,2};
        System.out.println(maxProfit(arr));
    }
}
