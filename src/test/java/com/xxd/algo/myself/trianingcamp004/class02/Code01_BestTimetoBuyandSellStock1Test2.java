package com.xxd.algo.myself.trianingcamp004.class02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-05-01 08:34
 * @description:
 */
public class Code01_BestTimetoBuyandSellStock1Test2 {
    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }

        int ans = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            ans += Math.max(prices[i + 1] - prices[i], 0);
        }


        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {1, 10, 3, 8, 2, 7};
        System.out.println(maxProfit(arr));
    }
}
