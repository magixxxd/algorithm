package com.xxd.algo.myself.trainingcamp001.class01;

import com.xxd.algo.newcode.high02.class06.Code03_TarjanAndDisjointSetsForLCA;

import java.util.LinkedList;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-28 09:44
 * @description:
 */
public class Code01_SlidingWindowMaxArrayTest {

    public static int[] getMaxWindow(int[] arr, int w) {
        if (arr == null || arr.length == 0 || arr.length < w) {
            return new int[0];
        }

        LinkedList<Integer> qmax = new LinkedList<>();
        int[] res = new int[arr.length - w + 1];
        int R = 0;
        int index = 0;
        while (R < arr.length) {
            while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[R]) {
                qmax.pollLast();
            }

            qmax.addLast(R);

            if (qmax.peekFirst() == R - w) {
                qmax.pollFirst();
            }

            if (R >= w - 1) {
                res[index++] = arr[qmax.peekFirst()];
            }

            R++;
        }

        return res;
    }

    public static void main(String[] args) {
        //int[] arr = {1, 3, -1, -3, 5, 3, 6, 7};
        int[] arr = {};
        int w = 0;
        int[] res = getMaxWindow(arr, w);
        System.out.println(res);

    }
}
