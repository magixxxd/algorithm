package com.xxd.algo.myself.trainingcamp001.class01;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-06-28 10:46
 * @description:
 */
public class Code04_AllTimesMinToMaxTest {




    public static int max(int[] arr) {
        int size = arr.length;

        // 累加和预处理
        int[] sums = new int[size];
        sums[0] = arr[0];
        for (int i = 1; i < size; i++) {
            sums[i] = sums[i - 1] + arr[i];
        }

        // 申请一个单调栈，栈底到栈顶 从小到大
        Stack<Integer> stack = new Stack<>();
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < size; i++) {
            while (!stack.isEmpty() && arr[i] <= arr[stack.peek()]) {
                // stack.peek() 左侧比我小的，离我最近的
                // i 右侧比我小的，离我最近的
                Integer index = stack.pop();
                int sum = stack.isEmpty() ? sums[i - 1] : sums[i - 1] - sums[stack.peek()];
                max = Math.max(max, sum * arr[index]);
            }

            stack.add(i);
        }

        while (!stack.isEmpty()) {
            int j = stack.pop();
            int sum = stack.isEmpty() ? sums[size - 1] : sums[size - 1] - sums[stack.peek()];
            max = Math.max(max, sum * arr[j]);
        }
        return max;
    }

    public static int max2(int[] arr) {
        int size = arr.length;
        int[] sums = new int[size];
        sums[0] = arr[0];
        for (int i = 1; i < size; i++) {
            sums[i] += sums[i - 1] + arr[i];
        }
        int max = Integer.MIN_VALUE;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < size; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] >= arr[i]) {
                int j = stack.pop();
                int sum = stack.isEmpty() ? sums[i - 1] : sums[i - 1] - sums[stack.peek()];
                max = Math.max(max, sum * arr[j]);
            }
            stack.push(i);
        }

        while (!stack.isEmpty()) {
            int j = stack.pop();
            int sum = stack.isEmpty() ? sums[size - 1] : sums[size - 1] - sums[stack.peek()];
            max = Math.max(max, sum * arr[j]);
        }
        return max;
    }


    public static void main(String[] args) {
        int[] arr = {2, 1, 3, 2, 1};
        System.out.println(max(arr));
        System.out.println(max2(arr));
    }
}
