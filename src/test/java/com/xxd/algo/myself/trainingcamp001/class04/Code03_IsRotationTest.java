package com.xxd.algo.myself.trainingcamp001.class04;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-16 09:34
 * @description:
 */
public class Code03_IsRotationTest {

    public static boolean isRotation(String a, String b) {
        if (a == null || b == null || a.length() != b.length()) {
            return false;
        }

        String b2 = b + b;
        return getIndexof(b2, a) != -1;
    }

    private static int getIndexof(String s, String m) {
        if (s.length() < m.length()) {
            return -1;
        }
        char[] str = s.toCharArray();
        char[] mtr = m.toCharArray();
        int[] next = getNextArray(mtr);

        int x = 0;
        int y = 0;
        while (x < str.length && y < mtr.length) {
            if (str[x] == mtr[y]) {
                x++;
                y++;
            } else if (next[y] == -1) {
                x++;
            } else {
                y = next[y];
            }
        }
        return y == mtr.length ? x - y : -1;
    }

    private static int[] getNextArray(char[] mtr) {
        if (mtr.length == 1) {
            return new int[]{-1};
        }

        int[] next = new int[mtr.length];
        next[0] = -1;
        next[1] = 1;

        int index = 2;
        int cn = 0;
        while (index < mtr.length) {
            if (mtr[index - 1] == mtr[cn]) {
                next[index++] = ++cn;
            } else if (cn > 0) {
                cn = next[cn];
            } else {
                next[index++] = 0;
            }
        }
        return next;
    }

    public static void main(String[] args) {
        String str1 = "yunzuocheng";
        String str2 = "zuochengyun";
        System.out.println(isRotation(str1, str2));

    }
}
