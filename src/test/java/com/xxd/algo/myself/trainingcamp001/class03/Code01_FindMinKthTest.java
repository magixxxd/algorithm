package com.xxd.algo.myself.trainingcamp001.class03;

import com.sun.org.apache.bcel.internal.generic.SWAP;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-16 10:09
 * @description:
 */
public class Code01_FindMinKthTest {

    public static int minKth(int[] array, int k) {
        int[] arr = copyArray(array);
        return process(arr, 0, arr.length - 1, k - 1);
    }

    // arr[L..R]  范围上，如果排序的话(不是真的去排序)，找位于index的数
    private static int process(int[] arr, int L, int R, int index) {
        if (L == R) {
            return arr[L];
        }

        int pivot = arr[L + (int) (Math.random() * (R - L + 1))];

        int[] range = partition(arr, L, R, pivot);
        if (index < range[0]) {
            return process(arr, L, range[0] - 1, index);
        } else if (index <= range[1]) {
            return arr[index];
        } else {
            return process(arr, range[1] + 1, R, index);
        }
    }

    private static int[] partition(int[] arr, int l, int r, int pivot) {
        int less = l - 1;
        int more = r + 1;
        int cur = l;
        while (cur < more) {
            if (arr[cur] < pivot) {
                swap(arr, ++less, cur++);
            } else if (arr[cur] == pivot) {
                cur++;
            } else if (arr[cur] > pivot) {
                swap(arr, cur, --more);
            }
        }
        return new int[]{less + 1, r - 1};
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static int[] copyArray(int[] arr) {
        int[] ans = new int[arr.length];
        for (int i = 0; i != ans.length; i++) {
            ans[i] = arr[i];
        }
        return ans;
    }


    public static void main(String[] args) {
        int[] arr = {2, 1, 3, 4, 5, 6, 2};
        System.out.println(minKth(arr, 6));
    }
}
