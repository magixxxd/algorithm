package com.xxd.algo.myself.trainingcamp001.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-07-17 16:47
 * @description:
 */
public class Code01_ManacherTest {

    public static int manacher(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // 处理成 manacher 子串
        char[] str = manacherString(s);

        // 申请pArray R C
        int[] pArray = new int[str.length];
        int R = -1;
        int C = -1;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < str.length; i++) {
            // i 是否在R 内
            pArray[i] = R > i ? Math.min(pArray[2 * C - i], R - i) : 1;

            while (i + pArray[i] < str.length && i - pArray[i] > -1) {
                if (str[i + pArray[i]] == str[i - pArray[i]]) {
                    pArray[i]++;
                } else {
                    break;
                }
            }

            if (i + pArray[i] > R) {
                R = i + pArray[i];
                C = i;
            }

            max = Math.max(max, pArray[i]);
        }

        return max - 1;
    }

    private static char[] manacherString(String s) {
        char[] chars = new char[2 * s.length() + 1];
        int index = 0;
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (i & 1) == 0 ? '#' : s.charAt(index++);
        }
        return chars;
    }

    public static void main(String[] args) {
        String test = "abc123321w2";
        System.out.println(manacher(test));
    }
}
