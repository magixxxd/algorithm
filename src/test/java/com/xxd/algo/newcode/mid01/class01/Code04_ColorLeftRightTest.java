package com.xxd.algo.newcode.mid01.class01;

import java.util.Arrays;

public class Code04_ColorLeftRightTest {

    public static int minPaint1(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }
        int res = Integer.MAX_VALUE;
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            // i 当前的边界位置,也就是左边需要多少个R
            int leftR = 0;
            for (int R = 0; R < i; R++) {
                if (chars[R] == 'G') {
                    leftR++;
                }
            }

            int rightG = 0;
            for (int j = i; j < chars.length; j++) {
                if (chars[j] == 'R') {
                    rightG++;
                }
            }

            res = Math.min(res, rightG + leftR);
        }

        return res;
    }

    // RGRGR -> RRRGG
    public static int minPaint(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }

        char[] chars = s.toCharArray();
        int[] left = new int[chars.length]; // 辅助数组，左边有多少个G
        int[] right = new int[chars.length];// 辅助数组，右边有多少个R
        for (int i = 0; i < chars.length; i++) {
            if (i == 0) {
                left[i] = chars[i] == 'G' ? 1 : 0;
            } else {
                left[i] = chars[i] == 'G' ? left[i - 1] + 1 : left[i - 1];
            }
        }

        for (int i = chars.length - 1; i >= 0; i--) {
            if (i == chars.length - 1) {
                right[i] = chars[i] == 'R' ? 1 : 0;
            } else {
                right[i] = chars[i] == 'R' ? right[i + 1] + 1 : right[i + 1];
            }
        }

        // 全部涂G
        int res = left[left.length - 1];

        // 全部涂R
        res = Math.min(res, right[0]);

        for (int i = 0; i < chars.length - 1; i++) {
            // i 当前的边界位置,也就是左边需要多少个R
            int leftR = left[i];
            int rightG = right[i + 1];
            res = Math.min(res, leftR + rightG);
        }

        return res;
    }


    public static int minPaint2(String s) {
        // 尝试所有的边界，0 ~ s.length - 1
        int N = s.length();
        char[] chars = s.toCharArray();

        int[] leftG = new int[N];
        int[] rightR = new int[N];

        // 左边多少个G啊
        leftG[0] = chars[0] == 'G' ? 1 : 0;
        for (int i = 1; i < chars.length; i++) {
            leftG[i] = chars[i] == 'G' ? leftG[i - 1] + 1 : leftG[i - 1];
        }

        // 右边有多少个R
        rightR[N - 1] = chars[N - 1] == 'R' ? 1 : 0;
        for (int i = N - 2; i >= 0; i--) {
            rightR[i] = chars[i] == 'R' ? rightR[i + 1] + 1 : rightR[i + 1];
        }

        // 全部涂R 的情况
        int ans = leftG[N - 1];

        // 全部涂G
        ans = Math.min(ans, rightR[0]);

        // 枚举左边涂R的个数
        for (int i = 1; i < N; i++) {
            ans = Math.min(ans, leftG[i - 1] + rightR[i]);
        }

        return ans;
    }

    public static void main(String[] args) {
        // String test = "GRRRGGGRRG";
        //  String test = "RGRGR";
        // String test = "RGRGGGG";
        // String test = "RGGGGGG";
        String test = "GRRGGGRG";
        System.out.println(minPaint(test));
        System.out.println(minPaint1(test));
        System.out.println(minPaint2(test));

    }
}
