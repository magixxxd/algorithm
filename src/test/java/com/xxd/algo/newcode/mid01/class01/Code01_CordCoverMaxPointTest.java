package com.xxd.algo.newcode.mid01.class01;

import com.sun.rowset.providers.RIOptimisticProvider;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-08-24 16:28
 * @description: 绳子覆盖点
 */
public class Code01_CordCoverMaxPointTest {

    /**
     * 暴力方法
     * 以数组每个元素开头 往后遍历，直到不能覆盖的那个元素为止
     * 然后在遍历下一个元素
     *
     * @param arr
     * @param L
     * @return
     */
    public static int test(int[] arr, int L) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            int curPoint = arr[i];
            int point = 1;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] - curPoint <= L) {
                    point++;
                }
            }
            max = Math.max(max, point);
        }
        return max;
    }

    /**
     * 二分查找
     *
     * @param arr
     * @param L
     * @return
     */
    public static int maxPoint1(int[] arr, int L) {
        int res = 1;
        for (int i = 0; i < arr.length; i++) {
            int pre = arr[i] - L;
            int index = nearestIndex(arr, i, pre);
            res = Math.max(res, i - index + 1);
        }
        return res;
    }

    /**
     * 2 7 13 18 20 32
     * 查找范围在 0 .. R 上 大于等于value的最小值 的索引
     *
     * @param arr
     * @param R
     * @param value 5
     * @return
     */
    private static int nearestIndex(int[] arr, int R, int value) {
        int index = R;
        int L = 0;
        while (L <= R) {
            int mid = L + ((R - L) >> 1);
            if (arr[mid] >= value) {
                R = mid - 1;
                index = mid;
            } else if (arr[mid] < value) {
                L = mid + 1;
            }
        }

        return index;
    }

    /**
     * 滑动窗口
     *
     * @param arr
     * @param L
     * @return
     */
    public static int maxPoint2(int[] arr, int L) {
        int left = 0;
        int right = 0;
        int max = 0;
        while (left < arr.length) {
            while (right < arr.length && arr[right] - arr[left] <= L) {
                max = Math.max(max, right - left + 1);
                right++;
            }
            left++;
        }


        return max;
    }


    public static int maxPoint3(int[] arr, int L) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int left = 0;
        int right = 0;
        int max = 0;

        while (left <= right) {
            while (right < arr.length && arr[right] - arr[left] <= L) {
                right++;
            }

            max = Math.max(max, right - left++);
        }
        return max;
    }


    public static void main(String[] args) {
        int[] arr = {99, 105, 120, 121, 122, 123, 125, 127, 128};
        //int[] arr = {2, 7, 13, 15, 16, 19, 22, 34};
        System.out.println(test(arr, 8));
        System.out.println(maxPoint1(arr, 8));
        System.out.println(maxPoint2(arr, 8));
        System.out.println(maxPoint3(arr, 8));
    }
}
