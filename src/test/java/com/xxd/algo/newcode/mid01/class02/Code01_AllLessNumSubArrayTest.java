package com.xxd.algo.newcode.mid01.class02;

import java.util.LinkedList;

public class Code01_AllLessNumSubArrayTest {

    public static int getNum(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        LinkedList<Integer> qmax = new LinkedList<>();
        LinkedList<Integer> qmin = new LinkedList<>();
        int res = 0;
        int L = 0;
        int R = 0;

        while (L < arr.length) {
            while (R < arr.length) {
                // 更新qmax
                while (!qmax.isEmpty() && arr[R] >= arr[qmax.peekLast()]) {
                    qmax.pollLast();
                }
                qmax.addLast(R);

                // 更新qmin
                while (!qmin.isEmpty() && arr[qmin.peekLast()] >= arr[R]) {
                    qmin.pollLast();
                }
                qmin.addLast(R);

                // 判断是否违规了
                if (arr[qmax.peekFirst()] - arr[qmin.peekFirst()] > num) {
                    break;
                }

                R++;
            }

            res += R - L;

            // 过期队列 qmin
            if (!qmin.isEmpty() && qmin.peekFirst() == L) {
                qmin.pollFirst();
            }

            if (!qmax.isEmpty() && qmax.peekFirst() == L) {
                qmax.pollFirst();
            }
            L++;
        }

        return res;
    }



    public static int getNum2(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        LinkedList<Integer> qmax = new LinkedList<>();
        LinkedList<Integer> qmin = new LinkedList<>();
        int L = 0;
        int R = 0;
        int res = 0;

        while (L < arr.length) {
            while (R < arr.length) {
                // 更新qmax
                while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[R]) {
                    qmax.pollLast();
                }
                qmax.addLast(R);

                // 更新qmin
                while (!qmin.isEmpty() && arr[qmin.peekLast()] >= arr[R]) {
                    qmin.pollLast();
                }
                qmin.addLast(R);

                // 判断是否违规
                if (arr[qmax.peekFirst()] - arr[qmin.peekFirst()] > num) {
                    break;
                }

                R++;
            }

            // 违规了，开始统计子数组的情况

            res += R - L;

            // 过期队列 qmin
            if (!qmin.isEmpty() && qmin.peekFirst() == L) {
                qmin.pollFirst();
            }

            if (!qmax.isEmpty() && qmax.peekFirst() == L) {
                qmax.pollFirst();
            }

            L++;
        }

        return res;
    }

    // for test
    public static int[] getRandomArray(int len) {
        if (len < 0) {
            return null;
        }
        int[] arr = new int[len];
        for (int i = 0; i < len; i++) {
            arr[i] = (int) (Math.random() * 10);
        }
        return arr;
    }

    // for test
    public static void printArray(int[] arr) {
        if (arr != null) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        // int[] arr = getRandomArray(30);
        int[] arr = {9, 3, 2, 6, 1, 8, 4, 2, 6};
        int num = 5;
        printArray(arr);
        System.out.println(getNum(arr, num));
        System.out.println(getNum2(arr, num));
    }

}
