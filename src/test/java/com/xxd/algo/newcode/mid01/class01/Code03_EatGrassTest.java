package com.xxd.algo.newcode.mid01.class01;

public class Code03_EatGrassTest {

    // n份青草放在一堆
    // 先手后手都决定聪明
    // string "先手" "后手"

    /**
     * 0 后手赢
     * 1 先手赢
     * 2 后手赢
     * 3 1 1 1 先手赢
     * 4 先手赢
     * 5 后手赢
     * 6 先
     *
     * @param n
     * @return
     */
    public static String winner1(int n) {
        if (n < 5) {
            return (n == 0 || n == 2) ? "后手" : "先手";
        }
        int base = 1;
        while (base <= n) {
            String winner = winner1(n - base);
            if (winner.equals("后手")) { // 说明我是真的赢了
                return "先手";
            }
            if (base > n / 4) {
                break;
            }

            base = base * 4;
        }

        return "后手";
    }

    /**
     *
     * @param n
     * @return
     */
    public static String winner3(int n) {
        if (n < 5) {
            return n == 0 || n == 2 ? "后手" : "先手";
        }

        int base = 1;

        while (base <= n) {
            String winner = winner3(n - base);
            if (winner.equals("后手")) {
                return "先手";
            }

            if (base > n / 4) {
                break;
            }

            base = base * 4;
        }

        return "后手";
    }


    public static String winner2(int n) {
        if (n % 5 == 0 || n % 5 == 2) {
            return "后手";
        } else {
            return "先手";
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i <= 50; i++) {
            System.out.println(i + " : " + winner1(i) + "\t" + winner2(i) + "\t" + winner3(i));
            System.out.println(i + " : " + winner1(i) + "\t" + winner2(i));
        }
    }
}
