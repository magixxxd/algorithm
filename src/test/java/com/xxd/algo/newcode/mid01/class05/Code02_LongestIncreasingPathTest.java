package com.xxd.algo.newcode.mid01.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-10-17 19:28
 */
public class Code02_LongestIncreasingPathTest {


    public static int maxPath(int[][] matrix) {
        int ans = Integer.MIN_VALUE;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                ans = Math.max(ans, process(matrix, i, j));
            }
        }

        return ans;
    }

    private static int process(int[][] matrix, int row, int col) {
        if (row < 0 || row >= matrix.length || col < 0 || col >= matrix[0].length) {
            return -1;
        }

        int up = 0;
        int down = 0;
        int left = 0;
        int right = 0;

        if (row - 1 > 0 && matrix[row - 1][col] > matrix[row][col]) {
            up = process(matrix, row - 1, col);
        }

        if (row + 1 < matrix.length && matrix[row + 1][col] > matrix[row][col]) {
            down = process(matrix, row + 1, col);
        }

        if (col - 1 > 0 && matrix[row][col - 1] > matrix[row][col]) {
            left = process(matrix, row, col - 1);
        }

        if (col + 1 < matrix[0].length && matrix[row][col + 1] > matrix[row][col]) {
            up = process(matrix, row, col + 1);
        }


        return 1 + Math.max(Math.max(up, down), Math.max(left, right));
    }

}
