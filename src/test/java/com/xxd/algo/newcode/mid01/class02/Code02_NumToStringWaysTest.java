package com.xxd.algo.newcode.mid01.class02;

public class Code02_NumToStringWaysTest {

    public static int convertWays(int num) {
        if (num < 1) {
            return 0;
        }
        return process(String.valueOf(num).toCharArray(), 0);
    }

    private static int process(char[] str, int index) {
        if (index == str.length) {
            return 1;
        }

        if (str[index] == '0') {
            return 0;
        }

        if (index == str.length - 1) {
            return 1;
        }

        int res = 0;
        // 单独 一个数字
        res += process(str, index + 1);

        // 如果可以两个数字 合并起来
        if (((str[index] - '0') * 10 + (str[index + 1] - '0')) < 27) {
            res += process2(str, index + 2);
        }

        return res;
    }


    public static int convertWays2(int num) {
        if (num < 1) {
            return 0;
        }
        return process2(String.valueOf(num).toCharArray(), 0);
    }

    /**
     * 代表 从 index 开始 到最后的length 有多少种转法
     *
     * @param str
     * @param index
     * @return
     */
    private static int process2(char[] str, int index) {
        if (index == str.length) { // 全部转完了，没有方法了呀
            return 1;
        }

        if (str[index] == '0') {
            return 0; // 没有转法
        }

        if (index == str.length - 1) { // 只有一种转法了哇
            return 1;
        }

        int res = process2(str, index + 1);

        if (((str[index] - '0') * 10 + str[index + 1] - '0') < 27) { // 1 2  看下能不能和下一位数值结合起来
            res += process2(str, index + 2);
        }

        return res;
    }


    // 还能简化
    public static int dpways(int num) {
        if (num < 1) {
            return 0;
        }
        char[] str = String.valueOf(num).toCharArray();
        int N = str.length;
        int[] dp = new int[N + 1];
        dp[N] = 1;
        dp[N - 1] = str[N - 1] == '0' ? 0 : 1;
        for (int i = N - 2; i >= 0; i--) {
            if (str[i] == '0') {
                dp[i] = 0;
            } else {
                dp[i] = dp[i + 1]
                        + (((str[i] - '0') * 10 + str[i + 1] - '0') < 27 ? dp[i + 2]
                        : 0);
            }
        }
        return dp[0];
    }


    public static int dpways2(int num) {
        if (num < 1) {
            return 0;
        }
        char[] str = String.valueOf(num).toCharArray();
        int N = str.length;

        // index = 0 ~ index
        int[] dp = new int[N + 1];
        dp[N] = 1;
        dp[N - 1] = str[N - 1] == '0' ? 0 : 1;
        for (int i = N - 2; i >= 0; i--) {
            if (str[i] == '0') {
                dp[i] = 0;
            } else {
                dp[i] = dp[i + 1]
                        + (((str[i] - '0') * 10 + str[i + 1] - '0') < 27 ? dp[i + 2]
                        : 0);
            }
        }

        return dp[0];
    }

    public static void main(String[] args) {
        int test = 312332791;
        System.out.println(convertWays(test));
        System.out.println(convertWays2(test));
        System.out.println(dpways(test));
        System.out.println(dpways2(test));
    }

}
