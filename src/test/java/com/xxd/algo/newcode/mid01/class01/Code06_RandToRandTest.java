package com.xxd.algo.newcode.mid01.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-08-06 05:53
 * @description:
 */
public class Code06_RandToRandTest {

    /**
     * 底层依赖f函数，f函数是可以等概率返回 a - b
     * 生成 等概率发挥 c - d 的函数
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    public static int g(int a, int b, int c, int d) {
        int range = d - c;
        int num = 1; // 需要几个二进制位呢
        while ((1 << num) - 1 < range) {
            num++;
        }

        int ans = 0;

        do {
            ans = 0;
            for (int i = 0; i < num; i++) {
                ans += (rand01(a, b) << i);
            }
        } while (ans > range);

        return ans + c;
    }

    /**
     * 等概率返回 0 和 1 的函数, 依赖等概率返回 a - b 的函数
     * @param a
     * @param b
     * @return
     */
    public static int rand01(int a, int b) {
        int size = b - a + 1;
        int mid = size / 2;
        boolean odd = size % 2 != 0; // 长度是奇数吗？
        int ans = 0;
        do {
            ans = f(a, b) - a;
        } while (odd && ans == mid);

        return ans < mid ? 0 : 1;
    }

    public static int f(int a, int b) {
        return a + (int) (Math.random() * (b - a + 1));
    }


    public static int rand01(double p) {
        int ans = 0;
        do {
            ans = rand01p(p);
        } while (ans == rand01p(p));

        return ans;
    }

    public static int rand01p(double p) {
        return Math.random() < p ? 0 : 1;
    }

    public static void main(String[] args) {
        int a = 2;
        int b = 7;
        int c = 3;
        int d = 10;
        int[] ans = new int[d + 1];
        int testTime1 = 1000000;
        for (int i = 0; i < testTime1; i++) {
            ans[g(a, b, c, d)]++;
        }
        for (int i = 0; i < ans.length; i++) {
            System.out.println(ans[i]);
        }

        int testTime2 = 10000000;
        int count2 = 0;
        // p是返回0的概率，1-p是返回1的概率，不管你怎么指定，都能调成0和1等概率返回
        double p = 0.88;
        for (int i = 0; i < testTime2; i++) {
            if (rand01(p) == 0) {
                count2++;
            }
        }
        System.out.println((double) count2 / (double) testTime2);

    }
}
