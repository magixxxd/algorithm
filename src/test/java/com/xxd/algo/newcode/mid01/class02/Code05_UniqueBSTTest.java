package com.xxd.algo.newcode.mid01.class02;

import java.util.LinkedList;
import java.util.List;

public class Code05_UniqueBSTTest {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    static class Info {
        int ways;

        public Info(int ways) {
            this.ways = ways;
        }
    }

    public static Info process(int n) {
        if (n < 0) {
            return new Info(0);
        }

        if (n == 1 || n == 0) {
            return new Info(1);
        }

        if (n == 2) {
            return new Info(2);
        }
        int res = 0;
        for (int leftNum = 0; leftNum < n; leftNum++) {
            Info leftInfo = process(leftNum);
            Info rightInfo = process(n - leftNum - 1);
            res += leftInfo.ways * rightInfo.ways;
        }

        return new Info(res);
    }

    public static int numTrees(int n) {
        if (n < 0) {
            return 0;
        }
        if (n == 1 || n == 0) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }

        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }

        return dp[n];
    }


    public static int numTrees2(int n) {
        if (n < 0) {
            return 0;
        }
        if (n == 1 || n == 0) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }

        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 1; i < n + 1; i++) {
            for (int j = 0; j < i; j++) { // 左侧的个数
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }
        return dp[n];
    }

    // 假设一共有n个节点，节点值依次1~n
    // list里面装的是不同结构的树的头
    public static List<Node> generateTrees(int n) {
        return generate(1, n);
    }

    // 节点值是 start .. end，形成所有不同的结构返回
    public static List<Node> generate(int start, int end) {
        List<Node> res = new LinkedList<Node>();
        if (start > end) {
            res.add(null);
        }
        Node head = null;
        for (int i = start; i < end + 1; i++) { // 节点值为i的节点，当头节点
            head = new Node(i);
            List<Node> lSubs = generate(start, i - 1);
            List<Node> rSubs = generate(i + 1, end);
            for (Node l : lSubs) {
                for (Node r : rSubs) {
                    head.left = l;
                    head.right = r;
                    res.add(cloneTree(head));
                }
            }
        }
        return res;
    }

    public static Node cloneTree(Node head) {
        if (head == null) {
            return null;
        }
        Node res = new Node(head.value);
        res.left = cloneTree(head.left);
        res.right = cloneTree(head.right);
        return res;
    }

    // for test -- print tree
    public static void printTree(Node head) {
        System.out.println("Binary Tree:");
        printInOrder(head, 0, "H", 17);
        System.out.println();
    }

    public static void printInOrder(Node head, int height, String to, int len) {
        if (head == null) {
            return;
        }
        printInOrder(head.right, height + 1, "v", len);
        String val = to + head.value + to;
        int lenM = val.length();
        int lenL = (len - lenM) / 2;
        int lenR = len - lenM - lenL;
        val = getSpace(lenL) + val + getSpace(lenR);
        System.out.println(getSpace(height * len) + val);
        printInOrder(head.left, height + 1, "^", len);
    }

    public static String getSpace(int num) {
        String space = " ";
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < num; i++) {
            buf.append(space);
        }
        return buf.toString();
    }

    public static void main(String[] args) {
        int n = 5;
        System.out.println(numTrees(n));
        System.out.println(process(n).ways);
       /* List<Node> res = generateTrees(n);
        for (Node node : res) {
            printTree(node);
        }*/
    }

}
