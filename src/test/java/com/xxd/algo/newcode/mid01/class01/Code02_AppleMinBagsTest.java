package com.xxd.algo.newcode.mid01.class01;

import java.security.interfaces.RSAKey;

public class Code02_AppleMinBagsTest {

    /**
     * 贪心算法
     *
     * @param apple
     * @return
     */
    public static int minBags(int apple) {
        if (apple < 6) {
            return -1;
        }

        // 先用8的袋子装
        int bag8 = apple / 8;

        int rest = apple - bag8 * 8;

        int bag6 = -1;
        while (bag8 >= 0) {
            // 再用6的袋子装
            int restUse6 = minBagBase6(rest);
            if (restUse6 != -1) {
                bag6 = restUse6;
                break;
            }

            rest = rest + 8;
            bag8--;
        }

        return bag6 == -1 ? -1 : bag6 + bag8;
    }

    // 如果剩余苹果rest可以被装6个苹果的袋子搞定，返回袋子数量
    // 不能搞定返回-1
    public static int minBagBase6(int rest) {
        return rest % 6 == 0 ? (rest / 6) : -1;
    }








    public static int minBags2(int apple) {
        if (apple < 6) {
            return -1;
        }


        int bag8 = apple / 8;

        int rest = apple - bag8 * 8;

        int bag6 = -1;

        while (bag8 >= 0) {
            int restUse6 = minBagBase6(rest);

            if (restUse6 != -1) {
                bag6 = restUse6;
                break;
            }

            bag8--;
            rest = rest + 8;
        }

        return bag6 == -1 ? -1 : bag6 + bag8;
    }















    public static int minBagAwesome(int apple) {
        // 奇数返回 -1
        if ((apple & 1) != 0) {
            return -1;
        }

        if (apple < 18) {
            return apple == 0 ? 0 : (apple == 6 || apple == 8) ? 1
                    : (apple == 12 || apple == 14 || apple == 16) ? 2 : -1;
        }

        return (apple - 18) / 8 + 3;
    }

    public static void main(String[] args) {
        for (int apple = 1; apple < 100; apple++) {
            System.out.println(apple + " : " + minBags(apple) + "\t" + minBagAwesome(apple));
        }
    }
}
