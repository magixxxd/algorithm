package com.xxd.algo.newcode.mid01.class02;


public class Code04_MaxSumInTreeTest {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int val) {
            value = val;
        }
    }

    public static class Info {
        public int value;

        public Info(int value) {
            this.value = value;
        }
    }

    public static int maxPathSum(Node x) {
        if (x == null) {
            return 0;
        }

        Info info = process(x);
        return info.value;
    }

    private static Info process(Node x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);

        if (leftInfo == null && rightInfo == null) {
            return new Info(x.value);
        }

        if (leftInfo != null && rightInfo != null) {
            return new Info(Math.max(leftInfo.value, rightInfo.value) + x.value);
        }

        if (leftInfo == null) {
            return new Info(rightInfo.value + x.value);
        }

		return new Info(leftInfo.value + x.value);
	}


    public static int maxPathSum2(Node x) {
        if (x == null) {
            return 0;
        }

        Info info = process2(x);
        return info.value;
    }

    private static Info process2(Node x) {
        if (x == null) {
            return null;
        }

        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);

        if (leftInfo == null && rightInfo == null) { // 同时为空
            return new Info(x.value);
        }


        // 左不空 右不空
        if (leftInfo != null && rightInfo != null) {
            return new Info(x.value + Math.max(leftInfo.value, rightInfo.value));
        }

        // 左空 右不空
        if (leftInfo == null && rightInfo != null) {
            return new Info(x.value + rightInfo.value);
        }

        // 左不空  右空
        return new Info(x.value + leftInfo.value);
    }

    public static void main(String[] args) {
        Node head = new Node(4);
        head.left = new Node(1);
        head.left.right = new Node(5);
        head.right = new Node(-7);
        head.right.left = new Node(3);
        System.out.println(maxPathSum(head));
        System.out.println(maxPathSum2(head));
    }
}
