package com.xxd.algo.newcode.base02.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-24 17:46
 * @description:
 */
public class TestBit {

    public static void main(String[] args) {
        int a = 983039;
        int b = a >> 18;
        System.out.println(b);

        System.out.println(b & 1);

        System.out.println(1 << 18);
    }
}
