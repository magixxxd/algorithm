package com.xxd.algo.newcode.base02.class01;


import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-02 15:14
 * @description: 并查集
 */
public class UnionFindTest {


    public static class Element<V> {
        V value;

        public Element(V value) {
            this.value = value;
        }
    }

    public static class UnionFindSet<V> {
        // 元素hash表
        public HashMap<V, Element<V>> elementMap;

        // 父
        public HashMap<Element<V>, Element<V>> fatherMap;

        // key 某个集合的代表元素 value 集合的大小
        public HashMap<Element<V>, Integer> sizeMap;

        public UnionFindSet(List<V> list) {
            elementMap = new HashMap<>();
            sizeMap = new HashMap<>();
            fatherMap = new HashMap<>();

            for (V value : list) {
                Element<V> element = new Element<>(value);
                sizeMap.put(element, 1);
                fatherMap.put(element, element);
                elementMap.put(value, element);
            }
        }

        /**
         * 是否是同一个集合
         * @param a
         * @param b
         * @return
         */
        public boolean isSameSet(V a, V b) {
            if (elementMap.containsKey(a) && elementMap.containsKey(b)) {
                return findHead(elementMap.get(a)) == findHead(elementMap.get(b));
            }
            return false;
        }

        /**
         * 合并两个集合
         * @param a
         * @param b
         */
        public void union(V a, V b) {
            if (elementMap.containsKey(a) && elementMap.containsKey(b)) {
                Element<V> aF = findHead(elementMap.get(a));
                Element<V> bF = findHead(elementMap.get(b));

                if (aF != bF) { // 两个人不相同，则把小的 挂在 大的上面
                    Element<V> big = sizeMap.get(aF) >= sizeMap.get(bF) ? aF : bF;
                    Element<V> small = big == aF ? bF : aF;

                    // 小的代表节点，搞到大的代表节点上面去
                    fatherMap.put(small, big);
                    sizeMap.put(big, sizeMap.get(aF) + sizeMap.get(bF));
                    sizeMap.remove(small);
                }
            }
        }

        /**
         * 给一个element ，返回他的代表元素
          * @param element 集合中任意的一个元素
         * @return 代表元素
         */
        private  Element<V> findHead(Element<V> element) {
            Stack<Element<V>> path = new Stack<>();
            while (element != fatherMap.get(element)) {
                path.push(element);
                element = fatherMap.get(element);
            }

            while (!path.isEmpty()) {
                fatherMap.put(path.pop(), element);
            }
            return element;
        }
    }

}
