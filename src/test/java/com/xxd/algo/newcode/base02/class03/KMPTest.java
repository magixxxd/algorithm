package com.xxd.algo.newcode.base02.class03;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-04 20:40
 * @description:
 */
public class KMPTest {


    public static void main(String[] args) {
        String str = "abcabcababaccc";
        String match = "ababa";
        System.out.println(getIndexOf(str, match));
    }

    private static int getIndexOf(String str, String match) {
        if (str == null || match == null || match.length() < 1 || str.length() < match.length()) {
            return -1;
        }

        char[] str1 = str.toCharArray();
        char[] str2 = match.toCharArray();

        int[] next = getNextArray(str2);

        int x = 0;
        int y = 0;

        while (x < str1.length && y < str2.length) {
            if (str1[x] == str2[y]) {
                x++;
                y++;
            } else if (next[y] == -1) {
                x++;
            } else {
                y = next[y];
            }
        }

        return y == str2.length ? x -y : -1;
    }

    private static int[] getNextArray(char[] ms) {
        if (ms.length == 1) {
            return new int[]{-1};
        }

        int[] next = new int[ms.length];
        next[0] = 0;
        next[1] = 1;

        int cn = 0;
        int i = 2;
        while (i < ms.length) {
            if (ms[i - 1] == next[cn]) {
                next[i++] = ++cn;
            } else if (cn != -1) {
                cn = next[cn];
            } else {
                next[i++] = 0;
            }
        }
        return next;
    }
}
