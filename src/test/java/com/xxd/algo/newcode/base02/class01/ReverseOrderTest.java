package com.xxd.algo.newcode.base02.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-04-02 12:19
 * @description: 逆序对问题 在一个数组中，
 * 左边的数如果比右边的数大，
 * 则这两个数构成一个逆序对，请打印所有逆序对
 */
public class ReverseOrderTest {

    public static int reverseOrder(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        return mergeSort(arr, 0, arr.length - 1);
    }

    /**
     * 利用归并排序的思想来
     *
     * @param arr
     * @param l
     * @param r
     */
    private static int mergeSort(int[] arr, int l, int r) {
        if (l == r) {
            return 0;
        }
        int mid = l + ((r - l) >> 1);
        return
        mergeSort(arr, l, mid) +
        mergeSort(arr, mid + 1, r) +
        merge(arr, l, mid, r);
    }

    /**
     * 打印所有的逆序对 左边的数如果比右边的数大，则这两个数构成一个逆序对，请打印所有逆序对
     *
     * @param arr
     * @param l
     * @param mid
     * @param r
     */
    private static int merge(int[] arr, int l, int mid, int r) {
        int[] help = new int[r - l + 1];
        int p1 = l;
        int p2 = mid + 1;
        int i = 0;
        int index = 0;
        int count = 0;
        while (p1 <= mid && p2 <= r) {

            count += arr[p1] > arr[p2] ? (mid - p1 + 1) : 0;

            /*if (arr[p1] > arr[p2]) {
                index = p1;
                while (index <= mid) {
                    count++;
                    System.out.println(arr[index++] + "----" + arr[p2]);
                }
            }*/
            help[i++] = arr[p1] <= arr[p2] ? arr[p1++] : arr[p2++];
        }

        while (p1 <= mid) {
            help[i++] = arr[p1++];
        }
        while (p2 <= r) {
            help[i++] = arr[p2++];
        }

        for (i = 0; i < help.length; i++) {
            arr[l + i] = help[i];
        }

        return count;
    }

    public static void main(String[] args) {
        // int[] arr = {7, 3, 4, 2, 5, 9};
        int[] arr = {1, 3, 2, 3, 1};
        System.out.println(reverseOrder(arr));
    }
}
