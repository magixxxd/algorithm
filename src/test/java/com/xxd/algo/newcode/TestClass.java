package com.xxd.algo.newcode;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-01-08 17:01
 * @description:
 */
public class TestClass {
    public static void main(String[] args) {
        int count = 1000000000;
        int sum = 0;

        long start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            sum += (i % 2 == 0) ? 1 : 0;
        }

        long end = System.currentTimeMillis();

        System.out.println(" 执行耗时 " +  (end - start)); // 827 834 900

    }
}
