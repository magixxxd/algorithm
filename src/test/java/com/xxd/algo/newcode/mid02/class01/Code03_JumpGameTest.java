package com.xxd.algo.newcode.mid02.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-08-18 10:52
 * @description:
 */
public class Code03_JumpGameTest {

    public static int jump(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int step = 0;
        int cur = 0; // 在跳step 的情况下 理论能跳的最远距离
        int next = -1;
        for (int i = 0; i < arr.length; i++) {
            while (cur < i) {
                step++;
                cur = next;
            }

            next = Math.max(next, i + arr[i]);// 再跳一步能跳的最远位置
        }

        return step;
    }

    public static void main(String[] args) {

    }
}
