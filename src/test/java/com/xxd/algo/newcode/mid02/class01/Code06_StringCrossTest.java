package com.xxd.algo.newcode.mid02.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-08-19 11:29
 * @description:
 */
public class Code06_StringCrossTest {

    public static boolean isCross(String s1, String s2, String ai) {
        if (s1 == null || s2 == null || ai == null) {
            return false;
        }


        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        char[] aim = ai.toCharArray();

        boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
        dp[0][0] = true;

        // 第0行
        for (int i = 1; i <= str2.length; i++) {
            if (str2[i - 1] != aim[i - 1]) {
                break;
            }

            dp[0][i] = true;
        }

        // 第0列
        for (int i = 1; i <= str1.length; i++) {
            if (str1[i - 1] != aim[i - 1]) {
                break;
            }

            dp[i][0] = true;
        }


        for (int row = 1; row <= str1.length; row++) {
            for (int col = 1; col <= str2.length; col++) {
                // 当前的 str1 row 位置 和 aim相同
                if (
                        str1[row - 1] == aim[row + col - 1] && dp[row - 1][col]
                                ||
                // 当前的 str2 和 aim 相同
                        str2[col - 1] == aim[row + col - 1] && dp[row][col - 1]) {
                    dp[row][col] = true;
                }

            }
        }


        return dp[str1.length][str2.length];
    }

    public static boolean isCross1(String s1, String s2, String ai) {
        if (s1 == null || s2 == null || ai == null) {
            return false;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        char[] aim = ai.toCharArray();
        if (aim.length != str1.length + str2.length) {
            return false;
        }
        boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
        dp[0][0] = true;
        for (int i = 1; i <= str1.length; i++) {
            if (str1[i - 1] != aim[i - 1]) {
                break;
            }
            dp[i][0] = true;
        }
        for (int j = 1; j <= str2.length; j++) {
            if (str2[j - 1] != aim[j - 1]) {
                break;
            }
            dp[0][j] = true;
        }
        for (int i = 1; i <= str1.length; i++) {
            for (int j = 1; j <= str2.length; j++) {
                if ((str1[i - 1] == aim[i + j - 1] && dp[i - 1][j])
                        || (str2[j - 1] == aim[i + j - 1] && dp[i][j - 1])) {
                    dp[i][j] = true;
                }
            }
        }
        return dp[str1.length][str2.length];
    }

    public static void main(String[] args) {
        String str1 = "1234";
        String str2 = "abcd";
        String aim = "1a23bcd4";
        System.out.println(isCross(str1, str2, aim));
        System.out.println(isCross1(str1, str2, aim));
    }

}
