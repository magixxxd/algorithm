package com.xxd.algo.newcode.mid02.class07;

import javax.jws.Oneway;
import java.util.HashSet;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-10-20 09:30
 * @description:
 */
public class Code02_WorldBreakTest {


    public static int ways1(String str, String[] arr) {
        if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
            return 0;
        }

        HashSet<String> map = new HashSet<>();
        for (String s : arr) {
            map.add(s);
        }
        return f(str, 0, map);
    }

    /**
     * 从 index... str.length 能拼凑出多少种方法
     *
     * @param str
     * @param index
     * @param map
     * @return
     */
    private static int f(String str, int index, HashSet<String> map) {
        if (index == str.length()) {
            return 1;
        }
        int ways = 0;
        // 0..0
        // 0..1
        for (int end = index; end < str.length(); end++) {
            if (map.contains(str.substring(index, end + 1))) {
                ways += f(str, end + 1, map);
            }
        }
        return ways;
    }

    public static class Node {
        public boolean end;
        public Node[] nexts;

        public Node() {
            end = false;
            nexts = new Node[26];
        }
    }

    public static int ways(String str, String[] arr) {
        if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
            return 0;
        }

        Node root = new Node();
        for (String s : arr) {
            char[] chs = s.toCharArray();
            Node node = root;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = chs[i] - 'a';
                if (node.nexts[index] == null) {
                    node.nexts[index] = new Node();
                }
                node = node.nexts[index];
            }
            node.end = true;
        }

        return g(str.toCharArray(), root, 0);
    }

    private static int g(char[] str, Node root, int index) {
        if (index == str.length) {
            return 1;
        }

        int ways = 0;
        Node cur = root;
        for (int end = index; end < str.length; end++) {
            int path = str[end] - 'a';
            if (cur.nexts[path] == null) {
                break;
            }
            // 有当前的路
            cur = cur.nexts[path];
            if (cur.end) { // 数组有这个值
                ways += g(str, root, end + 1);
            }
        }
        return ways;
    }

}
