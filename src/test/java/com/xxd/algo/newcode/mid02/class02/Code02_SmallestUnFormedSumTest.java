package com.xxd.algo.newcode.mid02.class02;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-09-01 09:36
 * @description:
 */
public class Code02_SmallestUnFormedSumTest {

    public static int unformedSum1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 1;
        }

        Set<Integer> set = new HashSet<>();
        process(arr, 0, 0, set);

        int min = Integer.MAX_VALUE;
        int sum = 0;
        for (int i : arr) {
            min = Math.min(i, min);
            sum += i;
        }

        for (int i = min + 1; i <= sum; i++) {
            if (!set.contains(i)) {
                return i;
            }
        }

        return sum + 1;
    }

    private static void process(int[] arr, int index, int sum, Set<Integer> set) {
        if (index == arr.length) {
            set.add(sum);
            return;
        }

        process(arr, index + 1, sum, set);
        process(arr, index + 1, sum + arr[index], set);
    }
}
