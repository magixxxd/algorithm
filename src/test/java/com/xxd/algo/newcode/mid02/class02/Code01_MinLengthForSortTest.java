package com.xxd.algo.newcode.mid02.class02;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-09-01 09:12
 * @description:
 */
public class Code01_MinLengthForSortTest {


    public static int getMinLength(int[] arr) {
        int max = arr[0];
        int noMaxIndex = -1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < max) {
                noMaxIndex = i; // 开始结算
            } else {
                max = Math.max(max, arr[i]); // 更新max
            }
        }


        int min = arr[arr.length - 1];
        int noMinIndex = -1;
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] > min) {
                noMinIndex = i; // 重置开始结算
            } else {
                min = Math.min(min, arr[i]);
            }
        }

        return noMaxIndex - noMinIndex + 1;
    }
}
