package com.xxd.algo.newcode.mid02.class06;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-10-18 09:48
 * @description:
 */
public class Code05_ExpressionAddOperatorsTest {


    public static int ways(String num, int target) {
        char[] str = num.toCharArray();
        int first = str[0] - '0';
        return process(str, 1, 0, first, target);
    }

    private static int process(char[] str, int index, int left,
                               int cur, int target) {
        if (index == str.length) {
            return (left + cur) == target ? 1 : 0;
        }

        int ways = 0;
        int num = str[index] - '0';

        // 这个决定是 cur 和 num 之间的决定！！！
        // +
        ways += process(str, index + 1, left + cur, num, target);
        // -
        ways += process(str, index + 1, left + cur, -num, target);
        // *
        ways += process(str, index + 1, left, cur * num, target);
        // 不加符号
        if (cur != 0) {
            if (cur > 0) {
                ways += process(str, index + 1, left, cur * 10 + num, target);
            } else {
                ways += process(str, index + 1, left, cur * 10 - num, target);
            }
        }

        return ways;
    }
}
