package com.xxd.algo.newcode.base01.class08;

public class HanoiTest {

    public static void hanoi(int n) {
        if (n > 0) {
            func(n, "左", "中", "右");
        }
    }

    // 1~i 圆盘 目标是from -> to， other是另外一个
    public static void func(int N, String from, String other, String to) {
        if (N <= 0) {
            return;
        }

        func(N - 1, from, to, other);
        System.out.println("Move " + N + " from " + from + " to " + to);
        func(N - 1, other, from, to);
    }

    public static void main(String[] args) {
        int n = 3;
        hanoi(n);
    }

}
