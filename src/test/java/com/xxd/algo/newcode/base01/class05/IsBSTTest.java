package com.xxd.algo.newcode.base01.class05;

import java.util.LinkedList;
import java.util.Random;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-29 12:32
 * @description: 是否是一个搜索二叉树
 */
public class IsBSTTest {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    public static class Info {
        boolean isBST;
        int max;
        int min;

        public Info(boolean isBST, int max, int min) {
            this.isBST = isBST;
            this.max = max;
            this.min = min;
        }
    }

    public static Info process(Node head) {
        if (head == null) {
            return null;
        }

        Info leftInfo = process(head.left);
        Info rightInfo = process(head.right);
        int max = head.value;
        int min = head.value;

        if (leftInfo != null) {
            max = Math.max(leftInfo.max, max);
            min = Math.min(leftInfo.min, min);
        }

        if (rightInfo != null) {
            max = Math.max(rightInfo.max, max);
            min = Math.min(rightInfo.min, min);
        }

        boolean left = leftInfo == null || (leftInfo.isBST && leftInfo.max < head.value);
        boolean right = rightInfo == null || (rightInfo.isBST && rightInfo.min > head.value);
        boolean isBST = left && right;
        return new Info(isBST, max, min);
    }

    public static boolean isBST1(Node head) {
        Info info = process(head);
        return info.isBST;
    }

    public static boolean isBST(Node head) {
        if (head == null) {
            return true;
        }
        LinkedList<Node> inOrderList = new LinkedList<>();
        process(head, inOrderList);
        int pre = Integer.MIN_VALUE;
        for (Node cur : inOrderList) {
            if (pre >= cur.value) {
                return false;
            }
            pre = cur.value;
        }
        return true;
    }

    public static void process(Node node,
                               LinkedList<Node> inOrderList) {
        if (node == null) {
            return;
        }
        process(node.left, inOrderList);
        inOrderList.add(node);
        process(node.right, inOrderList);
    }

    public static void main(String[] args) {
        Node head = new Node(32);
        head.left = new Node(26);
        head.right = new Node(47);
        head.left.left = new Node(19);
        head.left.left.right = new Node(27);
        //  head.left.right = new Node(5);
        //  head.right.left = new Node(6);
        head.right.right = new Node(56);

        System.out.println(isBST1(head));
        System.out.println(isBST(head));
    }
}
