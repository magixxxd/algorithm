package com.xxd.algo.newcode.base01.class05;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-25 14:40
 * @description:
 */
public class IsCBTTest {

    public static class Node {
		public int value;
		public Node left;
		public Node right;

		public Node(int data) {
			this.value = data;
		}
	}

	public static boolean isCBT(Node head) {
        if (head == null) {
            return true;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.add(head);
        Node left = null;
        Node right = null;
        boolean leafNoComplete = false;
        while (!queue.isEmpty()) {
            head = queue.poll();
            left = head.left;
            right = head.right;

            // 右孩子在，左孩子不在
            boolean hasRightNoLeft = (right != null && left == null);

            // 之前碰到过 孩子不双全的，这次碰到的竟然不是叶子节点
            boolean  result = leafNoComplete && !(left == null && right == null);
            if (hasRightNoLeft || result) {
                return false;
            }

            if (left != null) {
                queue.add(left);
            }

            if (right != null) {
                queue.add(right);
            }

            if (left == null || right == null) {
                leafNoComplete = true;
            }
        }

        return true;
    }
}
