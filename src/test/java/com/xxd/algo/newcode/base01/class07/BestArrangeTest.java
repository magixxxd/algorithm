package com.xxd.algo.newcode.base01.class07;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-04-14 22:04
 * @description: 一些项目要占用一个会议室宣讲，会议室不能同时容纳两个项目的宣讲。
 * 给你每一个项目开始的时间和结束的时间(给你一个数 组，里面是一个个具体的项目)，
 * 你来安排宣讲的日程，要求会议室进行的宣讲的场次最多。返回这个最多的宣讲场次。
 */
public class BestArrangeTest {

    public static class Program {
        public int start;
        public int end;

        public Program(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "Program{" +
                    "start=" + start +
                    ", end=" + end +
                    '}';
        }
    }

    public static int bestArrange1(Program[] programs) {
        if (programs == null || programs.length == 0) {
            return 0;
        }

        return process(programs, 0, 0);
    }

    /**
     * 会议暴力递归处理
     *
     * @param programs 会议项目
     * @return
     */
    private static int process(Program[] programs, int timeLine, int done) {
        if (programs.length == 0) {
            return 0;
        }

        int max = done;
        for (int i = 0; i < programs.length; i++) {
            if (programs[i].start >= timeLine) {
                Program[] next = copyButExcept(programs, i);
                max = Math.max(max, process(next, programs[i].end, done + 1));
            }
        }

        return max;
    }

    /**
     * 拷贝除了 i 节点元素的数组
     *
     * @param programs 数组
     * @param i        节点索引
     * @return 新数组
     */
    public static Program[] copyButExcept(Program[] programs, int i) {
        Program[] ans = new Program[programs.length - 1];
        int index = 0;
        for (int k = 0; k < programs.length; k++) {
            if (k != i) {
                ans[index++] = programs[k];
            }
        }
        return ans;
    }

    public static int bestArrange2(Program[] programs) {
        Arrays.sort(programs, new ProgramComparator());
        int timeLine = 0;
        int result = 0;
        for (int i = 0; i < programs.length; i++) {
            if (timeLine <= programs[i].start) {
                result++;
                timeLine = programs[i].end;
            }
        }
        return result;
    }

    public static class ProgramComparator implements Comparator<Program> {

        @Override
        public int compare(Program o1, Program o2) {
            return o1.end - o2.end;
        }
    }

    // for test
    public static Program[] generatePrograms(int programSize, int timeMax) {
        Program[] ans = new Program[(int) (Math.random() * (programSize + 1))];
        for (int i = 0; i < ans.length; i++) {
            int r1 = (int) (Math.random() * (timeMax + 1));
            int r2 = (int) (Math.random() * (timeMax + 1));
            if (r1 == r2) {
                ans[i] = new Program(r1, r1 + 1);
            } else {
                ans[i] = new Program(Math.min(r1, r2), Math.max(r1, r2));
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Program[] programs = new Program[4];
      /*
        programs[0] = new Program(0, 7);
        programs[1] = new Program(5, 6);
        programs[2] = new Program(12, 14);
        programs[3] = new Program(0, 3);

        */

        programs[0] = new Program(0, 3);
        programs[1] = new Program(0, 5);
        programs[2] = new Program(4, 6);
        programs[3] = new Program(12, 14);
        System.out.println(bestArrange1(programs));


    }
}
