package com.xxd.algo.newcode.base01.class07;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 15:22
 * @description:
 */
public class ConvertToLetterStringTest {

    /**
     * 主函数
     * @param str
     * @return
     */
    public static int number(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }

        return process(str.toCharArray(), 0);
    }

    private static int process(char[] str, int i) {
        if (i == str.length) {
            return 1;
        }

        if (str[i] == '0') {
            return 0;
        }

        if (str[i] == '1') {
            // 这个问题 可以拆分为 要 与 不要的结果

            // 要
            int res = process(str, i + 1);
            if (i + 1 < str.length) {
                // 加上一起要
                res += process(str, i + 2);
            }
            return res;
        }

        if (str[i] == '2') {
            int res = process(str, i + 1);

            if (i + 1 < str.length && (str[i + 1] >= '0' && str[i + 1] <= '6')) {
                res += process(str, i + 2);
            }
            return res;
        }

        return process(str, i + 1);
    }

    public static void main(String[] args) {
        System.out.println(number("121"));
    }
}
