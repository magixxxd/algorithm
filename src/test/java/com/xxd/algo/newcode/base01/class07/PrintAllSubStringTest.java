package com.xxd.algo.newcode.base01.class07;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 14:26
 * @description:
 */
public class PrintAllSubStringTest {

    public static List<String> getAllSubs(String str) {
        List<String> ans = new ArrayList<>();
        char[] chars = str.toCharArray();

        process(chars, 0, "", ans);

        return ans;
    }

    /**
     * 递归处理函数
     *
     * @param str
     * @param ans
     * @param index
     * @param path
     */
    public static void process(char[] str, int index, String path, List<String> ans) {
        // base case
        if (str.length == index) {
            ans.add(path);
            return;
        }

        // 不要这个字符
        process(str, index + 1, path, ans);

        // 要这个字符
        process(str, index + 1, path + str[index], ans);
    }

    public static void main(String[] args) {
        System.out.println(getAllSubs("abc"));
    }
}
