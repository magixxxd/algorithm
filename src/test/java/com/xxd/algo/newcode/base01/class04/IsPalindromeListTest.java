package com.xxd.algo.newcode.base01.class04;

import java.util.Stack;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-18 14:09
 * @description:
 */
public class IsPalindromeListTest {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    public static boolean isPalindrome1(Node head) {
        Stack<Node> stack = new Stack<>();
        Node cur = head;
        while (cur != null) {
            stack.push(cur);
            cur = cur.next;
        }

        while (!stack.isEmpty()) {
            if (head.value != stack.pop().value) {
                return false;
            }
            head = head.next;
        }

        return true;
    }



}
