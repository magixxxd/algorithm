package com.xxd.algo.newcode.base01.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-18 09:04
 * @description:
 */
public class BSExists {

    public static int exist(int[] sortedArr, int num) {
        if (sortedArr == null || sortedArr.length == 0) {
            return -1;
        }

        int L = 0;
        int R = sortedArr.length - 1;
        int mid = 0;
        int index = -1;
        while (L < R) {
            mid = L + ((R - L) >> 1);
            if (num > sortedArr[mid]) {
                L = mid + 1;
            } else if (num < sortedArr[mid]) {
                R = mid - 1;
            } else {
                return mid;
            }
        }

        return num == sortedArr[L] ? L : index;
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 5, 6, 7, 8, 9};

        System.out.println(exist(arr, 2));
    }
}
