package com.xxd.algo.newcode.base01.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-29 12:23
 * @description: 是否是一颗满二叉树 l = 2 ^ n - 1
 */
public class IsFullTreeTest {

    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(7);
        System.out.println(isFull(head));


    }

    public static boolean isFull(Node head) {
        Info info = process(head);
        return info.nodes == ((1 << info.height) - 1);
    }

    private static Info process(Node head) {
        if (head == null) {
            return new Info(0, 0);
        }

        Info leftInfo = process(head.left);
        Info rightInfo = process(head.right);

        int height = 0;
        int nodes = 1;
        height = Math.max(leftInfo.height, rightInfo.height) + 1;
        nodes = leftInfo.nodes + rightInfo.nodes + 1;
        return new Info(height, nodes);
    }

    public static class Info {
        int height;
        int nodes;

        public Info(int height, int nodes) {
            this.height = height;
            this.nodes = nodes;
        }
    }

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }
}
