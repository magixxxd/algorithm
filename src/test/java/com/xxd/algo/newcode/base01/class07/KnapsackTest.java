package com.xxd.algo.newcode.base01.class07;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 15:47
 * @description:
 */
public class KnapsackTest {


    /**
     * 背包问题
     * @param w
     * @param v
     * @param bag
     * @return
     */
    public static int getMaxValue(int[] w, int[] v, int bag) {
        return process(w, v, 0, 0, bag);
    }

    /**
     *
     * @param w
     * @param v
     * @param index
     * @param alreadyW 背包剩余的容易
     * @param bag
     * @return
     */
    private static int process(int[] w, int[] v, int index, int alreadyW, int bag) {
        if (alreadyW >= bag) {
            return -1;
        }

        if (index == w.length) {
            return 0;
        }

        // 不要这个东西
        int p1 = process(w, v, index + 1, alreadyW, bag);

        // 要这个东西
        int p2next = process(w, v, index + 1, alreadyW + v[index], bag);

        int p2 = -1;
        if (p2next != -1) {
            p2 = p2next + v[index];
        }

        return Math.max(p1, p2);
    }

    public static void main(String[] args) {
        int[] weights = {3, 2, 4, 7};
        int[] values = {5, 6, 3, 19};
        int bag = 11;
        System.out.println(getMaxValue(weights, values, bag));
    }
}
