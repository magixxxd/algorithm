package com.xxd.algo.newcode.base01.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-09-06 18:39
 * @description:
 */
public class Code05_BSNearLeftTest {

    // 在arr上，找满足>=value的最左位置
    public static int nearestIndex(int[] arr, int value) {
        int L = 0;
        int R = arr.length - 1;
        int mid = -1;
        int index = -1;
        while (L < R ) {
            mid = L + ((R - L) >> 1);
            if (arr[mid] >= value) {
                index = mid;
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }

        return index;
    }
}
