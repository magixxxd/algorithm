package com.xxd.algo.newcode.base01.class08;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-01-13 17:17
 * @description:
 */
public class PrintAllSubsquencesTest {

    public static List<String> getAllSubs(String str) {
        ArrayList<String> ans = new ArrayList<>();
        if (str == null) {
            return ans;
        }
        if (str.length() == 0) {
            ans.add("");
            return ans;
        }
        char[] chars = str.toCharArray();
        // str = "abc"
        // length = 3;
        process(chars, ans, 0, "");
        return ans;
    }

    private static void process(char[] str, List<String> ans,
                                int index, String path) {
        if (index == str.length) {
            ans.add(path);
            return;
        }

        process(str, ans, index + 1, path + str[index]);
        process(str, ans, index + 1, path);
    }

    public static void main(String[] args) {
        String test = "abc";
        List<String> ans = getAllSubs(test);


        for (String str : ans) {
            System.out.println(str);
        }
    }
}
