package com.xxd.algo.newcode.base01.class01;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-04 14:44
 * @description:
 */
public class BinarySearch {

    public static boolean exist(int[] sortedArr, int num) {
        if (sortedArr == null || sortedArr.length == 0) {
            return false;
        }

        int l = 0;
        int r = sortedArr.length - 1;
        int mid = 0;
        while (l <= r) {
            mid = ((r - l) >> 1) + l ;
            if (sortedArr[mid] > num) {
                r = mid - 1;
            } else if (sortedArr[mid] < num) {
                l = mid + 1;
            } else {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 5, 6, 7, 8};

        System.out.println(exist(arr, 4));
    }
}
