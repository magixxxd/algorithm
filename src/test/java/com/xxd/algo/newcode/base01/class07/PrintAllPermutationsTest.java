package com.xxd.algo.newcode.base01.class07;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 14:37
 * @description: 全排列
 */
public class PrintAllPermutationsTest {

    public static List<String> getAllC(String s) {
        List<String> ans = new ArrayList<>();

        List<Character> set = new ArrayList<>();
        for (char cha : s.toCharArray()) {
            set.add(cha);
        }

        process(set,  "", ans);

        return ans;
    }

    /**
     * 大问题 就是 选择全部的系列
     * 小问题就是，我选择了一个，省下的 是你们其他人的选择结果
     * @param list
     * @param path
     * @param ans
     */
    private static void process(List<Character> list, String path, List<String> ans) {
        if (list.isEmpty()) {
            ans.add(path);
            return;
        }

        HashSet<Character> set = new HashSet<>();

        for (int index = 0; index < list.size(); index++) {
            if (!set.contains(list.get(index))) {
                String pick = path + list.get(index);
                set.add(list.get(index));

                List<Character> next = new ArrayList<>(list);
                next.remove(index);

                process(next, pick, ans);
            }
        }
    }

    public static void main(String[] args) {
        String str = "aac";
        System.out.println(getAllC(str));
    }
}
