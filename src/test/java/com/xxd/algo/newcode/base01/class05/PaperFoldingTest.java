package com.xxd.algo.newcode.base01.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2021-01-08 10:19
 * @description: 折纸问题
 */
public class PaperFoldingTest {

    public static void printAllFolds(int N) {
        printProcess(1, N, true);
    }

    private static void printProcess(int i, int N, boolean down) {
        if (i > N) {
            return;
        }
        printProcess(i + 1, N, true);
        System.out.print(down ? "凹  " : "凸  ");
        printProcess(i + 1, N, false);
    }

    public static void main(String[] args) {
        printAllFolds(3);
    }
}
