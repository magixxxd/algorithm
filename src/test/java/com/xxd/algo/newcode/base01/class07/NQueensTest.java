package com.xxd.algo.newcode.base01.class07;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 17:00
 * @description:
 */
public class NQueensTest {

    public static int num(int n) {
        if (n < 1) {
            return 0;
        }

        int[] record = new int[n];
        return process(record, n, 0);
    }

    private static int process(int[] record, int n, int i) {
        if (i == n) {  // base case
            return 1;
        }

        int res = 0;
        for (int j = 0; j < n; j++) {
            if (isValid(record, i, j)) {
                record[i] = j;
                res += process(record, n, i + 1);
            }
        }

        return res;
    }

    public static boolean isValid(int[] record, int i, int j) {
        for (int k = 0; k < i; k++) { // 之前的某个k行的皇后
            if (j == record[k] || Math.abs(record[k] - j) == Math.abs(i - k)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int n = 8;

        long start = System.currentTimeMillis();
        System.out.println(num(n));
        long end = System.currentTimeMillis();
        System.out.println("cost time: " + (end - start) + "ms");
    }
}
