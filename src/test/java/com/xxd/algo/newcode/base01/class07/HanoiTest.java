package com.xxd.algo.newcode.base01.class07;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 17:20
 * @description: 汉诺塔
 */
public class HanoiTest {

    public static void hanoi(int n) {
        if (n > 0) {
            func(n, "左", "中" , "右");
        }
    }

    /**
     * n 层汉诺塔问题
     * @param n
     * @param from
     * @param other
     * @param to
     */
    private static void func(int n, String from, String other, String to) {
        if (n == 1) {
            System.out.println("Move 1 from " + from + " to " + to);
        } else {
            func(n - 1, from, other, to);
            System.out.println("Move " + n + " from " + from + " to" + to);
            func(n - 1, other, to, from);
        }
    }
}
