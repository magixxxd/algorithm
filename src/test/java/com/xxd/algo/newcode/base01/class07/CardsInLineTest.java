package com.xxd.algo.newcode.base01.class07;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-01 16:19
 * @description:
 */
public class CardsInLineTest {

    public static int win(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        return Math.max(first(arr, 0, arr.length - 1), second(arr, 0, arr.length - 1));
    }

    /**
     *
     * 1 3 120 30
     *
     * 先手拿，作为先手来讲, 我只有两种拿法，头和尾
     * 那我拿哪个最赚呢？
     *
     * 尝试 假设拿得是1
     *       拿得是30
     *       那种结果最多，我就选哪种
     *
     * @param arr
     * @param i
     * @param j
     * @return
     */
    private static int first(int[] arr, int i, int j) {
        if (i == j) { // base case
            return arr[i];
        }

        return Math.max(arr[i] + second(arr, i + 1, j), arr[j] + second(arr, i, j - 1));
    }

    /**
     * 后手拿
     * @param arr
     * @param i
     * @param j
     * @return
     */
    private static int second(int[] arr, int i, int j) {
        if (i == j) { // base case
            return 0;
        }
        return Math.min(first(arr, i + 1, j), first(arr, i, j - 1));
    }

    public static void main(String[] args) {
        int[] arr = {1, 9, 1};
        System.out.println(win(arr));
    }
}
