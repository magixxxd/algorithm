package com.xxd.algo.newcode.base01.class05;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-25 11:03
 * @description:
 */
public class TreeMaxWidthTest {

    public static class AnnotatedNode {
        Node node;
        int depth, pos;

        AnnotatedNode(Node n, int d, int p) {
            node = n;
            depth = d;
            pos = p;
        }
    }

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    public static int process(Node head) {
        if (head == null) {
            return 0;
        }
        Queue<AnnotatedNode> queue = new LinkedList<>();
        queue.add(new AnnotatedNode(head, 1, 1));

        int curDepth = 1, left = 1, ans = 0;
        while (!queue.isEmpty()) {
            AnnotatedNode node = queue.poll();
            if (node.depth == curDepth) { // 当前这层
                ans = Math.max(ans, node.pos - left + 1);
            } else { // 已经是新启的一层了
                left = node.pos;
                curDepth++;
            }

            if (node.node.left != null) {
                queue.add(new AnnotatedNode(node.node.left, node.depth + 1, node.pos * 2));
            }
            if (node.node.right != null) {
                queue.add(new AnnotatedNode(node.node.right, node.depth + 1, node.pos * 2 + 1));
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(7);

        head.right.right.right = new Node(11);
        head.left.left.left = new Node(0);

       /* head.left.left.left = new Node(8);
        head.left.left.right = new Node(9);
        head.left.right.left = new Node(10);
        head.left.right.right = new Node(11);
        head.right.left.left = new Node(12);
        head.right.left.right = new Node(13);*/
        System.out.println(process(head));

    }
}
