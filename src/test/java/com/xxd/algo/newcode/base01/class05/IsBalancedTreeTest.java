package com.xxd.algo.newcode.base01.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-29 14:07
 * @description:
 */
public class IsBalancedTreeTest {


    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    public static boolean isBalancedTree(Node head) {
        if (head == null) {
            return true;
        }

        Info data = process(head);

        return data.isBalanced;
    }

    public static Info process(Node head) {
        if (head == null) {
            return new Info(true, 0);
        }

        Info leftInfo = process(head.left);
        Info rightInfo = process(head.right);

        boolean isBalanced = leftInfo.isBalanced &&
                rightInfo.isBalanced &&
                Math.abs(leftInfo.height - rightInfo.height) < 2;

        int height = Math.max(leftInfo.height, rightInfo.height) + 1;

        return new Info(isBalanced, height);
    }


    public static class Info {
        boolean isBalanced;
        int height;

        public Info(boolean isBalanced, int height) {
            this.isBalanced = isBalanced;
            this.height = height;
        }
    }



    public static void main(String[] args) {
        Node head = new Node(6);
        head.left = new Node(5);
        head.right = new Node(7);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(8);
        System.out.println(isBalancedTree(head));
    }
}
