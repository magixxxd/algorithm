package com.xxd.algo.newcode.base01.class05;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-06-29 14:29
 * @description:
 */
public class LowestCommonAncestorTest {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }
    
    public static Node lowestAncestor(Node head, Node o1, Node o2) {
        if (head == null) {
            return null;
        }

        Info info = process(head, o1, o2);
        return info.ans;
    }

    public static Info process(Node head, Node o1, Node o2) {
        if (head == null) {
            return new Info(false, false, null);
        }

        Info leftInfo = process(head.left, o1, o2);
        Info rightInfo = process(head.right, o1, o2);

        boolean has01 = (head == o1) || leftInfo.has01 || rightInfo.has01;
        boolean has02 = (head == o2) || leftInfo.has02 || rightInfo.has02;
        Node ans = null;

        if (leftInfo.ans != null) {
            ans = leftInfo.ans;
        }

        if (rightInfo.ans != null) {
            ans = rightInfo.ans;
        }

        if (leftInfo.ans == null && rightInfo.ans == null) {
            if (has01 && has02) {
                ans = head;
            }
        }

        return new Info(has01, has02, ans);
    }

    public static class Info {
        // o1 在左 o2 左
        // o1 在右 o2 右
        // o1 左 o2 右 x
        // o1 右 o2 左 x
        // o1 null o2 null
        // o1 有 o2 没有
        boolean has01;
        boolean has02;
        Node ans;

        public Info(boolean has01, boolean has02, Node ans) {
            this.has01 = has01;
            this.has02 = has02;
            this.ans = ans;
        }
    }

    // for test -- print tree
    public static void printTree(Node head) {
        System.out.println("Binary Tree:");
        printInOrder(head, 0, "H", 17);
        System.out.println();
    }

    public static void printInOrder(Node head, int height, String to, int len) {
        if (head == null) {
            return;
        }
        printInOrder(head.right, height + 1, "v", len);
        String val = to + head.value + to;
        int lenM = val.length();
        int lenL = (len - lenM) / 2;
        int lenR = len - lenM - lenL;
        val = getSpace(lenL) + val + getSpace(lenR);
        System.out.println(getSpace(height * len) + val);
        printInOrder(head.left, height + 1, "^", len);
    }

    public static String getSpace(int num) {
        String space = " ";
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < num; i++) {
            buf.append(space);
        }
        return buf.toString();
    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(7);
        head.right.right.left = new Node(8);
        printTree(head);
        System.out.println("===============");

        Node o1 = head.right.right;
        Node o2 = head.right.left;

        System.out.println("o1 : " + o1.value);
        System.out.println("o2 : " + o2.value);
        System.out.println("ancestor : " + lowestAncestor(head, o1, o2).value);
        System.out.println("===============");

    }
}
