package com.xxd.algo.newcode.base01.class02;

/**
 * @author: XiaoDong.Xie
 * @create: 2020-07-18 10:16
 * @description:
 */
public class GetMax {

    public static int getMax(int[] arr) {
        return process(arr, 0, arr.length - 1);
    }

    private static int process(int[] arr, int l, int r) {
        if (l == r) {
            return arr[l];
        }

        int mid = (l + r) / 2;

        int left = process(arr, l, mid);
        int right = process(arr, mid + 1, r);
        return Math.max(left, right);
    }
    public static void main(String[] args) {
        int[] arr = {1, 5, 6, 8, 9, 7, 2, 3, 199};
        System.out.println(getMax(arr));
    }
}
